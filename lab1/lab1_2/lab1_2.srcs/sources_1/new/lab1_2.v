`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2016/09/24 18:13:30
// Design Name: 
// Module Name: lab1_2
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module lab1_2(a,b,c,aluctr,d,e);
    input a,b,c;
    input [1:0]aluctr;
    output d,e;
    
    wire [1:0]ans_1 = {(a&b)|((a^b)&c),(a^b^c)};
    wire ans_2 = a&b,
        ans_3 = a|b,
        ans_4 = a^b; 
    wire e,d;
    assign {e,d}= (aluctr==2'b00) ? ans_1:
                    ((aluctr==2'b01) ? {1'b0,ans_2}:
                    ((aluctr==2'b10) ? {1'b0,ans_3}:
                    ((aluctr==2'b11) ?{1'b0,ans_4}:2'b00)));
endmodule
