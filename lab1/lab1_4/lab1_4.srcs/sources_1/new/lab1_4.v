`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2016/09/24 18:21:00
// Design Name: 
// Module Name: lab1_4
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module lab1_4(a,b,c,aluctr,d,e);
    input [3:0]a,b;
    input [1:0]aluctr;
    input c;
    output [3:0]d;
    output e;
    wire [3:0] d = {ans_4,ans_3,ans_2,ans_1};
    wire ans_1,ans_2,ans_3,ans_4;
    wire d1,d2,d3;
    lab1_3 a1(a[0],b[0],c,aluctr,ans_1,d1);
    lab1_3 a2(a[1],b[1],d1,aluctr,ans_2,d2);
    lab1_3 a3(a[2],b[2],d2,aluctr,ans_3,d3);
    lab1_3 a4(a[3],b[3],d3,aluctr,ans_4,e);
    
endmodule
