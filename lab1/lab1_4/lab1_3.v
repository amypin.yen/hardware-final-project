`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2016/09/24 18:18:32
// Design Name: 
// Module Name: lab1_3
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module lab1_3(a,b,c,aluctr,d,e);
    input a,b,c;
    input [1:0]aluctr;
    output d,e;
    
    reg d,e;
    
    always @(a or b or c or aluctr)begin
        e = 1'b0;
        case (aluctr)
            2'b00:begin
                d = a^b^c;
                e = ((a^b)&c)|(a&b);
            end
            2'b01:d = a&b;
            2'b10:d = a|b;
            default:d = a^b;
        endcase
     end
        
endmodule
