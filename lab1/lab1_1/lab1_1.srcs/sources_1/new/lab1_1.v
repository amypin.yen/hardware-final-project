`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2016/09/24 18:09:35
// Design Name: 
// Module Name: lab1_1
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module lab1_1(a,b,c,aluctr,d,e);
    input a,b,c;
    input [1:0]aluctr;
    output d,e;
    
    wire ans_1,ans_2,ans_3,ans_4;
    wire ad1,ad2,ad3;
    wire [1:0]aluctr;
    
    and (ans_1,a,b);
    or (ans_2,a,b);
    xor (ans_3,a,b);
    xor (ans_4,ans_3,c);
    and (ad1,ans_3,c);
    or (ans_e,ad1,ans_1);
    
    mux4_to_1 mux({e,d},{ans_e,ans_4},ans_1,ans_2,ans_3,aluctr);
endmodule
