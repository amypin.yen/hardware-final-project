`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2016/09/30 21:37:39
// Design Name: 
// Module Name: lab2_2
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module lab2_2(clk,reset,en,dir,BCD,cout); 
    input clk,reset,en,dir;
    output [3:0]BCD;
    output cout;
    
    reg [3:0]BCD;
    wire [3:0]outputs;
    lab2_1 L1(BCD,en,dir,outputs,cout);
    always @(posedge reset or negedge clk)begin
        if (reset) BCD = 4'b0000;
        else BCD = outputs;
    end
endmodule
