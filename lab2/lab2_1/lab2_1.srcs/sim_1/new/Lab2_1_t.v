`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2016/09/29 19:25:03
// Design Name: 
// Module Name: Lab2_1_t
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Lab2_1_t;
    wire [3:0]out;
    wire cout;
    reg [3:0]in;
    reg en,dir,pass;
        
    lab2_1 L1(in,en,dir,out,cout);
    
    initial begin
       pass = 1'b1; in = 4'b0000; en = 1'b1; dir = 1'b1;
       #128;
       if (pass) $display("--------[PASS.	Congratulations!]--------");
       else        $display("--------ERROR. Try it again.--------");
       $finish;
    end
    always #64 en = 1'b0;
    always #32 dir = dir + 1'b1;
    always #2 begin
        in = in +1'b1;
    end 
       
    always @(*) begin
    #1      if (en) begin
                if (dir)begin
                    if (in < 4'b1001)begin
                        if (out == in + 1'b1)good;
                        else error;
                    end
                    else if (in == 4'b1001)begin
                        if (out != 4'b0000 || cout != 1'b1)error;
                        else good;
                    end
                    else begin
                        if (out == 4'b0000 && cout == 1'b0) good;
                        else error;
                    end
                 end
                 else begin
                     if (in <= 4'b1001 && in !=4'b0000)begin
                         if (out == in - 1'b1)good;
                         else error;
                     end
                     else if (in == 4'b0000)begin
                         if (out != 4'b1001 || cout != 1'b1)error;
                         else good;
                     end
                     else begin
                         if (out == 4'b0000 && cout == 1'b0)  good;
                         else error;
                     end
                  end
            end
            else begin
                if (out != in || cout != 1'b0) error;
                else good;
            end
    end
    task error;
        begin
            pass = 1'b0;
            $display("ERROR:",$time,"en:%b\tdir=%b\tinput=%b\toutputs=%b\tcout=%b",en,dir,in,out,cout);
        end
     endtask
     task good;
         begin
             $display ("RIGHT:",$time,"en:%b\tdir=%b\tinput=%b\toutputs=%b\tcout=%b",en,dir,in,out,cout);
         end
      endtask
endmodule
