`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2016/09/29 19:07:17
// Design Name: 
// Module Name: lab2_1
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module lab2_1(inputs,en,dir,outputs,cout);
    input [3:0]inputs;
    input en,dir;
    output [3:0]outputs;
    output cout;
 
    reg [3:0]outputs;
    reg cout;
    
    always@(*)
        begin
            if(en)begin
                if(dir) outputs = inputs +1'b1;
                else outputs = inputs - 1'b1;
                if (inputs > 4'b1001) begin
                    outputs = 4'b0000;
                    cout = 1'b0;
                end
                else if (outputs > 4'b1001) begin
                    outputs = {dir+1'b1,1'b0,1'b0,dir+1'b1};
                    cout = 1'b1;
                end
                else cout = 1'b0;
             end
             else begin
                outputs = inputs;
                cout = 1'b0;
             end
         end
endmodule
