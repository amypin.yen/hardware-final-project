@echo off
set xv_path=C:\\Xi\\Vivado\\2016.2\\bin
call %xv_path%/xsim Lab2_1_t_behav -key {Behavioral:sim_1:Functional:Lab2_1_t} -tclbatch Lab2_1_t.tcl -log simulate.log
if "%errorlevel%"=="0" goto SUCCESS
if "%errorlevel%"=="1" goto END
:END
exit 1
:SUCCESS
exit 0
