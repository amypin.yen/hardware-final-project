# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"
  #Adding Page
  set Page_0 [ipgui::add_page $IPINST -name "Page 0"]
  ipgui::add_param $IPINST -name "END" -parent ${Page_0}
  ipgui::add_param $IPINST -name "LEFTDOWN" -parent ${Page_0}
  ipgui::add_param $IPINST -name "LEFTUP" -parent ${Page_0}
  ipgui::add_param $IPINST -name "RIGHTDOWN" -parent ${Page_0}
  ipgui::add_param $IPINST -name "RIGHTUP" -parent ${Page_0}


}

proc update_PARAM_VALUE.END { PARAM_VALUE.END } {
	# Procedure called to update END when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.END { PARAM_VALUE.END } {
	# Procedure called to validate END
	return true
}

proc update_PARAM_VALUE.LEFTDOWN { PARAM_VALUE.LEFTDOWN } {
	# Procedure called to update LEFTDOWN when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.LEFTDOWN { PARAM_VALUE.LEFTDOWN } {
	# Procedure called to validate LEFTDOWN
	return true
}

proc update_PARAM_VALUE.LEFTUP { PARAM_VALUE.LEFTUP } {
	# Procedure called to update LEFTUP when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.LEFTUP { PARAM_VALUE.LEFTUP } {
	# Procedure called to validate LEFTUP
	return true
}

proc update_PARAM_VALUE.RIGHTDOWN { PARAM_VALUE.RIGHTDOWN } {
	# Procedure called to update RIGHTDOWN when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.RIGHTDOWN { PARAM_VALUE.RIGHTDOWN } {
	# Procedure called to validate RIGHTDOWN
	return true
}

proc update_PARAM_VALUE.RIGHTUP { PARAM_VALUE.RIGHTUP } {
	# Procedure called to update RIGHTUP when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.RIGHTUP { PARAM_VALUE.RIGHTUP } {
	# Procedure called to validate RIGHTUP
	return true
}


proc update_MODELPARAM_VALUE.RIGHTUP { MODELPARAM_VALUE.RIGHTUP PARAM_VALUE.RIGHTUP } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.RIGHTUP}] ${MODELPARAM_VALUE.RIGHTUP}
}

proc update_MODELPARAM_VALUE.LEFTUP { MODELPARAM_VALUE.LEFTUP PARAM_VALUE.LEFTUP } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.LEFTUP}] ${MODELPARAM_VALUE.LEFTUP}
}

proc update_MODELPARAM_VALUE.RIGHTDOWN { MODELPARAM_VALUE.RIGHTDOWN PARAM_VALUE.RIGHTDOWN } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.RIGHTDOWN}] ${MODELPARAM_VALUE.RIGHTDOWN}
}

proc update_MODELPARAM_VALUE.LEFTDOWN { MODELPARAM_VALUE.LEFTDOWN PARAM_VALUE.LEFTDOWN } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.LEFTDOWN}] ${MODELPARAM_VALUE.LEFTDOWN}
}

proc update_MODELPARAM_VALUE.END { MODELPARAM_VALUE.END PARAM_VALUE.END } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.END}] ${MODELPARAM_VALUE.END}
}

