`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2016/09/25 08:50:56
// Design Name: 
// Module Name: ButtonCtrl
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ButtonCtrl(output reg toright,
                  output reg toleft,
                  output reg light,
                  output reg exit,
                  output reg pause,
                  output reg stage1,
                  output reg stage2,
                  output reg stage3,
                  output reg stage4,
                  output reg stage5,
                  output reg NEXT,
                  input wire clk,
                  input wire rst,
                  inout wire PS2_DATA,
                  inout wire PS2_CLK);
                  
parameter [8:0] LEFT_SHIFT_CODES  = 9'b0_0001_0010;
parameter [8:0] RIGHT_SHIFT_CODES = 9'b0_0101_1001;
parameter [8:0] LEFT_CTRL_CODES = 9'b0_0001_0100;
parameter [8:0] RIGHT_CTRL_CODES = 9'b1_0001_0100;
wire [8:0] KEY_CODES [0:10]   = {
        9'b0_0001_1100, //A  => 1C
        9'b0_0010_0011, //D  => 23 
        9'b0_0100_1011, //L => 4B
        9'b0_0100_1101, //P => 4D
        9'b0_0111_0110, //ESC => 76
        9'b0_0001_0110, // 1 => 16
        9'b0_0001_1110, // 2 => 1E
        9'b0_0010_0110, // 3 => 26
        9'b0_0010_0101, // 4 => 25
        9'b0_0010_1110,  // 5 => 2E
        9'b0_0011_0001  //N  => 31
};
wire shift_down;
wire ctrl_down;
wire [511:0] key_down;
wire [8:0] last_change;
wire been_ready;
	
assign shift_down = (key_down[LEFT_SHIFT_CODES] == 1'b1 || key_down[RIGHT_SHIFT_CODES] == 1'b1) ? 1'b1 : 1'b0;
assign ctrl_down =  (key_down[LEFT_CTRL_CODES] == 1'b1 || key_down[RIGHT_CTRL_CODES] == 1'b1) ? 1'b1 : 1'b0;
KeyboardDecoder key_de (.key_down(key_down), .last_change(last_change), .key_valid(been_ready), .PS2_DATA(PS2_DATA), .PS2_CLK(PS2_CLK), .rst(rst), .clk(clk));

always @ (posedge clk, posedge rst) begin
    if (rst) begin
        toright <= 1'b0;
        toleft <= 1'b0;
        light <= 1'b0;
        exit <= 1'b0;
        pause <= 1'b0;
        stage1 <= 1'b0;
        stage2 <= 1'b0;
        stage3 <= 1'b0;
        stage4 <= 1'b0;
        stage5 <= 1'b0;
        NEXT <= 1'b0;
    end else begin
        toright <= 1'b0;
        toleft <= 1'b0;
        light <= 1'b0;
        exit <= 1'b0;
        pause <= 1'b0;
        stage1 <= 1'b0;
        stage2 <= 1'b0;
        stage3 <= 1'b0;
        stage4 <= 1'b0;
        stage5 <= 1'b0;
        NEXT <= 1'b0;
        if (been_ready && key_down[last_change] == 1'b1) begin
            if (last_change == KEY_CODES[0]) toleft <= 1'b1;
            if (last_change == KEY_CODES[1]) toright <= 1'b1;
            if (last_change == KEY_CODES[2]) light <= 1'b1;
            if (last_change == KEY_CODES[3]) pause <= 1'b1;
            if (last_change == KEY_CODES[4]) exit <= 1'b1;
            if (last_change == KEY_CODES[5]) stage1 <= 1'b1;
            if (last_change == KEY_CODES[6]) stage2 <= 1'b1;
            if (last_change == KEY_CODES[7]) stage3 <= 1'b1;
            if (last_change == KEY_CODES[8]) stage4 <= 1'b1;
            if (last_change == KEY_CODES[9]) stage5 <= 1'b1;
            if (last_change == KEY_CODES[10]) NEXT <= 1'b1;
        end
    end
end	
    
endmodule
