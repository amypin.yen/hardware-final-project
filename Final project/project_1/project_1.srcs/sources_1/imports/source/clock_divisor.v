module clock_divisor(clk1, clk, clk13,clk18,clk19);
input clk;
output clk1;
output clk13;
output clk18;
output clk19;
reg [21:0] num;
wire [21:0] next_num;

always @(posedge clk) begin
  num <= next_num;
end

assign next_num = num + 1'b1;
assign clk1 = num[1];
assign clk13 = num[12];
assign clk18 = num[19];
assign clk19 = num[20];

endmodule
