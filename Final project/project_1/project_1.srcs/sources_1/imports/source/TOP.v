`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2016/12/05 21:07:38
// Design Name: 
// Module Name: TOP
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module TOP(
   input clk,
   input reset,
   inout PS2_DATA,
   inout PS2_CLK,
   input ballspeed,
   input character0,
   input character1,
   input Mute,
   output [3:0]DIGIT,
   output [6:0]DISPLAY,
   output [9:0]LED,
   output [3:0] vgaRed,
   output [3:0] vgaGreen,
   output [3:0] vgaBlue,
   output hsync,
   output vsync,
   output pmod_1,
   output pmod_2,
   output pmod_4
 );
 
 wire clk_25MHz;
 wire clk13;
 wire clk18;
 wire clk19;
 wire s_clk = (ballspeed)? clk18 : clk19;
 
 wire valid;
 wire [11:0] pixel;
 wire [11:0] backpixel;
 wire [9:0] h_cnt; //640
 wire [9:0] v_cnt;

 wire toright,Lright;
 wire toleft,Lleft;
 wire light,Llight;
 wire exit;
 wire pause;
 wire stage1;
 wire stage2;
 wire stage3;
 wire stage4;
 wire stage5;
 wire NEXT;
 
 wire [9:0]pikapos;
 wire pikadir;
 wire [9:0]ball_h1;
 wire [9:0]ball_v1;
 wire [9:0]ball_h2;
 wire [9:0]ball_v2;
 wire [9:0]ball_h3;
 wire [9:0]ball_v3;
 wire [9:0]ball_h4;
 wire [9:0]ball_v4;
 wire [9:0]ball_h5;
 wire [9:0]ball_v5;
 
 reg [3:0]state;
 reg [3:0]state_n;
 parameter BACK = 4'b0000;
 parameter S1 = 4'b0001;
 parameter S2 = 4'b0010;
 parameter S3 = 4'b0011;
 parameter S4 = 4'b0100;
 parameter S5 = 4'b0101;
 wire s_end;
 wire ending1;
 wire ending2;
 wire ending3;
 wire ending4;
 wire ending5;
 wire [23:0]block1;
 wire [23:0]block2;
 wire [23:0]block3;
 wire [23:0]block4;
 wire [23:0]block5;
 
 wire [10:0]score1;
 wire [10:0]score2;
 wire [10:0]score3;
 wire [10:0]score4;
 wire [10:0]score5;
 reg [10:0]ALLscore;
 reg [10:0]ALLscore_n;
 reg [10:0]acscore;
 reg [10:0]acscore_n;
 
 wire [9:0]LED1;
 wire [9:0]LED2;
 wire [9:0]LED3;
 wire [9:0]LED4;
 wire [9:0]LED5;
 
 wire win1;
 wire win2;
 wire win3;
 wire win4;
 wire win5;
 wire s_win;
 
 wire NEXTstage;
 
 
 wire showlight1,showlight2,showlight3,showlight4,showlight5;
 wire [9:0]light_h1,light_h2,light_h3,light_h4,light_h5;
 
 wire [31:0] freq;
 wire [9:0] ibeatNum;
 wire [31:0]BEAT_FREQ;
 assign  BEAT_FREQ[31:0] = 32'd8; //one beat=0.125sec

 wire [9:0]DUTY_BEST;
 assign DUTY_BEST[9:0] = 10'd512;
 
assign LED = (state==BACK)? 10'd0 : 
              (state==4'b0001)? LED1 :
              (state==4'b0010)? LED2 :
              (state==4'b0011)? LED3 :
              (state==4'b0100)? LED4 : LED5;
assign pmod_2 = 1'd1;	//no gain(6dB)
assign pmod_4 = (Mute)? 1'd0:1'd1;

assign {vgaRed, vgaGreen, vgaBlue} = (valid==1'b0) ? 12'h0 : 
                                     (state==BACK)? backpixel : pixel;


assign s_end =(state==4'b0001)?ending1 :
              (state==4'b0010)?ending2 :
              (state==4'b0011)?ending3 : 
              (state==4'b0100)?ending4 :
              (state==4'b0101)?ending5 : 0;

assign s_win =(state==4'b0001)?win1 :
              (state==4'b0010)?win2 :
              (state==4'b0011)?win3 : 
              (state==4'b0100)?win4 :
              (state==4'b0101)?win5 : 0;
assign NEXTstage = s_win && NEXT;


 clock_divisor clk_wiz_0_inst(
   .clk(clk),
   .clk1(clk_25MHz),
   .clk13(clk13),
   .clk18(clk18),
   .clk19(clk19)
 );
 //  keyboard
 ButtonCtrl b_ctrl (
    .toright(toright),
    .toleft(toleft),
    .light(light),
    .exit(exit),
    .pause(pause),
    .stage1(stage1),
    .stage2(stage2),
    .stage3(stage3),
    .stage4(stage4),
    .stage5(stage5),
    .NEXT(NEXT),
    .PS2_DATA(PS2_DATA),
    .PS2_CLK(PS2_CLK),
    .rst(reset),
    .clk(clk)
 );
 
 CreateLargePulse #(
     .PULSE_SIZE(20)
 ) c_pulse_toright (
     .large_pulse(Lright),
     .small_pulse(toright),
     .rst(reset),
     .clk(clk)
 );
  CreateLargePulse #(
     .PULSE_SIZE(20)
 ) c_pulse_toleft (
     .large_pulse(Lleft),
     .small_pulse(toleft),
     .rst(reset),
     .clk(clk)
 );
   CreateLargePulse #(
    .PULSE_SIZE(20)
) c_pulse_light (
    .large_pulse(Llight),
    .small_pulse(light),
    .rst(reset),
    .clk(clk)
);
 
// MOVE
 pikaMove pM(
  .Lleft(Lleft),
  .Lright(Lright),
  .pause(pause),
  .reset(reset),
  .Top_state(state),
  .exit(exit),
  .clk(clk18),
  .ending(s_end),
  .NEXTstage(NEXTstage),
  .pikapos(pikapos),
  .dir(pikadir)
 );
 ballMoveS1 b1(
  .clk(s_clk),
  .reset(reset),
  .pause(pause),
  .Top_state(state),
  .exit(exit),
  .light(Llight),
  .pikapos(pikapos),
  .ball_h(ball_h1),
  .ball_v(ball_v1),
  .block(block1),
  .ending(ending1),
  .score(score1),
  .LED(LED1),
  .showlight(showlight1),
  .light_h(light_h1),
  .win(win1)
 );
  ballMoveS2 b2(
  .clk(s_clk),
  .reset(reset),
  .pause(pause),
  .Top_state(state),
  .exit(exit),
  .light(Llight),
  .pikapos(pikapos),
  .ball_h(ball_h2),
  .ball_v(ball_v2),
  .block(block2),
  .ending(ending2),
  .score(score2),
  .LED(LED2),
  .showlight(showlight2),
  .light_h(light_h2),
  .win(win2)
 );
 ballMoveS3 b3(
 .clk(s_clk),
 .reset(reset),
 .pause(pause),
 .Top_state(state),
 .exit(exit),
 .light(Llight),
 .pikapos(pikapos),
 .ball_h(ball_h3),
 .ball_v(ball_v3),
 .block(block3),
 .ending(ending3),
 .score(score3),
 .LED(LED3),
 .showlight(showlight3),
 .light_h(light_h3),
 .win(win3)
);
 ballMoveS4 b4(
.clk(s_clk),
.reset(reset),
.pause(pause),
.Top_state(state),
.exit(exit),
.light(Llight),
.pikapos(pikapos),
.ball_h(ball_h4),
.ball_v(ball_v4),
.block(block4),
.ending(ending4),
.score(score4),
.LED(LED4),
.showlight(showlight4),
.light_h(light_h4),
.win(win4)
);
 ballMoveS5 b5(
.clk(s_clk),
.reset(reset),
.pause(pause),
.Top_state(state),
.exit(exit),
.light(Llight),
.pikapos(pikapos),
.ball_h(ball_h5),
.ball_v(ball_v5),
.block(block5),
.ending(ending5),
.score(score5),
.LED(LED5),
.showlight(showlight5),
.light_h(light_h5),
.win(win5)
);

//7-seg
seven_segment seg(
  .clk(clk13),
  .score(ALLscore),
  .DIGIT(DIGIT),
  .DISPLAY(DISPLAY)
 );

 // V G A
 mem_addr_gen_S1andbk mem_addr_gen_inst(
 .clk(clk_25MHz),
 .rst(reset),
 .pikapos(pikapos),
 .pikadir(pikadir),
 .ball_h1(ball_h1),
 .ball_v1(ball_v1),
 .ball_h2(ball_h2),
 .ball_v2(ball_v2),
 .ball_h3(ball_h3),
 .ball_v3(ball_v3),
 .ball_h4(ball_h4),
 .ball_v4(ball_v4),
 .ball_h5(ball_h5),
 .ball_v5(ball_v5),
 .h_cnt(h_cnt),
 .v_cnt(v_cnt),
 .pixel(pixel),
 .block1(block1),
 .block2(block2),
 .block3(block3),
 .block4(block4),
 .block5(block5),    
 .light_h1(light_h1),
 .light_h2(light_h2),
 .light_h3(light_h3),
 .light_h4(light_h4),
 .light_h5(light_h5),    
 .showlight1(showlight1),
 .showlight2(showlight2),
 .showlight3(showlight3),
 .showlight4(showlight4),
 .showlight5(showlight5),
 .win1(win1),
 .win2(win2),
 .win3(win3),
 .win4(win4),
 .win5(win5),
 .ending(s_end),
 .TOPstate(state),
 .backpixel(backpixel),
 .character0(character0),
 .character1(character1)
 );

 vga_controller   vga_inst(
   .pclk(clk_25MHz),
   .reset(reset),
   .hsync(hsync),
   .vsync(vsync),
   .valid(valid),
   .h_cnt(h_cnt),
   .v_cnt(v_cnt)
 );
 //Generate beat speed
 PWM_gen btSpeedGen ( .clk(clk), 
                      .reset(reset),
                      .freq(BEAT_FREQ),
                      .duty(DUTY_BEST), 
                      .PWM(beatFreq)
 );
     
 //manipulate beat
 PlayerCtrl playerCtrl_00 ( .clk(beatFreq),
                            .reset(reset),
                            .ibeat(ibeatNum)
 );    
     
 //Generate variant freq. of tones
 Music music00 ( .ibeatNum(ibeatNum),
                 .tone(freq)
 );
 
 // Generate particular freq. signal
 PWM_gen toneGen ( .clk(clk), 
                   .reset(reset), 
                   .freq(freq),
                   .duty(DUTY_BEST), 
                   .PWM(pmod_1)
 );
 always@(posedge clk or posedge reset)begin
    if(reset)begin
        state <= BACK;
        ALLscore <= 11'd0;
        acscore <= 11'd0;
    end
    else
        state <= state_n;
        ALLscore <= ALLscore_n;
        acscore <= acscore_n;
 end
 always@(*)begin
    case(state)
        BACK:begin
            state_n <= BACK;
            ALLscore_n <= 11'd0;
            acscore_n <= 11'd0;
            if(stage1) state_n <= S1;
            else if(stage2) state_n <= S2;
            else if(stage3) state_n <= S3;
            else if(stage4) state_n <= S4;
            else if(stage5) state_n <= S5;
        end
        S1:begin
            state_n <= S1;
            ALLscore_n <= score1;
            acscore_n <= 11'd0;
            if(exit) state_n <= BACK;
            else if(win1&&NEXT) begin 
                state_n <= S2;
                acscore_n <= 11'd240;
            end
        end
        S2:begin
            state_n <= S2;
            ALLscore_n <= acscore + score2;
            acscore_n <= acscore;
            if(exit) state_n <= BACK;
            else if(win2&&NEXT) begin
                state_n <= S3;
                acscore_n <= acscore +11'd240;
            end
        end
        S3:begin
            state_n <= S3;
            ALLscore_n <= acscore + score3;
            acscore_n <= acscore;
            if(exit) state_n <= BACK;
            else if(win3&&NEXT) begin
                state_n <= S4;
                acscore_n <= acscore +11'd240;
            end                
        end
        S4:begin
             state_n <= S4;
             ALLscore_n <= acscore + score4;
             acscore_n <= acscore;
             if(exit) state_n <= BACK;
             else if(win4&&NEXT) begin 
                state_n <= S5;
                acscore_n <= acscore +11'd240;
            end                          
         end
        S5:begin
             state_n <= S5;
             ALLscore_n <= acscore + score5;
             acscore_n <= acscore;
             if(exit) state_n <= BACK;
         end          
        default:begin
            state_n <= BACK;
        end
    endcase
 end

endmodule
