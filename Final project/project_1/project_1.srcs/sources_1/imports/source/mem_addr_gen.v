module mem_addr_gen_S1andbk(
   input clk,
   input rst,
   input [9:0]pikapos,
   input pikadir,
   input [9:0]ball_h1,
   input [9:0]ball_v1,
   input [9:0]ball_h2,
   input [9:0]ball_v2,
   input [9:0]ball_h3,
   input [9:0]ball_v3,
   input [9:0]ball_h4,
   input [9:0]ball_v4,
   input [9:0]ball_h5,
   input [9:0]ball_v5,
   input [9:0] h_cnt,
   input [9:0] v_cnt,
   input [23:0]block1,
   input [23:0]block2,
   input [23:0]block3,
   input [23:0]block4,
   input [23:0]block5,
   input [9:0]light_h1,
   input [9:0]light_h2,
   input [9:0]light_h3,
   input [9:0]light_h4,
   input [9:0]light_h5,
   input win1,
   input win2,
   input win3,
   input win4,
   input win5,
   input ending,
   input showlight1,
   input showlight2,
   input showlight3,
   input showlight4,
   input showlight5,               
   input character0,
   input character1,
   input [3:0]TOPstate,
   output [11:0] pixel,
   output [11:0] backpixel
   );
   wire [11:0] data1,data2,data3,data4,data5,data6,data7,data8,data9,data10,data11,data12;
   wire [16:0] pixel_addr;
   wire [16:0] pikapixel_addr;
   wire [16:0] ballpixel_addr;
   wire [16:0] lightpixel_addr;
   wire [16:0] NEXTpixel_addr;
   wire [11:0] pi1pixel;
   wire [11:0] pi2pixel;
   wire [11:0] ballpixel;
   wire [11:0] lightpixel;
   wire [11:0] drapixel1;
   wire [11:0] drapixel2;
   wire [11:0] firepixel;
   wire [11:0] bulpixel1;
   wire [11:0] bulpixel2;
   wire [11:0] leafpixel;
   wire [11:0] squpixel1;
   wire [11:0] squpixel2;
   wire [11:0] waterpixel;
   wire [11:0] NEXTpixel;
   wire [11:0] winpixel;
   wire [11:0] losepixel;
   wire [9:0] blockup1 [0:23]={
       10'd10,10'd10,10'd10,10'd10,10'd10,10'd10,
       10'd30,10'd30,10'd30,10'd30,10'd30,10'd30,
       10'd50,10'd50,10'd50,10'd50,10'd50,10'd50,
       10'd70,10'd70,10'd70,10'd70,10'd70,10'd70
       };
   wire [9:0] blockleft1 [0:23]={
       10'd10,10'd60,10'd110,10'd160,10'd210,10'd260,
       10'd10,10'd60,10'd110,10'd160,10'd210,10'd260,
       10'd10,10'd60,10'd110,10'd160,10'd210,10'd260,
       10'd10,10'd60,10'd110,10'd160,10'd210,10'd260
       };
   wire [9:0] blockup2 [0:23]={
           10'd20,10'd30,10'd40,10'd40,10'd40,10'd40,10'd40,10'd40,10'd40,
           10'd50,10'd50,10'd50,10'd50,10'd50,10'd50,10'd50,10'd60,10'd60,10'd70,10'd70,
           10'd80,10'd80,10'd90,10'd90
           };
   wire [9:0] blockleft2 [0:23]={
           10'd145,10'd145,10'd55,10'd85,10'd115,10'd145,10'd175,10'd205,
           10'd235,10'd55,10'd85,10'd115,10'd145,10'd175,10'd205,10'd235,10'd130,10'd160,
           10'd100,10'd190,10'd70,10'd220,10'd40,10'd250
           };   
   wire [9:0] blockup3 [0:23]={
               10'd50,10'd40,10'd60,10'd30,10'd50,10'd70,10'd20,10'd40,10'd60,10'd80,
               10'd10,10'd30,10'd70,10'd90,10'd20,10'd40,10'd60,10'd80,10'd30,10'd50,10'd70,
               10'd40,10'd60,10'd50
               };
   wire [9:0] blockleft3 [0:23]={
               10'd25,10'd55,10'd55,10'd85,10'd85,10'd85,10'd115,10'd115,10'd115,10'd115,
               10'd145,10'd145,10'd145,10'd145,10'd175,10'd175,10'd175,10'd175,
               10'd205,10'd205,10'd205,10'd235,10'd235,10'd265
               };
   wire [9:0] blockup4 [0:23]={
            10'd10,10'd10,10'd10,10'd10
            ,10'd20,10'd20
            ,10'd30,10'd30
            ,10'd40,10'd40,10'd40,10'd40,10'd40,10'd40,10'd40,10'd40
            ,10'd50,10'd50
            ,10'd60,10'd60
            ,10'd70,10'd70,10'd70,10'd70
            };
    wire [9:0] blockleft4 [0:23]={
        10'd40,10'd100,10'd190,10'd250
        ,10'd70,10'd220,10'd100,10'd190,10'd10,10'd40,10'd70,10'd130
        ,10'd160,10'd220,10'd250,10'd280,10'd100,10'd190,10'd70,10'd220
        ,10'd40,10'd100,10'd190,10'd250
        };
   wire [9:0] blockup5 [0:23]={
             10'd10,10'd20,10'd20,10'd30,10'd30,10'd30,10'd30,10'd30
             ,10'd40,10'd40,10'd40,10'd40,10'd40,10'd40,10'd40,10'd40
             ,10'd50,10'd50,10'd50,10'd50,10'd50,10'd60,10'd60,10'd70
             };
   wire [9:0] blockleft5 [0:23]={
     10'd205,10'd175,10'd205,10'd55,10'd145,10'd175,10'd205,10'd235,10'd25
     ,10'd85,10'd115,10'd145,10'd175,10'd205,10'd235,10'd265,10'd55,10'd145
     ,10'd175,10'd205,10'd235,10'd175,10'd205,10'd205
     };
   wire [11:0] charapixel1;
   wire [11:0] charapixel2;
   wire [11:0] skillpixel;
   wire [9:0] blockup [0:24] ;
   wire [9:0] blockleft [0:24] ;
   wire [9:0] ball_h;
   wire [9:0] ball_v;
   wire [9:0] light_h;
   wire [23:0]block;
   wire showlight;
   wire WIN;
   assign WIN = (TOPstate==4'b0001)? win1 :
                (TOPstate==4'b0010)? win2 :
                (TOPstate==4'b0011)? win3 :
                (TOPstate==4'b0100)? win4 : win5;
   assign showlight =  (TOPstate==4'b0001)? showlight1 :
                        (TOPstate==4'b0010)? showlight2 :
                        (TOPstate==4'b0011)? showlight3 : 
                        (TOPstate==4'b0100)? showlight4 : showlight5;       
   assign block = (TOPstate==4'b0001)? block1 :
                       (TOPstate==4'b0010)? block2 :
                       (TOPstate==4'b0011)? block3 : 
                       (TOPstate==4'b0100)? block4 : block5;                                                
   assign blockup[0] = (TOPstate==4'b0001)? blockup1[0] :
                       (TOPstate==4'b0010)? blockup2[0] :
                       (TOPstate==4'b0011)? blockup3[0] : 
                       (TOPstate==4'b0100)? blockup4[0] : blockup5[0];
  assign blockup[1] = (TOPstate==4'b0001)? blockup1[1] :
                       (TOPstate==4'b0010)? blockup2[1] :
                       (TOPstate==4'b0011)? blockup3[1] : 
                       (TOPstate==4'b0100)? blockup4[1] : blockup5[1];
   assign blockup[2] = (TOPstate==4'b0001)? blockup1[2] :
                        (TOPstate==4'b0010)? blockup2[2] :
                        (TOPstate==4'b0011)? blockup3[2] : 
                        (TOPstate==4'b0100)? blockup4[2] : blockup5[2];
   assign blockup[3] = (TOPstate==4'b0001)? blockup1[3] :
                       (TOPstate==4'b0010)? blockup2[3] :
                      (TOPstate==4'b0011)? blockup3[3] : 
                      (TOPstate==4'b0100)? blockup4[3] : blockup5[3];
   assign blockup[4] = (TOPstate==4'b0001)? blockup1[4] :
                      (TOPstate==4'b0010)? blockup2[4] :
                     (TOPstate==4'b0011)? blockup3[4] : 
                     (TOPstate==4'b0100)? blockup4[4] : blockup5[4];
    assign blockup[5] = (TOPstate==4'b0001)? blockup1[5] :
                         (TOPstate==4'b0010)? blockup2[5] :
                         (TOPstate==4'b0011)? blockup3[5] : 
                         (TOPstate==4'b0100)? blockup4[5] : blockup5[5];
    assign blockup[6] = (TOPstate==4'b0001)? blockup1[6] :
                         (TOPstate==4'b0010)? blockup2[6] :
                         (TOPstate==4'b0011)? blockup3[6] : 
                         (TOPstate==4'b0100)? blockup4[6] : blockup5[6];
     assign blockup[7] = (TOPstate==4'b0001)? blockup1[7] :
                          (TOPstate==4'b0010)? blockup2[7] :
                          (TOPstate==4'b0011)? blockup3[7] : 
                          (TOPstate==4'b0100)? blockup4[7] : blockup5[7];
     assign blockup[8] = (TOPstate==4'b0001)? blockup1[8] :
                         (TOPstate==4'b0010)? blockup2[8] :
                        (TOPstate==4'b0011)? blockup3[8] : 
                        (TOPstate==4'b0100)? blockup4[8] : blockup5[8];
     assign blockup[9] = (TOPstate==4'b0001)? blockup1[9] :
                        (TOPstate==4'b0010)? blockup2[9] :
                       (TOPstate==4'b0011)? blockup3[9] : 
                       (TOPstate==4'b0100)? blockup4[9] : blockup5[9];   
   assign blockup[10] = (TOPstate==4'b0001)? blockup1[10] :
                           (TOPstate==4'b0010)? blockup2[10] :
                           (TOPstate==4'b0011)? blockup3[10] : 
                           (TOPstate==4'b0100)? blockup4[10] : blockup5[10];
      assign blockup[11] = (TOPstate==4'b0001)? blockup1[11] :
                           (TOPstate==4'b0010)? blockup2[11] :
                           (TOPstate==4'b0011)? blockup3[11] : 
                           (TOPstate==4'b0100)? blockup4[11] : blockup5[11];
       assign blockup[12] = (TOPstate==4'b0001)? blockup1[12] :
                            (TOPstate==4'b0010)? blockup2[12] :
                            (TOPstate==4'b0011)? blockup3[12] : 
                            (TOPstate==4'b0100)? blockup4[12] : blockup5[12];
       assign blockup[13] = (TOPstate==4'b0001)? blockup1[13] :
                           (TOPstate==4'b0010)? blockup2[13] :
                          (TOPstate==4'b0011)? blockup3[13] : 
                          (TOPstate==4'b0100)? blockup4[13] : blockup5[13];
       assign blockup[14] = (TOPstate==4'b0001)? blockup1[14] :
                          (TOPstate==4'b0010)? blockup2[14] :
                         (TOPstate==4'b0011)? blockup3[14] : 
                         (TOPstate==4'b0100)? blockup4[14] : blockup5[14];
        assign blockup[15] = (TOPstate==4'b0001)? blockup1[15] :
                             (TOPstate==4'b0010)? blockup2[15] :
                             (TOPstate==4'b0011)? blockup3[15] : 
                             (TOPstate==4'b0100)? blockup4[15] : blockup5[15];
        assign blockup[16] = (TOPstate==4'b0001)? blockup1[16] :
                             (TOPstate==4'b0010)? blockup2[16] :
                             (TOPstate==4'b0011)? blockup3[16] : 
                             (TOPstate==4'b0100)? blockup4[16] : blockup5[16];
         assign blockup[17] = (TOPstate==4'b0001)? blockup1[17] :
                              (TOPstate==4'b0010)? blockup2[17] :
                              (TOPstate==4'b0011)? blockup3[17] : 
                              (TOPstate==4'b0100)? blockup4[17] : blockup5[17];
         assign blockup[18] = (TOPstate==4'b0001)? blockup1[18] :
                             (TOPstate==4'b0010)? blockup2[18] :
                            (TOPstate==4'b0011)? blockup3[18] : 
                            (TOPstate==4'b0100)? blockup4[18] : blockup5[18];
         assign blockup[19] = (TOPstate==4'b0001)? blockup1[19] :
                            (TOPstate==4'b0010)? blockup2[19] :
                           (TOPstate==4'b0011)? blockup3[19] : 
                           (TOPstate==4'b0100)? blockup4[19] : blockup5[19];
    assign blockup[20] = (TOPstate==4'b0001)? blockup1[20] :
                       (TOPstate==4'b0010)? blockup2[20] :
                       (TOPstate==4'b0011)? blockup3[20] : 
                       (TOPstate==4'b0100)? blockup4[20] : blockup5[20];
  assign blockup[21] = (TOPstate==4'b0001)? blockup1[21] :
                       (TOPstate==4'b0010)? blockup2[21] :
                       (TOPstate==4'b0011)? blockup3[21] : 
                       (TOPstate==4'b0100)? blockup4[21] : blockup5[21];
   assign blockup[22] = (TOPstate==4'b0001)? blockup1[22] :
                        (TOPstate==4'b0010)? blockup2[22] :
                        (TOPstate==4'b0011)? blockup3[22] : 
                        (TOPstate==4'b0100)? blockup4[22] : blockup5[22];
   assign blockup[23] = (TOPstate==4'b0001)? blockup1[23] :
                       (TOPstate==4'b0010)? blockup2[23] :
                      (TOPstate==4'b0011)? blockup3[23] : 
                      (TOPstate==4'b0100)? blockup4[23] : blockup5[23];  
   assign blockleft[0] = (TOPstate==4'b0001)? blockleft1[0] :
                      (TOPstate==4'b0010)? blockleft2[0] :
                      (TOPstate==4'b0011)? blockleft3[0] : 
                      (TOPstate==4'b0100)? blockleft4[0] : blockleft5[0];
 assign blockleft[1] = (TOPstate==4'b0001)? blockleft1[1] :
                      (TOPstate==4'b0010)? blockleft2[1] :
                      (TOPstate==4'b0011)? blockleft3[1] : 
                      (TOPstate==4'b0100)? blockleft4[1] : blockleft5[1];
  assign blockleft[2] = (TOPstate==4'b0001)? blockleft1[2] :
                       (TOPstate==4'b0010)? blockleft2[2] :
                       (TOPstate==4'b0011)? blockleft3[2] : 
                       (TOPstate==4'b0100)? blockleft4[2] : blockleft5[2];
  assign blockleft[3] = (TOPstate==4'b0001)? blockleft1[3] :
                      (TOPstate==4'b0010)? blockleft2[3] :
                     (TOPstate==4'b0011)? blockleft3[3] : 
                     (TOPstate==4'b0100)? blockleft4[3] : blockleft5[3];
  assign blockleft[4] = (TOPstate==4'b0001)? blockleft1[4] :
                     (TOPstate==4'b0010)? blockleft2[4] :
                    (TOPstate==4'b0011)? blockleft3[4] : 
                    (TOPstate==4'b0100)? blockleft4[4] : blockleft5[4];
   assign blockleft[5] = (TOPstate==4'b0001)? blockleft1[5] :
                        (TOPstate==4'b0010)? blockleft2[5] :
                        (TOPstate==4'b0011)? blockleft3[5] : 
                        (TOPstate==4'b0100)? blockleft4[5] : blockleft5[5];
   assign blockleft[6] = (TOPstate==4'b0001)? blockleft1[6] :
                        (TOPstate==4'b0010)? blockleft2[6] :
                        (TOPstate==4'b0011)? blockleft3[6] : 
                        (TOPstate==4'b0100)? blockleft4[6] : blockleft5[6];
    assign blockleft[7] = (TOPstate==4'b0001)? blockleft1[7] :
                         (TOPstate==4'b0010)? blockleft2[7] :
                         (TOPstate==4'b0011)? blockleft3[7] : 
                         (TOPstate==4'b0100)? blockleft4[7] : blockleft5[7];
    assign blockleft[8] = (TOPstate==4'b0001)? blockleft1[8] :
                        (TOPstate==4'b0010)? blockleft2[8] :
                       (TOPstate==4'b0011)? blockleft3[8] : 
                       (TOPstate==4'b0100)? blockleft4[8] : blockleft5[8];
    assign blockleft[9] = (TOPstate==4'b0001)? blockleft1[9] :
                       (TOPstate==4'b0010)? blockleft2[9] :
                      (TOPstate==4'b0011)? blockleft3[9] : 
                      (TOPstate==4'b0100)? blockleft4[9] : blockleft5[9];   
  assign blockleft[10] = (TOPstate==4'b0001)? blockleft1[10] :
                          (TOPstate==4'b0010)? blockleft2[10] :
                          (TOPstate==4'b0011)? blockleft3[10] : 
                          (TOPstate==4'b0100)? blockleft4[10] : blockleft5[10];
     assign blockleft[11] = (TOPstate==4'b0001)? blockleft1[11] :
                          (TOPstate==4'b0010)? blockleft2[11] :
                          (TOPstate==4'b0011)? blockleft3[11] : 
                          (TOPstate==4'b0100)? blockleft4[11] : blockleft5[11];
      assign blockleft[12] = (TOPstate==4'b0001)? blockleft1[12] :
                           (TOPstate==4'b0010)? blockleft2[12] :
                           (TOPstate==4'b0011)? blockleft3[12] : 
                           (TOPstate==4'b0100)? blockleft4[12] : blockleft5[12];
      assign blockleft[13] = (TOPstate==4'b0001)? blockleft1[13] :
                          (TOPstate==4'b0010)? blockleft2[13] :
                         (TOPstate==4'b0011)? blockleft3[13] : 
                         (TOPstate==4'b0100)? blockleft4[13] : blockleft5[13];
      assign blockleft[14] = (TOPstate==4'b0001)? blockleft1[14] :
                         (TOPstate==4'b0010)? blockleft2[14] :
                        (TOPstate==4'b0011)? blockleft3[14] : 
                        (TOPstate==4'b0100)? blockleft4[14] : blockleft5[14];
       assign blockleft[15] = (TOPstate==4'b0001)? blockleft1[15] :
                            (TOPstate==4'b0010)? blockleft2[15] :
                            (TOPstate==4'b0011)? blockleft3[15] : 
                            (TOPstate==4'b0100)? blockleft4[15] : blockleft5[15];
       assign blockleft[16] = (TOPstate==4'b0001)? blockleft1[16] :
                            (TOPstate==4'b0010)? blockleft2[16] :
                            (TOPstate==4'b0011)? blockleft3[16] : 
                            (TOPstate==4'b0100)? blockleft4[16] : blockleft5[16];
        assign blockleft[17] = (TOPstate==4'b0001)? blockleft1[17] :
                             (TOPstate==4'b0010)? blockleft2[17] :
                             (TOPstate==4'b0011)? blockleft3[17] : 
                             (TOPstate==4'b0100)? blockleft4[17] : blockleft5[17];
        assign blockleft[18] = (TOPstate==4'b0001)? blockleft1[18] :
                            (TOPstate==4'b0010)? blockleft2[18] :
                           (TOPstate==4'b0011)? blockleft3[18] : 
                           (TOPstate==4'b0100)? blockleft4[18] : blockleft5[18];
        assign blockleft[19] = (TOPstate==4'b0001)? blockleft1[19] :
                           (TOPstate==4'b0010)? blockleft2[19] :
                          (TOPstate==4'b0011)? blockleft3[19] : 
                          (TOPstate==4'b0100)? blockleft4[19] : blockleft5[19];
   assign blockleft[20] = (TOPstate==4'b0001)? blockleft1[20] :
                      (TOPstate==4'b0010)? blockleft2[20] :
                      (TOPstate==4'b0011)? blockleft3[20] : 
                      (TOPstate==4'b0100)? blockleft4[20] : blockleft5[20];
 assign blockleft[21] = (TOPstate==4'b0001)? blockleft1[21] :
                      (TOPstate==4'b0010)? blockleft2[21] :
                      (TOPstate==4'b0011)? blockleft3[21] : 
                      (TOPstate==4'b0100)? blockleft4[21] : blockleft5[21];
  assign blockleft[22] = (TOPstate==4'b0001)? blockleft1[22] :
                       (TOPstate==4'b0010)? blockleft2[22] :
                       (TOPstate==4'b0011)? blockleft3[22] : 
                       (TOPstate==4'b0100)? blockleft4[22] : blockleft5[22];
  assign blockleft[23] = (TOPstate==4'b0001)? blockleft1[23] :
                      (TOPstate==4'b0010)? blockleft2[23] :
                     (TOPstate==4'b0011)? blockleft3[23] : 
                     (TOPstate==4'b0100)? blockleft4[23] : blockleft5[23];
   assign ball_h = (TOPstate==4'b0001)? ball_h1 :
                   (TOPstate==4'b0010)? ball_h2 :
                  (TOPstate==4'b0011)?  ball_h3 : 
                  (TOPstate==4'b0100)?  ball_h4 :  ball_h5; 
   assign ball_v = (TOPstate==4'b0001)? ball_v1 :
                  (TOPstate==4'b0010)? ball_v2 :
                 (TOPstate==4'b0011)?  ball_v3 : 
                 (TOPstate==4'b0100)?  ball_v4 :  ball_v5;
    assign light_h = (TOPstate==4'b0001)? light_h1 :
                    (TOPstate==4'b0010)? light_h2 :
                   (TOPstate==4'b0011)? light_h3 : 
                   (TOPstate==4'b0100)?  light_h4 : light_h5;                                                                                   
   assign charapixel1 = (character0==1 &&character1==0)? drapixel1 :
						(character0==0 &&character1==1)? bulpixel1 : 
						(character0==1 &&character1==1)? squpixel1 : pi1pixel;
   assign charapixel2 = (character0==1 &&character1==0)? drapixel2 :
						(character0==0 &&character1==1)? bulpixel2 : 
						(character0==1 &&character1==1)?  squpixel2 : pi2pixel;
   assign skillpixel = (character0==1 &&character1==0)? firepixel :
					   (character0==0 &&character1==1)? leafpixel : 
					   (character0==1 &&character1==1)? waterpixel : lightpixel;
   assign pixel_addr =  ((h_cnt>>1) +(v_cnt>>1)*320)%76800;
   assign pikapixel_addr = ((v_cnt>>1)>=200 && (h_cnt>>1)>=pikapos) ? ((h_cnt>>1)-pikapos + ((v_cnt>>1)-200)*40)%1600 : 17'd1;
   assign ballpixel_addr = ((v_cnt>>1)>=ball_v && (h_cnt>>1)>=ball_h) ? ((h_cnt>>1)-ball_h + ((v_cnt>>1)-ball_v)*10)%100 : 17'd1;
   assign lightpixel_addr = ((v_cnt>>1)>=0 && (h_cnt>>1)>=light_h) ? ((h_cnt>>1)-light_h + ((v_cnt>>1)*30))%6000 : 17'd1;
   assign NEXTpixel_addr = ((v_cnt>>1)>=100 && (h_cnt>>1)>=110) ? ((h_cnt>>1)-110 + ((v_cnt>>1)-100)*100)%4000 : 17'd1;
   assign pixel = ((h_cnt>>1)>=ball_h && (h_cnt>>1)<(ball_h+10) && (v_cnt>>1)>=ball_v && (v_cnt>>1)<(ball_v+10)) ? ballpixel :
                  ((h_cnt>>1)>=pikapos && (h_cnt>>1)<(pikapos+40) && (v_cnt>>1)>=200 && pikadir) ? charapixel1 :
                  ((h_cnt>>1)>=pikapos && (h_cnt>>1)<(pikapos+40) && (v_cnt>>1)>=200 && pikadir==1'b0) ? charapixel2 : 
                  ((h_cnt>>1)>=light_h && (h_cnt>>1)<(light_h+30) && (v_cnt>>1)<200 && showlight) ? skillpixel : 
                  ((h_cnt>>1)>=110 && (h_cnt>>1)<210 && (v_cnt>>1)>=100 && (v_cnt>>1)<140 && WIN && TOPstate!=4'b0101) ? NEXTpixel :
                  ((h_cnt>>1)>=110 && (h_cnt>>1)<210 && (v_cnt>>1)>=100 && (v_cnt>>1)<140 && WIN && TOPstate==4'b0101) ? winpixel :
                  ((h_cnt>>1)>=110 && (h_cnt>>1)<210 && (v_cnt>>1)>=100 && (v_cnt>>1)<140 && ending && !WIN) ? losepixel :
                  ((h_cnt>>1)>=blockleft[0] && ((h_cnt>>1)<blockleft[0]+47) && (v_cnt>>1)>=blockup[0] && ((v_cnt>>1)<blockup[0]+17) && block[0] && TOPstate==4'b0001) ? 12'hF00 :
                  ((h_cnt>>1)>=blockleft[1] && ((h_cnt>>1)<blockleft[1]+47) && (v_cnt>>1)>=blockup[1] && ((v_cnt>>1)<blockup[1]+17) && block[1] && TOPstate==4'b0001) ? 12'hAF6 :
                  ((h_cnt>>1)>=blockleft[2] && ((h_cnt>>1)<blockleft[2]+47) && (v_cnt>>1)>=blockup[2] && ((v_cnt>>1)<blockup[2]+17) && block[2] && TOPstate==4'b0001) ? 12'hF0F :
                  ((h_cnt>>1)>=blockleft[3] && ((h_cnt>>1)<blockleft[3]+47) && (v_cnt>>1)>=blockup[3] && ((v_cnt>>1)<blockup[3]+17) && block[3] && TOPstate==4'b0001) ? 12'h0F0 :
                  ((h_cnt>>1)>=blockleft[4] && ((h_cnt>>1)<blockleft[4]+47) && (v_cnt>>1)>=blockup[4] && ((v_cnt>>1)<blockup[4]+17) && block[4] && TOPstate==4'b0001) ? 12'hF13 :
                  ((h_cnt>>1)>=blockleft[5] && ((h_cnt>>1)<blockleft[5]+47) && (v_cnt>>1)>=blockup[5] && ((v_cnt>>1)<blockup[5]+17) && block[5] && TOPstate==4'b0001) ? 12'hF0F :
                  ((h_cnt>>1)>=blockleft[6] && ((h_cnt>>1)<blockleft[6]+47) && (v_cnt>>1)>=blockup[6] && ((v_cnt>>1)<blockup[6]+17) && block[6] && TOPstate==4'b0001) ? 12'hC5D :
                  ((h_cnt>>1)>=blockleft[7] && ((h_cnt>>1)<blockleft[7]+47) && (v_cnt>>1)>=blockup[7] && ((v_cnt>>1)<blockup[7]+17) && block[7] && TOPstate==4'b0001) ? 12'h05F :
                  ((h_cnt>>1)>=blockleft[8] && ((h_cnt>>1)<blockleft[8]+47) && (v_cnt>>1)>=blockup[8] && ((v_cnt>>1)<blockup[8]+17) && block[8] && TOPstate==4'b0001) ? 12'h85F :
                  ((h_cnt>>1)>=blockleft[9] && ((h_cnt>>1)<blockleft[9]+47) && (v_cnt>>1)>=blockup[9] && ((v_cnt>>1)<blockup[9]+17) && block[9] && TOPstate==4'b0001) ? 12'hFF6 :
                  ((h_cnt>>1)>=blockleft[10] && ((h_cnt>>1)<blockleft[10]+47) && (v_cnt>>1)>=blockup[10] && ((v_cnt>>1)<blockup[10]+17) && block[10] && TOPstate==4'b0001) ? 12'h85F :
                  ((h_cnt>>1)>=blockleft[11] && ((h_cnt>>1)<blockleft[11]+47) && (v_cnt>>1)>=blockup[11] && ((v_cnt>>1)<blockup[11]+17) && block[11] && TOPstate==4'b0001) ? 12'h9E5 :
                  ((h_cnt>>1)>=blockleft[12] && ((h_cnt>>1)<blockleft[12]+47) && (v_cnt>>1)>=blockup[12] && ((v_cnt>>1)<blockup[12]+17) && block[12] && TOPstate==4'b0001) ? 12'hFA3 :
                  ((h_cnt>>1)>=blockleft[13] && ((h_cnt>>1)<blockleft[13]+47) && (v_cnt>>1)>=blockup[13] && ((v_cnt>>1)<blockup[13]+17) && block[13] && TOPstate==4'b0001) ? 12'h8F0 :
                  ((h_cnt>>1)>=blockleft[14] && ((h_cnt>>1)<blockleft[14]+47) && (v_cnt>>1)>=blockup[14] && ((v_cnt>>1)<blockup[14]+17) && block[14] && TOPstate==4'b0001) ? 12'h6FF :
                  ((h_cnt>>1)>=blockleft[15] && ((h_cnt>>1)<blockleft[15]+47) && (v_cnt>>1)>=blockup[15] && ((v_cnt>>1)<blockup[15]+17) && block[15] && TOPstate==4'b0001) ? 12'hFB4 :
                  ((h_cnt>>1)>=blockleft[16] && ((h_cnt>>1)<blockleft[16]+47) && (v_cnt>>1)>=blockup[16] && ((v_cnt>>1)<blockup[16]+17) && block[16] && TOPstate==4'b0001) ? 12'h0F5 :
                  ((h_cnt>>1)>=blockleft[17] && ((h_cnt>>1)<blockleft[17]+47) && (v_cnt>>1)>=blockup[17] && ((v_cnt>>1)<blockup[17]+17) && block[17] && TOPstate==4'b0001) ? 12'h93F :
                  ((h_cnt>>1)>=blockleft[18] && ((h_cnt>>1)<blockleft[18]+47) && (v_cnt>>1)>=blockup[18] && ((v_cnt>>1)<blockup[18]+17) && block[18] && TOPstate==4'b0001) ? 12'hFF5 :
                  ((h_cnt>>1)>=blockleft[19] && ((h_cnt>>1)<blockleft[19]+47) && (v_cnt>>1)>=blockup[19] && ((v_cnt>>1)<blockup[19]+17) && block[19] && TOPstate==4'b0001) ? 12'h29F :
                  ((h_cnt>>1)>=blockleft[20] && ((h_cnt>>1)<blockleft[20]+47) && (v_cnt>>1)>=blockup[20] && ((v_cnt>>1)<blockup[20]+17) && block[20] && TOPstate==4'b0001) ? 12'hFF0 :
                  ((h_cnt>>1)>=blockleft[21] && ((h_cnt>>1)<blockleft[21]+47) && (v_cnt>>1)>=blockup[21] && ((v_cnt>>1)<blockup[21]+17) && block[21] && TOPstate==4'b0001) ? 12'h7F6 :
                  ((h_cnt>>1)>=blockleft[22] && ((h_cnt>>1)<blockleft[22]+47) && (v_cnt>>1)>=blockup[22] && ((v_cnt>>1)<blockup[22]+17) && block[22] && TOPstate==4'b0001) ? 12'h00F :
                  ((h_cnt>>1)>=blockleft[23] && ((h_cnt>>1)<blockleft[23]+47) && (v_cnt>>1)>=blockup[23] && ((v_cnt>>1)<blockup[23]+17) && block[23] && TOPstate==4'b0001) ? 12'hF00 : 
                  ((h_cnt>>1)>=blockleft[0] && ((h_cnt>>1)<blockleft[0]+28) && (v_cnt>>1)>=blockup[0] && ((v_cnt>>1)<blockup[0]+8) && block[0]) ? 12'hF00 :
                  ((h_cnt>>1)>=blockleft[1] && ((h_cnt>>1)<blockleft[1]+28) && (v_cnt>>1)>=blockup[1] && ((v_cnt>>1)<blockup[1]+8) && block[1]) ? 12'hAF6 :
                  ((h_cnt>>1)>=blockleft[2] && ((h_cnt>>1)<blockleft[2]+28) && (v_cnt>>1)>=blockup[2] && ((v_cnt>>1)<blockup[2]+8) && block[2]) ? 12'hF0F :
                  ((h_cnt>>1)>=blockleft[3] && ((h_cnt>>1)<blockleft[3]+28) && (v_cnt>>1)>=blockup[3] && ((v_cnt>>1)<blockup[3]+8) && block[3]) ? 12'h0F0 :
                  ((h_cnt>>1)>=blockleft[4] && ((h_cnt>>1)<blockleft[4]+28) && (v_cnt>>1)>=blockup[4] && ((v_cnt>>1)<blockup[4]+8) && block[4]) ? 12'hF13 :
                  ((h_cnt>>1)>=blockleft[5] && ((h_cnt>>1)<blockleft[5]+28) && (v_cnt>>1)>=blockup[5] && ((v_cnt>>1)<blockup[5]+8) && block[5]) ? 12'hF0F :
                  ((h_cnt>>1)>=blockleft[6] && ((h_cnt>>1)<blockleft[6]+28) && (v_cnt>>1)>=blockup[6] && ((v_cnt>>1)<blockup[6]+8) && block[6]) ? 12'hC5D :
                  ((h_cnt>>1)>=blockleft[7] && ((h_cnt>>1)<blockleft[7]+28) && (v_cnt>>1)>=blockup[7] && ((v_cnt>>1)<blockup[7]+8) && block[7]) ? 12'h05F :
                  ((h_cnt>>1)>=blockleft[8] && ((h_cnt>>1)<blockleft[8]+28) && (v_cnt>>1)>=blockup[8] && ((v_cnt>>1)<blockup[8]+8) && block[8]) ? 12'h85F :
                  ((h_cnt>>1)>=blockleft[9] && ((h_cnt>>1)<blockleft[9]+28) && (v_cnt>>1)>=blockup[9] && ((v_cnt>>1)<blockup[9]+8) && block[9]) ? 12'hFF6 :
                  ((h_cnt>>1)>=blockleft[10] && ((h_cnt>>1)<blockleft[10]+28) && (v_cnt>>1)>=blockup[10] && ((v_cnt>>1)<blockup[10]+8) && block[10]) ? 12'h85F :
                  ((h_cnt>>1)>=blockleft[11] && ((h_cnt>>1)<blockleft[11]+28) && (v_cnt>>1)>=blockup[11] && ((v_cnt>>1)<blockup[11]+8) && block[11]) ? 12'h9E5 :
                  ((h_cnt>>1)>=blockleft[12] && ((h_cnt>>1)<blockleft[12]+28) && (v_cnt>>1)>=blockup[12] && ((v_cnt>>1)<blockup[12]+8) && block[12]) ? 12'hFA3 :
                  ((h_cnt>>1)>=blockleft[13] && ((h_cnt>>1)<blockleft[13]+28) && (v_cnt>>1)>=blockup[13] && ((v_cnt>>1)<blockup[13]+8) && block[13]) ? 12'h8F0 :
                  ((h_cnt>>1)>=blockleft[14] && ((h_cnt>>1)<blockleft[14]+28) && (v_cnt>>1)>=blockup[14] && ((v_cnt>>1)<blockup[14]+8) && block[14]) ? 12'h6FF :
                  ((h_cnt>>1)>=blockleft[15] && ((h_cnt>>1)<blockleft[15]+28) && (v_cnt>>1)>=blockup[15] && ((v_cnt>>1)<blockup[15]+8) && block[15]) ? 12'hFB4 :
                  ((h_cnt>>1)>=blockleft[16] && ((h_cnt>>1)<blockleft[16]+28) && (v_cnt>>1)>=blockup[16] && ((v_cnt>>1)<blockup[16]+8) && block[16]) ? 12'h0F5 :
                  ((h_cnt>>1)>=blockleft[17] && ((h_cnt>>1)<blockleft[17]+28) && (v_cnt>>1)>=blockup[17] && ((v_cnt>>1)<blockup[17]+8) && block[17]) ? 12'h93F :
                  ((h_cnt>>1)>=blockleft[18] && ((h_cnt>>1)<blockleft[18]+28) && (v_cnt>>1)>=blockup[18] && ((v_cnt>>1)<blockup[18]+8) && block[18]) ? 12'hFF5 :
                  ((h_cnt>>1)>=blockleft[19] && ((h_cnt>>1)<blockleft[19]+28) && (v_cnt>>1)>=blockup[19] && ((v_cnt>>1)<blockup[19]+8) && block[19]) ? 12'h29F :
                  ((h_cnt>>1)>=blockleft[20] && ((h_cnt>>1)<blockleft[20]+28) && (v_cnt>>1)>=blockup[20] && ((v_cnt>>1)<blockup[20]+8) && block[20]) ? 12'hFF0 :
                  ((h_cnt>>1)>=blockleft[21] && ((h_cnt>>1)<blockleft[21]+28) && (v_cnt>>1)>=blockup[21] && ((v_cnt>>1)<blockup[21]+8) && block[21]) ? 12'h7F6 :
                  ((h_cnt>>1)>=blockleft[22] && ((h_cnt>>1)<blockleft[22]+28) && (v_cnt>>1)>=blockup[22] && ((v_cnt>>1)<blockup[22]+8) && block[22]) ? 12'h00F :
                  ((h_cnt>>1)>=blockleft[23] && ((h_cnt>>1)<blockleft[23]+28) && (v_cnt>>1)>=blockup[23] && ((v_cnt>>1)<blockup[23]+8) && block[23]) ? 12'hF00 : 12'h000;
    blk_mem_gen_0 blk_mem_gen_0_inst(
     .clka(clk),
     .wea(0),
     .addra(pikapixel_addr),
     .dina(data1[11:0]),
     .douta(pi1pixel)
   );
    blk_mem_gen_1 blk_mem_gen_1_inst(
     .clka(clk),
     .wea(0),
     .addra(pikapixel_addr),
     .dina(data2[11:0]),
     .douta(pi2pixel)
   ); 
     blk_mem_gen_2 blk_mem_gen_2_inst(
    .clka(clk),
    .wea(0),
    .addra(ballpixel_addr),
    .dina(data3[11:0]),
    .douta(ballpixel)
  );
    blk_mem_gen_3 blk_mem_gen_3_inst(
   .clka(clk),
   .wea(0),
   .addra(lightpixel_addr),
   .dina(data4[11:0]),
   .douta(lightpixel)
  );
    blk_mem_gen_4 blk_mem_gen_4_inst(
   .clka(clk),
   .wea(0),
   .addra(pixel_addr),
   .dina(data5[11:0]),
   .douta(backpixel)
  );
  blk_mem_gen_5 blk_mem_gen_5_inst(
 .clka(clk),
 .wea(0),
 .addra(pikapixel_addr),
 .dina(data6[11:0]),
 .douta(drapixel1)
);
  blk_mem_gen_6 blk_mem_gen_6_inst(
 .clka(clk),
 .wea(0),
 .addra(pikapixel_addr),
 .dina(data7[11:0]),
 .douta(drapixel2)
);
  blk_mem_gen_7 blk_mem_gen_7_inst(
 .clka(clk),
 .wea(0),
 .addra(lightpixel_addr),
 .dina(data8[11:0]),
 .douta(firepixel)
);
  blk_mem_gen_8 blk_mem_gen_8_inst(
 .clka(clk),
 .wea(0),
 .addra(pikapixel_addr),
 .dina(data9[11:0]),
 .douta(bulpixel1)
);
  blk_mem_gen_9 blk_mem_gen_9_inst(
 .clka(clk),
 .wea(0),
 .addra(pikapixel_addr),
 .dina(data10[11:0]),
 .douta(bulpixel2)
);
  blk_mem_gen_10 blk_mem_gen_10_inst(
 .clka(clk),
 .wea(0),
 .addra(lightpixel_addr),
 .dina(data11[11:0]),
 .douta(leafpixel)
);
  blk_mem_gen_11 blk_mem_gen_11_inst(
.clka(clk),
.wea(0),
.addra(NEXTpixel_addr),
.dina(data12[11:0]),
.douta(NEXTpixel)
);
 squblk SQU1(
 .pixel_addr1(pikapixel_addr),
 .pixel_addr2(pikapixel_addr),
 .lightpixel_addr(lightpixel_addr),
 .squpixel1(squpixel1),
 .squpixel2(squpixel2),
 .waterpixel(waterpixel)
 );
 winlose WL(
 .NEXTpixel_addr1(NEXTpixel_addr),
 .NEXTpixel_addr2(NEXTpixel_addr),
 .winpixel(winpixel),
 .losepixel(losepixel)
 );
endmodule
