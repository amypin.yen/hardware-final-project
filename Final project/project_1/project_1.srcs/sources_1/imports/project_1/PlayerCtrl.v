//
//
//
//

module PlayerCtrl (
	input clk,
	input reset,
	output reg [9:0] ibeat
);
parameter BEATLEAGTH = 575;

always @(posedge clk, posedge reset) begin
	if (reset)
		ibeat <= 0;
	else if (ibeat < BEATLEAGTH) 
		ibeat <= ibeat + 1;
	else 
		ibeat <= 0;
end

endmodule