`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2016/12/19 00:59:12
// Design Name: 
// Module Name: ballMoveS1
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ballMoveS5(input reset,
            input clk,
            input pause,
            input [9:0]pikapos,
            input [3:0]Top_state,
            input exit,
            input light,
            output reg[9:0]ball_h,
            output reg[9:0]ball_v,
            output [23:0]block,
            output ending,
            output [10:0]score,
            output [9:0]LED,
            output showlight,
            output [9:0]light_h,
            output win   
    );
    
   wire [9:0] blockup [0:23]={
        10'd10,10'd20,10'd20,10'd30,10'd30,10'd30,10'd30,10'd30
        ,10'd40,10'd40,10'd40,10'd40,10'd40,10'd40,10'd40,10'd40
        ,10'd50,10'd50,10'd50,10'd50,10'd50,10'd60,10'd60,10'd70
        };
    wire [9:0] blockleft [0:23]={
        10'd205,10'd175,10'd205,10'd55,10'd145,10'd175,10'd205,10'd235,10'd25
        ,10'd85,10'd115,10'd145,10'd175,10'd205,10'd235,10'd265,10'd55,10'd145
        ,10'd175,10'd205,10'd235,10'd175,10'd205,10'd205
        };
    reg [2:0]state,n_state;
    reg [9:0] n_h,n_v;

    parameter [2:0]RIGHTUP = 3'b000;
    parameter [2:0]LEFTUP = 3'b001;
    parameter [2:0]RIGHTDOWN = 3'b010;
    parameter [2:0]LEFTDOWN = 3'b011;
    parameter [2:0]END = 3'b100;
    
    reg [5:0]count,n_count;
    reg isplay;
    reg [23:0] n_block;
    reg [9:0] n_LED;
    Lightning5 L1(.clk(clk),
              .reset(reset),
              .LED(n_LED),
              .isplay(isplay),
              .pikapos(pikapos),
              .block(n_block),
              .light(light),
              .light_h(light_h),
              .Lightblock(block),
              .LightLED(LED),
              .exit(exit),
              .Top_state(Top_state),
              .showlight(showlight)
        );
    assign win = ~(|block);
    assign ending = (state == END)? 1:0;
    assign score = (24-(block[0]+block[1]+block[2]+block[3]+block[4]+block[5]+block[6]+block[7]+block[8]
                    +block[9]+block[10]+block[11] +block[12] +block[13] + block[14]+block[15] +block[16] 
                    +block[17] +block[18]+block[19] +block[20]+block[21]+block[22]+block[23]))*10;
    
    always@(posedge reset or posedge pause or posedge exit)begin
        if(reset || exit||Top_state!=4'b0101) isplay <= 0;
        else isplay <= ~isplay;
    end
    
    always @(posedge clk or posedge reset or posedge exit)begin
        if (reset ||exit||Top_state!=4'b0101)begin
            state <= RIGHTUP;
            ball_h <= 160;
            ball_v <= 165;
            count <= 0;
        end
        else begin
            state <= n_state;
            ball_h <= n_h;
            ball_v <= n_v;
            count <= n_count;
        end
    end
    
    always @(*)begin: define_loop
        if (isplay)begin     
            n_block <= block;
            n_count <= count;
            case(state)
                RIGHTUP:begin
                    n_state <= RIGHTUP;
                    n_h <= ball_h +1;
                    n_v <= ball_v -1;
                    n_LED <= LED;
                    if (block[0])begin
                        if (ball_v == blockup[0]+10&&ball_h+10== blockleft[0])begin//corner
                            n_block[0]<=0;
                            n_state <= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v == blockup[0] +10 && //bottom
                            ((ball_h+10 >blockleft[0] && ball_h+10<blockleft[0] +30)||
                            (ball_h >blockleft[0] && ball_h <blockleft[0] +30)))begin
                            n_block[0] <= 0;
                            n_state<= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h+10==blockleft[0]&& //lrft
                            ((ball_v+10>blockup[0] && ball_v+10<=blockup[0]+10)||
                            (ball_v>=blockup[0] && ball_v <blockup[0]+10)))begin
                            n_block[0] <=0;
                            n_state <= LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[1])begin
                        if (ball_v == blockup[1]+10&&ball_h+10== blockleft[1])begin
                            n_block[1]<=0;
                            n_state <= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v == blockup[1] +10 && //bottom
                            ((ball_h+10 >blockleft[1] && ball_h+10<blockleft[1] +30)||
                            (ball_h >blockleft[1] && ball_h <blockleft[1] +30)))begin
                            n_block[1] <= 0;
                            n_state<= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h+10==blockleft[1]&& 
                            ((ball_v+10>blockup[1] && ball_v+10<=blockup[1]+10)||
                            (ball_v>=blockup[1] && ball_v <blockup[1]+10)))begin
                            n_block[1] <=0;
                            n_state <= LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[2])begin
                        if (ball_v == blockup[2]+10&&ball_h+10== blockleft[2])begin
                            n_block[2]<=0;
                            n_state <= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v == blockup[2] +10 && //bottom
                            ((ball_h+10 >blockleft[2] && ball_h+10<blockleft[2] +30)||
                            (ball_h >blockleft[2] && ball_h <blockleft[2] +30)))begin
                            n_block[2] <= 0;
                            n_state<= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h+10==blockleft[2]&& 
                            ((ball_v+10>blockup[2] && ball_v+10<=blockup[2]+10)||
                            (ball_v>=blockup[2] && ball_v <blockup[2]+10)))begin
                            n_block[2] <=0;
                            n_state <= LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[3])begin
                        if (ball_v == blockup[3]+10&&ball_h+10== blockleft[3])begin
                            n_block[3]<=0;
                            n_state <= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v == blockup[3] +10 && //bottom
                            ((ball_h+10 >blockleft[3] && ball_h+10<blockleft[3] +30)||
                            (ball_h >blockleft[3] && ball_h <blockleft[3] +30)))begin
                            n_block[3] <= 0;
                            n_state<= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h+10==blockleft[3]&& 
                            ((ball_v+10>blockup[3] && ball_v+10<=blockup[3]+10)||
                            (ball_v>=blockup[3] && ball_v <blockup[3]+10)))begin
                            n_block[3] <=0;
                            n_state <= LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[4])begin
                        if (ball_v == blockup[4]+10&&ball_h+10== blockleft[4])begin
                            n_block[4]<=0;
                            n_state <= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v == blockup[4] +10 && //bottom
                            ((ball_h+10 >blockleft[4] && ball_h+10<blockleft[4] +30)||
                            (ball_h >blockleft[4] && ball_h <blockleft[4] +30)))begin
                            n_block[4] <= 0;
                            n_state<= RIGHTDOWN;
                             if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h+10==blockleft[4]&& 
                            ((ball_v+10>blockup[4] && ball_v+10<=blockup[4]+10)||
                            (ball_v>=blockup[4] && ball_v <blockup[4]+10)))begin
                            n_block[4] <=0;
                            n_state <= LEFTUP;
                             if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[5])begin
                        if (ball_v == blockup[5]+10&&ball_h+10== blockleft[5])begin
                            n_block[5]<=0;
                            n_state <= LEFTDOWN;
                             if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v == blockup[5] +10 && //bottom
                            ((ball_h+10 >blockleft[5] && ball_h+10<blockleft[5] +30)||
                            (ball_h >blockleft[5] && ball_h <blockleft[5] +30)))begin
                            n_block[5] <= 0;
                            n_state<= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h+10==blockleft[5]&& 
                            ((ball_v+10>blockup[5] && ball_v+10<=blockup[5]+10)||
                            (ball_v>=blockup[5] && ball_v <blockup[5]+10)))begin
                            n_block[5] <=0;
                            n_state <= LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[6])begin
                        if (ball_v == blockup[6]+10&&ball_h+10== blockleft[6])begin
                            n_block[6]<=0;
                            n_state <= LEFTDOWN;
                             if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v == blockup[6] +10 && //bottom
                            ((ball_h+10 >blockleft[6] && ball_h+10<blockleft[6] +30)||
                            (ball_h >blockleft[6] && ball_h <blockleft[6] +30)))begin
                            n_block[6] <= 0;
                            n_state<= RIGHTDOWN;
                             if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h+10==blockleft[6]&& 
                            ((ball_v+10>blockup[6] && ball_v+10<=blockup[6]+10)||
                            (ball_v>=blockup[6] && ball_v <blockup[6]+10)))begin
                            n_block[6] <=0;
                            n_state <= LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[7])begin
                        if (ball_v == blockup[7]+10&&ball_h+10== blockleft[7])begin//corner
                            n_block[7]<=0;
                            n_state <= LEFTDOWN;
                             if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v == blockup[7] +10 && //bottom
                            ((ball_h+10 >blockleft[7] && ball_h+10<blockleft[7] +30)||
                            (ball_h >blockleft[7] && ball_h <blockleft[7] +30)))begin
                            n_block[7] <= 0;
                            n_state<= RIGHTDOWN;
                             if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h+10==blockleft[7]&& //lrft
                            ((ball_v+10>blockup[7] && ball_v+10<=blockup[7]+10)||
                            (ball_v>=blockup[7] && ball_v <blockup[7]+10)))begin
                            n_block[7] <=0;
                            n_state <= LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[8])begin
                        if (ball_v == blockup[8]+10&&ball_h+10== blockleft[8])begin
                            n_block[8]<=0;
                            n_state <= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v == blockup[8] +10 && //bottom
                            ((ball_h+10 >blockleft[8] && ball_h+10<blockleft[8] +30)||
                            (ball_h >blockleft[8] && ball_h <blockleft[8] +30)))begin
                            n_block[8] <= 0;
                            n_state<= RIGHTDOWN;
                             if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h+10==blockleft[8]&& 
                            ((ball_v+10>blockup[8] && ball_v+10<=blockup[8]+10)||
                            (ball_v>=blockup[8] && ball_v <blockup[8]+10)))begin
                            n_block[8] <=0;
                            n_state <= LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[9])begin
                        if (ball_v == blockup[9]+10&&ball_h+10== blockleft[9])begin
                            n_block[9]<=0;
                            n_state <= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v == blockup[9] +10 && //bottom
                            ((ball_h+10 >blockleft[9] && ball_h+10<blockleft[9] +30)||
                            (ball_h >blockleft[9] && ball_h <blockleft[9] +30)))begin
                            n_block[9] <= 0;
                            n_state<= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h+10==blockleft[9]&& 
                            ((ball_v+10>blockup[9] && ball_v+10<=blockup[9]+10)||
                            (ball_v>=blockup[9] && ball_v <blockup[9]+10)))begin
                            n_block[9] <=0;
                            n_state <= LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[10])begin
                        if (ball_v == blockup[10]+10&&ball_h+10== blockleft[10])begin
                            n_block[10]<=0;
                            n_state <= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v == blockup[10] +10 && //bottom
                            ((ball_h+10 >blockleft[10] && ball_h+10<blockleft[10] +30)||
                            (ball_h >blockleft[10] && ball_h <blockleft[10] +30)))begin
                            n_block[10] <= 0;
                            n_state<= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h+10==blockleft[10]&& 
                            ((ball_v+10>blockup[10] && ball_v+10<=blockup[10]+10)||
                            (ball_v>=blockup[10] && ball_v <blockup[10]+10)))begin
                            n_block[10] <=0;
                            n_state <= LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[11])begin
                        if (ball_v == blockup[11]+10&&ball_h+10== blockleft[11])begin
                            n_block[11]<=0;
                            n_state <= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v == blockup[11] +10 && //bottom
                            ((ball_h+10 >blockleft[11] && ball_h+10<blockleft[11] +30)||
                            (ball_h >blockleft[11] && ball_h <blockleft[11] +30)))begin
                            n_block[11] <= 0;
                            n_state<= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h+10==blockleft[11]&& 
                            ((ball_v+10>blockup[11] && ball_v+10<=blockup[11]+10)||
                            (ball_v>=blockup[11] && ball_v <blockup[11]+10)))begin
                            n_block[11] <=0;
                            n_state <= LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[12])begin
                        if (ball_v == blockup[12]+10&&ball_h+10== blockleft[12])begin
                            n_block[12]<=0;
                            n_state <= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v == blockup[12] +10 && //bottom
                            ((ball_h+10 >blockleft[12] && ball_h+10<blockleft[12] +30)||
                            (ball_h >blockleft[12] && ball_h <blockleft[12] +30)))begin
                            n_block[12] <= 0;
                            n_state<= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h+10==blockleft[12]&& 
                            ((ball_v+10>blockup[12] && ball_v+10<=blockup[12]+10)||
                            (ball_v>=blockup[12] && ball_v <blockup[12]+10)))begin
                            n_block[12] <=0;
                            n_state <= LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[13])begin
                        if (ball_v == blockup[13]+10&&ball_h+10== blockleft[13])begin
                            n_block[13]<=0;
                            n_state <= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v == blockup[13] +10 && //bottom
                            ((ball_h+10 >blockleft[13] && ball_h+10<blockleft[13] +30)||
                            (ball_h >blockleft[13] && ball_h <blockleft[13] +30)))begin
                            n_block[13] <= 0;
                            n_state<= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h+10==blockleft[13]&& 
                            ((ball_v+10>blockup[13] && ball_v+10<=blockup[13]+10)||
                            (ball_v>=blockup[13] && ball_v <blockup[13]+10)))begin
                            n_block[13] <=0;
                            n_state <= LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[14])begin
                        if (ball_v == blockup[14]+10&&ball_h+10== blockleft[14])begin
                            n_block[14]<=0;
                            n_state <= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v == blockup[14] +10 && //bottom
                            ((ball_h+10 >blockleft[14] && ball_h+10<blockleft[14] +30)||
                            (ball_h >blockleft[14] && ball_h <blockleft[14] +30)))begin
                            n_block[14] <= 0;
                            n_state<= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h+10==blockleft[14]&& 
                            ((ball_v+10>blockup[14] && ball_v+10<=blockup[14]+10)||
                            (ball_v>=blockup[14] && ball_v <blockup[14]+10)))begin
                            n_block[14] <=0;
                            n_state <= LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[15])begin
                        if (ball_v == blockup[15]+10&&ball_h+10== blockleft[15])begin//corner
                            n_block[15]<=0;
                            n_state <= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v == blockup[15] +10 && //bottom
                            ((ball_h+10 >blockleft[15] && ball_h+10<blockleft[15] +30)||
                            (ball_h >blockleft[15] && ball_h <blockleft[15] +30)))begin
                            n_block[15] <= 0;
                            n_state<= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h+10==blockleft[15]&& //lrft
                            ((ball_v+10>blockup[15] && ball_v+10<=blockup[15]+10)||
                            (ball_v>=blockup[15] && ball_v <blockup[15]+10)))begin
                            n_block[15] <=0;
                            n_state <= LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[16])begin
                        if (ball_v == blockup[16]+10&&ball_h+10== blockleft[16])begin
                            n_block[16]<=0;
                            n_state <= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v == blockup[16] +10 && //bottom
                            ((ball_h+10 >blockleft[16] && ball_h+10<blockleft[16] +30)||
                            (ball_h >blockleft[16] && ball_h <blockleft[16] +30)))begin
                            n_block[16] <= 0;
                            n_state<= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h+10==blockleft[16]&& 
                            ((ball_v+10>blockup[16] && ball_v+10<=blockup[16]+10)||
                            (ball_v>=blockup[16] && ball_v <blockup[16]+10)))begin
                            n_block[16] <=0;
                            n_state <= LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[17])begin
                        if (ball_v == blockup[17]+10&&ball_h+10== blockleft[17])begin
                            n_block[17]<=0;
                            n_state <= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v == blockup[17] +10 && //bottom
                            ((ball_h+10 >blockleft[17] && ball_h+10<blockleft[17] +30)||
                            (ball_h >blockleft[17] && ball_h <blockleft[17] +30)))begin
                            n_block[17] <= 0;
                            n_state<= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h+10==blockleft[17]&& 
                            ((ball_v+10>blockup[17] && ball_v+10<=blockup[17]+10)||
                            (ball_v>=blockup[17] && ball_v <blockup[17]+10)))begin
                            n_block[17] <=0;
                            n_state <= LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[18])begin
                        if (ball_v == blockup[18]+10&&ball_h+10== blockleft[18])begin
                            n_block[18]<=0;
                            n_state <= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v == blockup[18] +10 && //bottom
                            ((ball_h+10 >blockleft[18] && ball_h+10<blockleft[18] +30)||
                            (ball_h >blockleft[18] && ball_h <blockleft[18] +30)))begin
                            n_block[18] <= 0;
                            n_state<= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h+10==blockleft[18]&& 
                            ((ball_v+10>blockup[18] && ball_v+10<=blockup[18]+10)||
                            (ball_v>=blockup[18] && ball_v <blockup[18]+10)))begin
                            n_block[18] <=0;
                            n_state <= LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[19])begin
                        if (ball_v == blockup[19]+10&&ball_h+10== blockleft[19])begin
                            n_block[19]<=0;
                            n_state <= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v == blockup[19] +10 && //bottom
                            ((ball_h+10 >blockleft[19] && ball_h+10<blockleft[19] +30)||
                            (ball_h >blockleft[19] && ball_h <blockleft[19] +30)))begin
                            n_block[19] <= 0;
                            n_state<= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h+10==blockleft[19]&& 
                            ((ball_v+10>blockup[19] && ball_v+10<=blockup[19]+10)||
                            (ball_v>=blockup[19] && ball_v <blockup[19]+10)))begin
                            n_block[19] <=0;
                            n_state <= LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[20])begin
                        if (ball_v == blockup[20]+10&&ball_h+10== blockleft[20])begin
                            n_block[20]<=0;
                            n_state <= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v == blockup[20] +10 && //bottom
                            ((ball_h+10 >blockleft[20] && ball_h+10<blockleft[20] +30)||
                            (ball_h >blockleft[20] && ball_h <blockleft[20] +30)))begin
                            n_block[20] <= 0;
                            n_state<= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h+10==blockleft[20]&& 
                            ((ball_v+10>blockup[20] && ball_v+10<=blockup[20]+10)||
                            (ball_v>=blockup[20] && ball_v <blockup[20]+10)))begin
                            n_block[20] <=0;
                            n_state <= LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[21])begin
                        if (ball_v == blockup[21]+10&&ball_h+10== blockleft[21])begin
                            n_block[21]<=0;
                            n_state <= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v == blockup[21] +10 && //bottom
                            ((ball_h+10 >blockleft[21] && ball_h+10<blockleft[21] +30)||
                            (ball_h >blockleft[21] && ball_h <blockleft[21] +30)))begin
                            n_block[21] <= 0;
                            n_state<= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h+10==blockleft[21]&& 
                            ((ball_v+10>blockup[21] && ball_v+10<=blockup[21]+10)||
                            (ball_v>=blockup[21] && ball_v <blockup[21]+10)))begin
                            n_block[21] <=0;
                            n_state <= LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[22])begin
                        if (ball_v == blockup[22]+10&&ball_h+10== blockleft[22])begin
                            n_block[22]<=0;
                            n_state <= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v == blockup[22] +10 && //bottom
                            ((ball_h+10 >blockleft[22] && ball_h+10<blockleft[22] +30)||
                            (ball_h >blockleft[22] && ball_h <blockleft[22] +30)))begin
                            n_block[22] <= 0;
                            n_state<= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h+10==blockleft[22]&& 
                            ((ball_v+10>blockup[22] && ball_v+10<=blockup[22]+10)||
                            (ball_v>=blockup[22] && ball_v <blockup[22]+10)))begin
                            n_block[22] <=0;
                            n_state <= LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[23])begin
                        if (ball_v == blockup[23]+10&&ball_h+10== blockleft[23])begin
                            n_block[23]<=0;
                            n_state <= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v == blockup[23] +10 && //bottom
                            ((ball_h+10 >blockleft[23] && ball_h+10<blockleft[23] +30)||
                            (ball_h >blockleft[23] && ball_h <blockleft[23] +30)))begin
                            n_block[23] <= 0;
                            n_state<= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h+10==blockleft[23]&& 
                            ((ball_v+10>blockup[23] && ball_v+10<=blockup[23]+10)||
                            (ball_v>=blockup[23] && ball_v <blockup[23]+10)))begin
                            n_block[23] <=0;
                            n_state <= LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end  
                    if (win) n_state <= END;     
                    else if (n_h ==10'd310 && n_v ==10'd0) n_state <= LEFTDOWN;
                    else if (n_h >= 10'd310) n_state <= LEFTUP;
                    else if (n_v == 10'd0||n_v==1023) n_state <= RIGHTDOWN; 
                end
                LEFTUP:begin
                    n_state <= LEFTUP;
                    n_h <= ball_h -1;
                    n_v <= ball_v -1;
                    n_LED <= LED;
                    if (block[0])begin
                        if (ball_v == blockup[0]+10&&ball_h == blockleft[0]+30)begin//corner
                            n_block[0]<=0;
                            n_state <= RIGHTDOWN;
                             if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v == blockup[0] +10 && //bottom
                            ((ball_h+10 >blockleft[0] && ball_h+10<blockleft[0] +30)||
                            (ball_h >blockleft[0] && ball_h <blockleft[0] +30)))begin
                            n_block[0] <= 0;
                            n_state<= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h ==blockleft[0]+30&& //right
                            ((ball_v+10>blockup[0] && ball_v+10<=blockup[0]+10)||
                            (ball_v>=blockup[0] && ball_v <blockup[0]+10)))begin
                            n_block[0] <=0;
                            n_state <= RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[1])begin
                        if (ball_v == blockup[1]+10&&ball_h == blockleft[1]+30)begin//corner
                            n_block[1]<=0;
                            n_state <= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v == blockup[1] +10 && //bottom
                            ((ball_h+10 >blockleft[1] && ball_h+10<blockleft[1] +30)||
                            (ball_h >blockleft[1] && ball_h <blockleft[1] +30)))begin
                            n_block[1] <= 0;
                            n_state<= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h ==blockleft[1]+30&& //right
                            ((ball_v+10>blockup[1] && ball_v+10<=blockup[1]+10)||
                            (ball_v>=blockup[1] && ball_v <blockup[1]+10)))begin
                            n_block[1] <=0;
                            n_state <= RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[2])begin
                        if (ball_v == blockup[2]+10&&ball_h == blockleft[2]+30)begin//corner
                            n_block[2]<=0;
                            n_state <= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v == blockup[2] +10 && //bottom
                            ((ball_h+10 >blockleft[2] && ball_h+10<blockleft[2] +30)||
                            (ball_h >blockleft[2] && ball_h <blockleft[2] +30)))begin
                            n_block[2] <= 0;
                            n_state<= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h ==blockleft[2]+30&& //right
                            ((ball_v+10>blockup[2] && ball_v+10<=blockup[2]+10)||
                            (ball_v>=blockup[2] && ball_v <blockup[2]+10)))begin
                            n_block[2] <=0;
                            n_state <= RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[3])begin
                        if (ball_v == blockup[3]+10&&ball_h == blockleft[3]+30)begin//corner
                            n_block[3]<=0;
                            n_state <= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v == blockup[3] +10 && //bottom
                            ((ball_h+10 >blockleft[3] && ball_h+10<blockleft[3] +30)||
                            (ball_h >blockleft[3] && ball_h <blockleft[3] +30)))begin
                            n_block[3] <= 0;
                            n_state<= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h ==blockleft[3]+30&& //right
                            ((ball_v+10>blockup[3] && ball_v+10<=blockup[3]+10)||
                            (ball_v>=blockup[3] && ball_v <blockup[3]+10)))begin
                            n_block[3] <=0;
                            n_state <= RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[4])begin
                        if (ball_v == blockup[4]+10&&ball_h == blockleft[4]+30)begin//corner
                            n_block[4]<=0;
                            n_state <= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v == blockup[4] +10 && //bottom
                            ((ball_h+10 >blockleft[4] && ball_h+10<blockleft[4] +30)||
                            (ball_h >blockleft[4] && ball_h <blockleft[4] +30)))begin
                            n_block[4] <= 0;
                            n_state<= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h ==blockleft[4]+30&& //right
                            ((ball_v+10>blockup[4] && ball_v+10<=blockup[4]+10)||
                            (ball_v>=blockup[4] && ball_v <blockup[4]+10)))begin
                            n_block[4] <=0;
                            n_state <= RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[5])begin
                        if (ball_v == blockup[5]+10&&ball_h == blockleft[5]+30)begin//corner
                            n_block[5]<=0;
                            n_state <= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v == blockup[5] +10 && //bottom
                            ((ball_h+10 >blockleft[5] && ball_h+10<blockleft[5] +30)||
                            (ball_h >blockleft[5] && ball_h <blockleft[5] +30)))begin
                            n_block[5] <= 0;
                            n_state<= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h ==blockleft[5]+30&& //right
                            ((ball_v+10>blockup[5] && ball_v+10<=blockup[5]+10)||
                            (ball_v>=blockup[5] && ball_v <blockup[5]+10)))begin
                            n_block[5] <=0;
                            n_state <= RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[6])begin
                        if (ball_v == blockup[6]+10&&ball_h == blockleft[6]+30)begin//corner
                            n_block[6]<=0;
                            n_state <= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v == blockup[6] +10 && //bottom
                            ((ball_h+10 >blockleft[6] && ball_h+10<blockleft[6] +30)||
                            (ball_h >blockleft[6] && ball_h <blockleft[6] +30)))begin
                            n_block[6] <= 0;
                            n_state<= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h ==blockleft[6]+30&& //right
                            ((ball_v+10>blockup[6] && ball_v+10<=blockup[6]+10)||
                            (ball_v>=blockup[6] && ball_v <blockup[6]+10)))begin
                            n_block[6] <=0;
                            n_state <= RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[7])begin
                        if (ball_v == blockup[7]+10&&ball_h == blockleft[7]+30)begin//corner
                            n_block[7]<=0;
                            n_state <= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v == blockup[7] +10 && //bottom
                            ((ball_h+10 >blockleft[7] && ball_h+10<blockleft[7] +30)||
                            (ball_h >blockleft[7] && ball_h <blockleft[7] +30)))begin
                            n_block[7] <= 0;
                            n_state<= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h ==blockleft[7]+30&& //right
                            ((ball_v+10>blockup[7] && ball_v+10<=blockup[7]+10)||
                            (ball_v>=blockup[7] && ball_v <blockup[7]+10)))begin
                            n_block[7] <=0;
                            n_state <= RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[8])begin
                        if (ball_v == blockup[8]+10&&ball_h == blockleft[8]+30)begin//corner
                            n_block[8]<=0;
                            n_state <= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v == blockup[8] +10 && //bottom
                            ((ball_h+10 >blockleft[8] && ball_h+10<blockleft[8] +30)||
                            (ball_h >blockleft[8] && ball_h <blockleft[8] +30)))begin
                            n_block[8] <= 0;
                            n_state<= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h ==blockleft[8]+30&& //right
                            ((ball_v+10>blockup[8] && ball_v+10<=blockup[8]+10)||
                            (ball_v>=blockup[8] && ball_v <blockup[8]+10)))begin
                            n_block[8] <=0;
                            n_state <= RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[9])begin
                        if (ball_v == blockup[9]+10&&ball_h == blockleft[9]+30)begin//corner
                            n_block[9]<=0;
                            n_state <= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v == blockup[9] +10 && //bottom
                            ((ball_h+10 >blockleft[9] && ball_h+10<blockleft[9] +30)||
                            (ball_h >blockleft[9] && ball_h <blockleft[9] +30)))begin
                            n_block[9] <= 0;
                            n_state<= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h ==blockleft[9]+30&& //right
                            ((ball_v+10>blockup[9] && ball_v+10<=blockup[9]+10)||
                            (ball_v>=blockup[9] && ball_v <blockup[9]+10)))begin
                            n_block[9] <=0;
                            n_state <= RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[10])begin
                        if (ball_v == blockup[10]+10&&ball_h == blockleft[10]+30)begin//corner
                            n_block[10]<=0;
                            n_state <= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v == blockup[10] +10 && //bottom
                            ((ball_h+10 >blockleft[10] && ball_h+10<blockleft[10] +30)||
                            (ball_h >blockleft[10] && ball_h <blockleft[10] +30)))begin
                            n_block[10] <= 0;
                            n_state<= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h ==blockleft[10]+30&& //right
                            ((ball_v+10>blockup[10] && ball_v+10<=blockup[10]+10)||
                            (ball_v>=blockup[10] && ball_v <blockup[10]+10)))begin
                            n_block[10] <=0;
                            n_state <= RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[11])begin
                        if (ball_v == blockup[11]+10&&ball_h == blockleft[11]+30)begin//corner
                            n_block[11]<=0;
                            n_state <= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v == blockup[11] +10 && //bottom
                            ((ball_h+10 >blockleft[11] && ball_h+10<blockleft[11] +30)||
                            (ball_h >blockleft[11] && ball_h <blockleft[11] +30)))begin
                            n_block[11] <= 0;
                            n_state<= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h ==blockleft[11]+30&& //right
                            ((ball_v+10>blockup[11] && ball_v+10<=blockup[11]+10)||
                            (ball_v>=blockup[11] && ball_v <blockup[11]+10)))begin
                            n_block[11] <=0;
                            n_state <= RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[12])begin
                        if (ball_v == blockup[12]+10&&ball_h == blockleft[12]+30)begin//corner
                            n_block[12]<=0;
                            n_state <= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v == blockup[12] +10 && //bottom
                            ((ball_h+10 >blockleft[12] && ball_h+10<blockleft[12] +30)||
                            (ball_h >blockleft[12] && ball_h <blockleft[12] +30)))begin
                            n_block[12] <= 0;
                            n_state<= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h ==blockleft[12]+30&& //right
                            ((ball_v+10>blockup[12] && ball_v+10<=blockup[12]+10)||
                            (ball_v>=blockup[12] && ball_v <blockup[12]+10)))begin
                            n_block[12] <=0;
                            n_state <= RIGHTUP;
                             if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[13])begin
                        if (ball_v == blockup[13]+10&&ball_h == blockleft[13]+30)begin//corner
                            n_block[13]<=0;
                            n_state <= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v == blockup[13] +10 && //bottom
                            ((ball_h+10 >blockleft[13] && ball_h+10<blockleft[13] +30)||
                            (ball_h >blockleft[13] && ball_h <blockleft[13] +30)))begin
                            n_block[13] <= 0;
                            n_state<= LEFTDOWN;
                             if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h ==blockleft[13]+30&& //right
                            ((ball_v+10>blockup[13] && ball_v+10<=blockup[13]+10)||
                            (ball_v>=blockup[13] && ball_v <blockup[13]+10)))begin
                            n_block[13] <=0;
                            n_state <= RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[14])begin
                        if (ball_v == blockup[14]+10&&ball_h == blockleft[14]+30)begin//corner
                            n_block[14]<=0;
                            n_state <= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v == blockup[14] +10 && //bottom
                            ((ball_h+10 >blockleft[14] && ball_h+10<blockleft[14] +30)||
                            (ball_h >blockleft[14] && ball_h <blockleft[14] +30)))begin
                            n_block[14] <= 0;
                            n_state<= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h ==blockleft[14]+30&& //right
                            ((ball_v+10>blockup[14] && ball_v+10<=blockup[14]+10)||
                            (ball_v>=blockup[14] && ball_v <blockup[14]+10)))begin
                            n_block[14] <=0;
                            n_state <= RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[15])begin
                        if (ball_v == blockup[15]+10&&ball_h == blockleft[15]+30)begin//corner
                            n_block[15]<=0;
                            n_state <= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v == blockup[15] +10 && //bottom
                            ((ball_h+10 >blockleft[15] && ball_h+10<blockleft[15] +30)||
                            (ball_h >blockleft[15] && ball_h <blockleft[15] +30)))begin
                            n_block[15] <= 0;
                            n_state<= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h ==blockleft[15]+30&& //right
                            ((ball_v+10>blockup[15] && ball_v+10<=blockup[15]+10)||
                            (ball_v>=blockup[15] && ball_v <blockup[15]+10)))begin
                            n_block[15] <=0;
                            n_state <= RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[16])begin
                        if (ball_v == blockup[16]+10&&ball_h == blockleft[16]+30)begin//corner
                            n_block[16]<=0;
                            n_state <= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v == blockup[16] +10 && //bottom
                            ((ball_h+10 >blockleft[16] && ball_h+10<blockleft[16] +30)||
                            (ball_h >blockleft[16] && ball_h <blockleft[16] +30)))begin
                            n_block[16] <= 0;
                            n_state<= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h ==blockleft[16]+30&& //right
                            ((ball_v+10>blockup[16] && ball_v+10<=blockup[16]+10)||
                            (ball_v>=blockup[16] && ball_v <blockup[16]+10)))begin
                            n_block[16] <=0;
                            n_state <= RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[17])begin
                        if (ball_v == blockup[17]+10&&ball_h == blockleft[17]+30)begin//corner
                            n_block[17]<=0;
                            n_state <= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v == blockup[17] +10 && //bottom
                            ((ball_h+10 >blockleft[17] && ball_h+10<blockleft[17] +30)||
                            (ball_h >blockleft[17] && ball_h <blockleft[17] +30)))begin
                            n_block[17] <= 0;
                            n_state<= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h ==blockleft[17]+30&& //right
                            ((ball_v+10>blockup[17] && ball_v+10<=blockup[17]+10)||
                            (ball_v>=blockup[17] && ball_v <blockup[17]+10)))begin
                            n_block[17] <=0;
                            n_state <= RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[18])begin
                        if (ball_v == blockup[18]+10&&ball_h == blockleft[18]+30)begin//corner
                            n_block[18]<=0;
                            n_state <= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v == blockup[18] +10 && //bottom
                            ((ball_h+10 >blockleft[18] && ball_h+10<blockleft[18] +30)||
                            (ball_h >blockleft[18] && ball_h <blockleft[18] +30)))begin
                            n_block[18] <= 0;
                            n_state<= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h ==blockleft[18]+30&& //right
                            ((ball_v+10>blockup[18] && ball_v+10<=blockup[18]+10)||
                            (ball_v>=blockup[18] && ball_v <blockup[18]+10)))begin
                            n_block[18] <=0;
                            n_state <= RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[19])begin
                        if (ball_v == blockup[19]+10&&ball_h == blockleft[19]+30)begin//corner
                            n_block[19]<=0;
                            n_state <= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v == blockup[19] +10 && //bottom
                            ((ball_h+10 >blockleft[19] && ball_h+10<blockleft[19] +30)||
                            (ball_h >blockleft[19] && ball_h <blockleft[19] +30)))begin
                            n_block[19] <= 0;
                            n_state<= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h ==blockleft[19]+30&& //right
                            ((ball_v+10>blockup[19] && ball_v+10<=blockup[19]+10)||
                            (ball_v>=blockup[19] && ball_v <blockup[19]+10)))begin
                            n_block[19] <=0;
                            n_state <= RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[20])begin
                        if (ball_v == blockup[20]+10&&ball_h == blockleft[20]+30)begin//corner
                            n_block[20]<=0;
                            n_state <= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v == blockup[20] +10 && //bottom
                            ((ball_h+10 >blockleft[20] && ball_h+10<blockleft[20] +30)||
                            (ball_h >blockleft[20] && ball_h <blockleft[20] +30)))begin
                            n_block[20] <= 0;
                            n_state<= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h ==blockleft[20]+30&& //right
                            ((ball_v+10>blockup[20] && ball_v+10<=blockup[20]+10)||
                            (ball_v>=blockup[20] && ball_v <blockup[20]+10)))begin
                            n_block[20] <=0;
                            n_state <= RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[21])begin
                        if (ball_v == blockup[21]+10&&ball_h == blockleft[21]+30)begin//corner
                            n_block[21]<=0;
                            n_state <= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v == blockup[21] +10 && //bottom
                            ((ball_h+10 >blockleft[21] && ball_h+10<blockleft[21] +30)||
                            (ball_h >blockleft[21] && ball_h <blockleft[21] +30)))begin
                            n_block[21] <= 0;
                            n_state<= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h ==blockleft[21]+30&& //right
                            ((ball_v+10>blockup[21] && ball_v+10<=blockup[21]+10)||
                            (ball_v>=blockup[21] && ball_v <blockup[21]+10)))begin
                            n_block[21] <=0;
                            n_state <= RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[22])begin
                        if (ball_v == blockup[22]+10&&ball_h == blockleft[22]+30)begin//corner
                            n_block[22]<=0;
                            n_state <= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v == blockup[22] +10 && //bottom
                            ((ball_h+10 >blockleft[22] && ball_h+10<blockleft[22] +30)||
                            (ball_h >blockleft[22] && ball_h <blockleft[22] +30)))begin
                            n_block[22] <= 0;
                            n_state<= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h ==blockleft[22]+30&& //right
                            ((ball_v+10>blockup[22] && ball_v+10<=blockup[22]+10)||
                            (ball_v>=blockup[22] && ball_v <blockup[22]+10)))begin
                            n_block[22] <=0;
                            n_state <= RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[23])begin
                        if (ball_v == blockup[23]+10&&ball_h == blockleft[23]+30)begin//corner
                            n_block[23]<=0;
                            n_state <= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v == blockup[23] +10 && //bottom
                            ((ball_h+10 >blockleft[23] && ball_h+10<blockleft[23] +30)||
                            (ball_h >blockleft[23] && ball_h <blockleft[23] +30)))begin
                            n_block[23] <= 0;
                            n_state<= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h ==blockleft[23]+30&& //right
                            ((ball_v+10>blockup[23] && ball_v+10<=blockup[23]+10)||
                            (ball_v>=blockup[23] && ball_v <blockup[23]+10)))begin
                            n_block[23] <=0;
                            n_state <= RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end  
                    if (win) n_state <= END;                           
                    else if (n_h == 10'd0 && n_v == 10'd0) n_state <= RIGHTDOWN;
                    else if (n_h == 10'd0||n_h==1023) n_state <= RIGHTUP;
                    else if (n_v == 10'd0||n_v==1023) n_state <= LEFTDOWN;
                end
                RIGHTDOWN:begin
                    n_state <= RIGHTDOWN;
                    n_h <= ball_h +1;
                    n_v <= ball_v +1;
                    n_LED <= LED;
                    if (block[0])begin
                        if (ball_v+10 == blockup[0]&&ball_h+10 == blockleft[0])begin//corner
                            n_block[0]<=0;
                            n_state <= LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v+10 == blockup[0]  && //top
                            ((ball_h+10 >blockleft[0] && ball_h+10<blockleft[0] +30)||
                            (ball_h >blockleft[0] && ball_h <blockleft[0] +30)))begin
                            n_block[0] <= 0;
                            n_state<=RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h+10 ==blockleft[0]&& //left
                            ((ball_v+10>blockup[0] && ball_v+10<=blockup[0]+10)||
                            (ball_v>=blockup[0] && ball_v <blockup[0]+10)))begin
                            n_block[0] <=0;
                            n_state <= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[1])begin
                        if (ball_v+10 == blockup[1]&&ball_h+10 == blockleft[1])begin//corner
                            n_block[1]<=0;
                            n_state <= LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v+10 == blockup[1]  && //top
                            ((ball_h+10 >blockleft[1] && ball_h+10<blockleft[1] +30)||
                            (ball_h >blockleft[1] && ball_h <blockleft[1] +30)))begin
                            n_block[1] <= 0;
                            n_state<=RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h+10 ==blockleft[1]&& //left
                            ((ball_v+10>blockup[1] && ball_v+10<=blockup[1]+10)||
                            (ball_v>=blockup[1] && ball_v <blockup[1]+10)))begin
                            n_block[1] <=0;
                            n_state <= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[2])begin
                        if (ball_v+10 == blockup[2]&&ball_h+10 == blockleft[2])begin//corner
                            n_block[2]<=0;
                            n_state <= LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v+10 == blockup[2]  && //top
                            ((ball_h+10 >blockleft[2] && ball_h+10<blockleft[2] +30)||
                            (ball_h >blockleft[2] && ball_h <blockleft[2] +30)))begin
                            n_block[2] <= 0;
                            n_state<=RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h+10 ==blockleft[2]&& //left
                            ((ball_v+10>blockup[2] && ball_v+10<=blockup[2]+10)||
                            (ball_v>=blockup[2] && ball_v <blockup[2]+10)))begin
                            n_block[2] <=0;
                            n_state <= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[3])begin
                        if (ball_v+10 == blockup[3]&&ball_h+10 == blockleft[3])begin//corner
                            n_block[3]<=0;
                            n_state <= LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v+10 == blockup[3]  && //top
                            ((ball_h+10 >blockleft[3] && ball_h+10<blockleft[3] +30)||
                            (ball_h >blockleft[3] && ball_h <blockleft[3] +30)))begin
                            n_block[3] <= 0;
                            n_state<=RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h+10 ==blockleft[3]&& //left
                            ((ball_v+10>blockup[3] && ball_v+10<=blockup[3]+10)||
                            (ball_v>=blockup[3] && ball_v <blockup[3]+10)))begin
                            n_block[3] <=0;
                            n_state <= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[4])begin
                        if (ball_v+10 == blockup[4]&&ball_h+10 == blockleft[4])begin//corner
                            n_block[4]<=0;
                            n_state <= LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v+10 == blockup[4]  && //top
                            ((ball_h+10 >blockleft[4] && ball_h+10<blockleft[4] +30)||
                            (ball_h >blockleft[4] && ball_h <blockleft[4] +30)))begin
                            n_block[4] <= 0;
                            n_state<=RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h+10 ==blockleft[4]&& //left
                            ((ball_v+10>blockup[4] && ball_v+10<=blockup[4]+10)||
                            (ball_v>=blockup[4] && ball_v <blockup[4]+10)))begin
                            n_block[4] <=0;
                            n_state <= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[5])begin
                        if (ball_v+10 == blockup[5]&&ball_h+10 == blockleft[5])begin//corner
                            n_block[5]<=0;
                            n_state <= LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v+10 == blockup[5]  && //top
                            ((ball_h+10 >blockleft[5] && ball_h+10<blockleft[5] +30)||
                            (ball_h >blockleft[5] && ball_h <blockleft[5] +30)))begin
                            n_block[5] <= 0;
                            n_state<=RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h+10 ==blockleft[5]&& //left
                            ((ball_v+10>blockup[5] && ball_v+10<=blockup[5]+10)||
                            (ball_v>=blockup[5] && ball_v <blockup[5]+10)))begin
                            n_block[5] <=0;
                            n_state <= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[6])begin
                        if (ball_v+10 == blockup[6]&&ball_h+10 == blockleft[6])begin//corner
                            n_block[6]<=0;
                            n_state <= LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v+10 == blockup[6]  && //top
                            ((ball_h+10 >blockleft[6] && ball_h+10<blockleft[6] +30)||
                            (ball_h >blockleft[6] && ball_h <blockleft[6] +30)))begin
                            n_block[6] <= 0;
                            n_state<=RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h+10 ==blockleft[6]&& //left
                            ((ball_v+10>blockup[6] && ball_v+10<=blockup[6]+10)||
                            (ball_v>=blockup[6] && ball_v <blockup[6]+10)))begin
                            n_block[6] <=0;
                            n_state <= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[7])begin
                        if (ball_v+10 == blockup[7]&&ball_h+10 == blockleft[7])begin//corner
                            n_block[7]<=0;
                            n_state <= LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v+10 == blockup[7]  && //top
                            ((ball_h+10 >blockleft[7] && ball_h+10<blockleft[7] +30)||
                            (ball_h >blockleft[7] && ball_h <blockleft[7] +30)))begin
                            n_block[7] <= 0;
                            n_state<=RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h+10 ==blockleft[7]&& //left
                            ((ball_v+10>blockup[7] && ball_v+10<=blockup[7]+10)||
                            (ball_v>=blockup[7] && ball_v <blockup[7]+10)))begin
                            n_block[7] <=0;
                            n_state <= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[8])begin
                        if (ball_v+10 == blockup[8]&&ball_h+10 == blockleft[8])begin//corner
                            n_block[8]<=0;
                            n_state <= LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v+10 == blockup[8]  && //top
                            ((ball_h+10 >blockleft[8] && ball_h+10<blockleft[8] +30)||
                            (ball_h >blockleft[8] && ball_h <blockleft[8] +30)))begin
                            n_block[8] <= 0;
                            n_state<=RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h+10 ==blockleft[8]&& //left
                            ((ball_v+10>blockup[8] && ball_v+10<=blockup[8]+10)||
                            (ball_v>=blockup[8] && ball_v <blockup[8]+10)))begin
                            n_block[8] <=0;
                            n_state <= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[9])begin
                        if (ball_v+10 == blockup[9]&&ball_h+10 == blockleft[9])begin//corner
                            n_block[9]<=0;
                            n_state <= LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v+10 == blockup[9]  && //top
                            ((ball_h+10 >blockleft[9] && ball_h+10<blockleft[9] +30)||
                            (ball_h >blockleft[9] && ball_h <blockleft[9] +30)))begin
                            n_block[9] <= 0;
                            n_state<=RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h+10 ==blockleft[9]&& //left
                            ((ball_v+10>blockup[9] && ball_v+10<=blockup[9]+10)||
                            (ball_v>=blockup[9] && ball_v <blockup[9]+10)))begin
                            n_block[9] <=0;
                            n_state <= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[10])begin
                        if (ball_v+10 == blockup[10]&&ball_h+10 == blockleft[10])begin//corner
                            n_block[10]<=0;
                            n_state <= LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v+10 == blockup[10]  && //top
                            ((ball_h+10 >blockleft[10] && ball_h+10<blockleft[10] +30)||
                            (ball_h >blockleft[10] && ball_h <blockleft[10] +30)))begin
                            n_block[10] <= 0;
                            n_state<=RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h+10 ==blockleft[10]&& //left
                            ((ball_v+10>blockup[10] && ball_v+10<=blockup[10]+10)||
                            (ball_v>=blockup[10] && ball_v <blockup[10]+10)))begin
                            n_block[10] <=0;
                            n_state <= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[11])begin
                        if (ball_v+10 == blockup[11]&&ball_h+10 == blockleft[11])begin//corner
                            n_block[11]<=0;
                            n_state <= LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v+10 == blockup[11]&& //top
                            ((ball_h+10 >blockleft[11] && ball_h+10<blockleft[11] +30)||
                            (ball_h >blockleft[11] && ball_h <blockleft[11] +30)))begin
                            n_block[11] <= 0;
                            n_state<=RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h+10 ==blockleft[11]&& //left
                            ((ball_v+10>blockup[11] && ball_v+10<=blockup[11]+10)||
                            (ball_v>=blockup[11] && ball_v <blockup[11]+10)))begin
                            n_block[11] <=0;
                            n_state <= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[12])begin
                        if (ball_v+10 == blockup[12]&&ball_h+10 == blockleft[12])begin//corner
                            n_block[12]<=0;
                            n_state <= LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v+10 == blockup[12]  && //top
                            ((ball_h+10 >blockleft[12] && ball_h+10<blockleft[12] +30)||
                            (ball_h >blockleft[12] && ball_h <blockleft[12] +30)))begin
                            n_block[12] <= 0;
                            n_state<=RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h+10 ==blockleft[12]&& //left
                            ((ball_v+10>blockup[12] && ball_v+10<=blockup[12]+10)||
                            (ball_v>=blockup[12] && ball_v <blockup[12]+10)))begin
                            n_block[12] <=0;
                            n_state <= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[13])begin
                        if (ball_v+10 == blockup[13]&&ball_h+10 == blockleft[13])begin//corner
                            n_block[13]<=0;
                            n_state <= LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v+10 == blockup[13]  && //top
                            ((ball_h+10 >blockleft[13] && ball_h+10<blockleft[13] +30)||
                            (ball_h >blockleft[13] && ball_h <blockleft[13] +30)))begin
                            n_block[13] <= 0;
                            n_state<=RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h+10 ==blockleft[13]&& //left
                            ((ball_v+10>blockup[13] && ball_v+10<=blockup[13]+10)||
                            (ball_v>=blockup[13] && ball_v <blockup[13]+10)))begin
                            n_block[13] <=0;
                            n_state <= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[14])begin
                        if (ball_v+10 == blockup[14]&&ball_h+10 == blockleft[14])begin//corner
                            n_block[14]<=0;
                            n_state <= LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v+10 == blockup[14]  && //top
                            ((ball_h+10 >blockleft[14] && ball_h+10<blockleft[14] +30)||
                            (ball_h >blockleft[14] && ball_h <blockleft[14] +30)))begin
                            n_block[14] <= 0;
                            n_state<=RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h+10 ==blockleft[14]&& //left
                            ((ball_v+10>blockup[14] && ball_v+10<=blockup[14]+10)||
                            (ball_v>=blockup[14] && ball_v <blockup[14]+10)))begin
                            n_block[14] <=0;
                            n_state <= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[15])begin
                        if (ball_v+10 == blockup[15]&&ball_h+10 == blockleft[15])begin//corner
                            n_block[15]<=0;
                            n_state <= LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v+10 == blockup[15]  && //top
                            ((ball_h+10 >blockleft[15] && ball_h+10<blockleft[15] +30)||
                            (ball_h >blockleft[15] && ball_h <blockleft[15] +30)))begin
                            n_block[15] <= 0;
                            n_state<=RIGHTUP;
                             if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h+10 ==blockleft[15]&& //left
                            ((ball_v+10>blockup[15] && ball_v+10<=blockup[15]+10)||
                            (ball_v>=blockup[15] && ball_v <blockup[15]+10)))begin
                            n_block[15] <=0;
                            n_state <= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[16])begin
                        if (ball_v+10 == blockup[16]&&ball_h+10 == blockleft[16])begin//corner
                            n_block[16]<=0;
                            n_state <= LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v+10 == blockup[16]  && //top
                            ((ball_h+10 >blockleft[16] && ball_h+10<blockleft[16] +30)||
                            (ball_h >blockleft[16] && ball_h <blockleft[16] +30)))begin
                            n_block[16] <= 0;
                            n_state<=RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h+10 ==blockleft[16]&& //left
                            ((ball_v+10>blockup[16] && ball_v+10<=blockup[16]+10)||
                            (ball_v>=blockup[16] && ball_v <blockup[16]+10)))begin
                            n_block[16] <=0;
                            n_state <= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[17])begin
                        if (ball_v+10 == blockup[17]&&ball_h+10 == blockleft[17])begin//corner
                            n_block[17]<=0;
                            n_state <= LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v+10 == blockup[17]  && //top
                            ((ball_h+10 >blockleft[17] && ball_h+10<blockleft[17] +30)||
                            (ball_h >blockleft[17] && ball_h <blockleft[17] +30)))begin
                            n_block[17] <= 0;
                            n_state<=RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h+10 ==blockleft[17]&& //left
                            ((ball_v+10>blockup[17] && ball_v+10<=blockup[17]+10)||
                            (ball_v>=blockup[17] && ball_v <blockup[17]+10)))begin
                            n_block[17] <=0;
                            n_state <= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[18])begin
                        if (ball_v+10 == blockup[18]&&ball_h+10 == blockleft[18])begin//corner
                            n_block[18]<=0;
                            n_state <= LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v+10 == blockup[18]  && //top
                            ((ball_h+10 >blockleft[18] && ball_h+10<blockleft[18] +30)||
                            (ball_h >blockleft[18] && ball_h <blockleft[18] +30)))begin
                            n_block[18] <= 0;
                            n_state<=RIGHTUP;
                             if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h+10 ==blockleft[18]&& //left
                            ((ball_v+10>blockup[18] && ball_v+10<=blockup[18]+10)||
                            (ball_v>=blockup[18] && ball_v <blockup[18]+10)))begin
                            n_block[18] <=0;
                            n_state <= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[19])begin
                        if (ball_v+10 == blockup[19]&&ball_h+10 == blockleft[19])begin//corner
                            n_block[19]<=0;
                            n_state <= LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v+10 == blockup[19]  && //top
                            ((ball_h+10 >blockleft[19] && ball_h+10<blockleft[19] +30)||
                            (ball_h >blockleft[19] && ball_h <blockleft[19] +30)))begin
                            n_block[19] <= 0;
                            n_state<=RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h+10 ==blockleft[19]&& //left
                            ((ball_v+10>blockup[19] && ball_v+10<=blockup[19]+10)||
                            (ball_v>=blockup[19] && ball_v <blockup[19]+10)))begin
                            n_block[19] <=0;
                            n_state <= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[20])begin
                        if (ball_v+10 == blockup[20]&&ball_h+10 == blockleft[20])begin//corner
                            n_block[20]<=0;
                            n_state <= LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v+10 == blockup[20]  && //top
                            ((ball_h+10 >blockleft[20] && ball_h+10<blockleft[20] +30)||
                            (ball_h >blockleft[20] && ball_h <blockleft[20] +30)))begin
                            n_block[20] <= 0;
                            n_state<=RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h+10 ==blockleft[20]&& //left
                            ((ball_v+10>blockup[20] && ball_v+10<=blockup[20]+10)||
                            (ball_v>=blockup[20] && ball_v <blockup[20]+10)))begin
                            n_block[20] <=0;
                            n_state <= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[21])begin
                        if (ball_v+10 == blockup[21]&&ball_h+10 == blockleft[21])begin//corner
                            n_block[21]<=0;
                            n_state <= LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v+10 == blockup[21]  && //top
                            ((ball_h+10 >blockleft[21] && ball_h+10<blockleft[21] +30)||
                            (ball_h >blockleft[21] && ball_h <blockleft[21] +30)))begin
                            n_block[21] <= 0;
                            n_state<=RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h+10 ==blockleft[21]&& //left
                            ((ball_v+10>blockup[21] && ball_v+10<=blockup[21]+10)||
                            (ball_v>=blockup[21] && ball_v <blockup[21]+10)))begin
                            n_block[21] <=0;
                            n_state <= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[22])begin
                        if (ball_v+10 == blockup[22]&&ball_h+10 == blockleft[22])begin//corner
                            n_block[22]<=0;
                            n_state <= LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v+10 == blockup[22]  && //top
                            ((ball_h+10 >blockleft[22] && ball_h+10<blockleft[22] +30)||
                            (ball_h >blockleft[22] && ball_h <blockleft[22] +30)))begin
                            n_block[22] <= 0;
                            n_state<=RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h+10 ==blockleft[22]&& //left
                            ((ball_v+10>blockup[22] && ball_v+10<=blockup[22]+10)||
                            (ball_v>=blockup[22] && ball_v <blockup[22]+10)))begin
                            n_block[22] <=0;
                            n_state <= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end
                    if (block[23])begin
                        if (ball_v+10 == blockup[23]&&ball_h+10 == blockleft[23])begin//corner
                            n_block[23]<=0;
                            n_state <= LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v+10 == blockup[23]  && //top
                            ((ball_h+10 >blockleft[23] && ball_h+10<blockleft[23] +30)||
                            (ball_h >blockleft[23] && ball_h <blockleft[23] +30)))begin
                            n_block[23] <= 0;
                            n_state<=RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h+10 ==blockleft[23]&& //left
                            ((ball_v+10>blockup[23] && ball_v+10<=blockup[23]+10)||
                            (ball_v>=blockup[23] && ball_v <blockup[23]+10)))begin
                            n_block[23] <=0;
                            n_state <= LEFTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end  
                    if (win) n_state <= END;                           
                    else if (n_h >= 10'd310) n_state <= LEFTDOWN;
                    else if (n_v >=190)begin
                        if (n_h+10 >=pikapos && n_h <= pikapos+30) n_state <= RIGHTUP;
                        else if (n_v >=225)n_state <= END;
                    end 
                end
                LEFTDOWN:begin
                    n_state <= LEFTDOWN;
                    n_h <= ball_h -1;
                    n_v <= ball_v +1;
                    n_LED <= LED;
                    if (block[0])begin
                        if (ball_v+10 == blockup[0]&&ball_h == blockleft[0]+30)begin//corner
                            n_block[0]<=0;
                            n_state <= RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v+10 == blockup[0]  && //top
                            ((ball_h+10 >blockleft[0] && ball_h+10<blockleft[0] +30)||
                            (ball_h >blockleft[0] && ball_h <blockleft[0] +30)))begin
                            n_block[0] <= 0;
                            n_state<=LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h ==blockleft[0] +30&& //right
                            ((ball_v+10>blockup[0] && ball_v+10<=blockup[0]+10)||
                            (ball_v>=blockup[0] && ball_v <blockup[0]+10)))begin
                            n_block[0] <=0;
                            n_state <= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end  
                    if (block[1])begin
                        if (ball_v+10 == blockup[1]&&ball_h == blockleft[1]+30)begin//corner
                            n_block[1]<=0;
                            n_state <= RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v+10 == blockup[1]  && //top
                            ((ball_h+10 >blockleft[1] && ball_h+10<blockleft[1] +30)||
                            (ball_h >blockleft[1] && ball_h <blockleft[1] +30)))begin
                            n_block[1] <= 0;
                            n_state<=LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h ==blockleft[1] +30&& //right
                            ((ball_v+10>blockup[1] && ball_v+10<=blockup[1]+10)||
                            (ball_v>=blockup[1] && ball_v <blockup[1]+10)))begin
                            n_block[1] <=0;
                            n_state <= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end  
                    if (block[2])begin
                        if (ball_v+10 == blockup[2]&&ball_h == blockleft[2]+30)begin//corner
                            n_block[2]<=0;
                            n_state <= RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v+10 == blockup[2]  && //top
                            ((ball_h+10 >blockleft[2] && ball_h+10<blockleft[2] +30)||
                            (ball_h >blockleft[2] && ball_h <blockleft[2] +30)))begin
                            n_block[2] <= 0;
                            n_state<=LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h ==blockleft[2] +30&& //right
                            ((ball_v+10>blockup[2] && ball_v+10<=blockup[2]+10)||
                            (ball_v>=blockup[2] && ball_v <blockup[2]+10)))begin
                            n_block[2] <=0;
                            n_state <= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end  
                    if (block[3])begin
                        if (ball_v+10 == blockup[3]&&ball_h == blockleft[3]+30)begin//corner
                            n_block[3]<=0;
                            n_state <= RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v+10 == blockup[3]  && //top
                            ((ball_h+10 >blockleft[3] && ball_h+10<blockleft[3] +30)||
                            (ball_h >blockleft[3] && ball_h <blockleft[3] +30)))begin
                            n_block[3] <= 0;
                            n_state<=LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h ==blockleft[3] +30&& //right
                            ((ball_v+10>blockup[3] && ball_v+10<=blockup[3]+10)||
                            (ball_v>=blockup[3] && ball_v <blockup[3]+10)))begin
                            n_block[3] <=0;
                            n_state <= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end  
                    if (block[4])begin
                        if (ball_v+10 == blockup[4]&&ball_h == blockleft[4]+30)begin//corner
                            n_block[4]<=0;
                            n_state <= RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v+10 == blockup[4]  && //top
                            ((ball_h+10 >blockleft[4] && ball_h+10<blockleft[4] +30)||
                            (ball_h >blockleft[4] && ball_h <blockleft[4] +30)))begin
                            n_block[4] <= 0;
                            n_state<=LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h ==blockleft[4] +30&& //right
                            ((ball_v+10>blockup[4] && ball_v+10<=blockup[4]+10)||
                            (ball_v>=blockup[4] && ball_v <blockup[4]+10)))begin
                            n_block[4] <=0;
                            n_state <= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end  
                    if (block[5])begin
                        if (ball_v+10 == blockup[5]&&ball_h == blockleft[5]+30)begin//corner
                            n_block[5]<=0;
                            n_state <= RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v+10 == blockup[5]  && //top
                            ((ball_h+10 >blockleft[5] && ball_h+10<blockleft[5] +30)||
                            (ball_h >blockleft[5] && ball_h <blockleft[5] +30)))begin
                            n_block[5] <= 0;
                            n_state<=LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h ==blockleft[5] +30&& //right
                            ((ball_v+10>blockup[5] && ball_v+10<=blockup[5]+10)||
                            (ball_v>=blockup[5] && ball_v <blockup[5]+10)))begin
                            n_block[5] <=0;
                            n_state <= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end  
                    if (block[6])begin
                        if (ball_v+10 == blockup[6]&&ball_h == blockleft[6]+30)begin//corner
                            n_block[6]<=0;
                            n_state <= RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v+10 == blockup[6]  && //top
                            ((ball_h+10 >blockleft[6] && ball_h+10<blockleft[6] +30)||
                            (ball_h >blockleft[6] && ball_h <blockleft[6] +30)))begin
                            n_block[6] <= 0;
                            n_state<=LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h ==blockleft[6] +30&& //right
                            ((ball_v+10>blockup[6] && ball_v+10<=blockup[6]+10)||
                            (ball_v>=blockup[6] && ball_v <blockup[6]+10)))begin
                            n_block[6] <=0;
                            n_state <= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end  
                    if (block[7])begin
                        if (ball_v+10 == blockup[7]&&ball_h == blockleft[7]+30)begin//corner
                            n_block[7]<=0;
                            n_state <= RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v+10 == blockup[7]  && //top
                            ((ball_h+10 >blockleft[7] && ball_h+10<blockleft[7] +30)||
                            (ball_h >blockleft[7] && ball_h <blockleft[7] +30)))begin
                            n_block[7] <= 0;
                            n_state<=LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h ==blockleft[7] +30&& //right
                            ((ball_v+10>blockup[7] && ball_v+10<=blockup[7]+10)||
                            (ball_v>=blockup[7] && ball_v <blockup[7]+10)))begin
                            n_block[7] <=0;
                            n_state <= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end  
                    if (block[8])begin
                        if (ball_v+10 == blockup[8]&&ball_h == blockleft[8]+30)begin//corner
                            n_block[8]<=0;
                            n_state <= RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v+10 == blockup[8]  && //top
                            ((ball_h+10 >blockleft[8] && ball_h+10<blockleft[8] +30)||
                            (ball_h >blockleft[8] && ball_h <blockleft[8] +30)))begin
                            n_block[8] <= 0;
                            n_state<=LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h ==blockleft[8] +30&& //right
                            ((ball_v+10>blockup[8] && ball_v+10<=blockup[8]+10)||
                            (ball_v>=blockup[8] && ball_v <blockup[8]+10)))begin
                            n_block[8] <=0;
                            n_state <= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end  
                    if (block[9])begin
                        if (ball_v+10 == blockup[9]&&ball_h == blockleft[9]+30)begin//corner
                            n_block[9]<=0;
                            n_state <= RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v+10 == blockup[9]  && //top
                            ((ball_h+10 >blockleft[9] && ball_h+10<blockleft[9] +30)||
                            (ball_h >blockleft[9] && ball_h <blockleft[9] +30)))begin
                            n_block[9] <= 0;
                            n_state<=LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h ==blockleft[9] +30&& //right
                            ((ball_v+10>blockup[9] && ball_v+10<=blockup[9]+10)||
                            (ball_v>=blockup[9] && ball_v <blockup[9]+10)))begin
                            n_block[9] <=0;
                            n_state <= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end  
                    if (block[10])begin
                        if (ball_v+10 == blockup[10]&&ball_h == blockleft[10]+30)begin//corner
                            n_block[10]<=0;
                            n_state <= RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v+10 == blockup[10]  && //top
                            ((ball_h+10 >blockleft[10] && ball_h+10<blockleft[10] +30)||
                            (ball_h >blockleft[10] && ball_h <blockleft[10] +30)))begin
                            n_block[10] <= 0;
                            n_state<=LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h ==blockleft[10] +30&& //right
                            ((ball_v+10>blockup[10] && ball_v+10<=blockup[10]+10)||
                            (ball_v>=blockup[10] && ball_v <blockup[10]+10)))begin
                            n_block[10] <=0;
                            n_state <= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end  
                    if (block[11])begin
                        if (ball_v+10 == blockup[11]&&ball_h == blockleft[11]+30)begin//corner
                            n_block[11]<=0;
                            n_state <= RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v+10 == blockup[11]  && //top
                            ((ball_h+10 >blockleft[11] && ball_h+10<blockleft[11] +30)||
                            (ball_h >blockleft[11] && ball_h <blockleft[11] +30)))begin
                            n_block[11] <= 0;
                            n_state<=LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h ==blockleft[11] +30&& //right
                            ((ball_v+10>blockup[11] && ball_v+10<=blockup[11]+10)||
                            (ball_v>=blockup[11] && ball_v <blockup[11]+10)))begin
                            n_block[11] <=0;
                            n_state <= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end  
                    if (block[12])begin
                        if (ball_v+10 == blockup[12]&&ball_h == blockleft[12]+30)begin//corner
                            n_block[12]<=0;
                            n_state <= RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v+10 == blockup[12]  && //top
                            ((ball_h+10 >blockleft[12] && ball_h+10<blockleft[12] +30)||
                            (ball_h >blockleft[12] && ball_h <blockleft[12] +30)))begin
                            n_block[12] <= 0;
                            n_state<=LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h ==blockleft[12] +30&& //right
                            ((ball_v+10>blockup[12] && ball_v+10<=blockup[12]+10)||
                            (ball_v>=blockup[12] && ball_v <blockup[12]+10)))begin
                            n_block[12] <=0;
                            n_state <= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end  
                    if (block[13])begin
                        if (ball_v+10 == blockup[13]&&ball_h == blockleft[13]+30)begin//corner
                            n_block[13]<=0;
                            n_state <= RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v+10 == blockup[13]  && //top
                            ((ball_h+10 >blockleft[13] && ball_h+10<blockleft[13] +30)||
                            (ball_h >blockleft[13] && ball_h <blockleft[13] +30)))begin
                            n_block[13] <= 0;
                            n_state<=LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h ==blockleft[13] +30&& //right
                            ((ball_v+10>blockup[13] && ball_v+10<=blockup[13]+10)||
                            (ball_v>=blockup[13] && ball_v <blockup[13]+10)))begin
                            n_block[13] <=0;
                            n_state <= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end  
                    if (block[14])begin
                        if (ball_v+10 == blockup[14]&&ball_h == blockleft[14]+30)begin//corner
                            n_block[14]<=0;
                            n_state <= RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v+10 == blockup[14]  && //top
                            ((ball_h+10 >blockleft[14] && ball_h+10<blockleft[14] +30)||
                            (ball_h >blockleft[14] && ball_h <blockleft[14] +30)))begin
                            n_block[14] <= 0;
                            n_state<=LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h ==blockleft[14] +30&& //right
                            ((ball_v+10>blockup[14] && ball_v+10<=blockup[14]+10)||
                            (ball_v>=blockup[14] && ball_v <blockup[14]+10)))begin
                            n_block[14] <=0;
                            n_state <= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end  
                    if (block[15])begin
                        if (ball_v+10 == blockup[15]&&ball_h == blockleft[15]+30)begin//corner
                            n_block[15]<=0;
                            n_state <= RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v+10 == blockup[15]  && //top
                            ((ball_h+10 >blockleft[15] && ball_h+10<blockleft[15] +30)||
                            (ball_h >blockleft[15] && ball_h <blockleft[15] +30)))begin
                            n_block[15] <= 0;
                            n_state<=LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h ==blockleft[15] +30&& //right
                            ((ball_v+10>blockup[15] && ball_v+10<=blockup[15]+10)||
                            (ball_v>=blockup[15] && ball_v <blockup[15]+10)))begin
                            n_block[15] <=0;
                            n_state <= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end  
                    if (block[16])begin
                        if (ball_v+10 == blockup[16]&&ball_h == blockleft[16]+30)begin//corner
                            n_block[16]<=0;
                            n_state <= RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v+10 == blockup[16]  && //top
                            ((ball_h+10 >blockleft[16] && ball_h+10<blockleft[16] +30)||
                            (ball_h >blockleft[16] && ball_h <blockleft[16] +30)))begin
                            n_block[16] <= 0;
                            n_state<=LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h ==blockleft[16] +30&& //right
                            ((ball_v+10>blockup[16] && ball_v+10<=blockup[16]+10)||
                            (ball_v>=blockup[16] && ball_v <blockup[16]+10)))begin
                            n_block[16] <=0;
                            n_state <= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end  
                    if (block[17])begin
                        if (ball_v+10 == blockup[17]&&ball_h == blockleft[17]+30)begin//corner
                            n_block[17]<=0;
                            n_state <= RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v+10 == blockup[17]  && //top
                            ((ball_h+10 >blockleft[17] && ball_h+10<blockleft[17] +30)||
                            (ball_h >blockleft[17] && ball_h <blockleft[17] +30)))begin
                            n_block[17] <= 0;
                            n_state<=LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h ==blockleft[17] +30&& //right
                            ((ball_v+10>blockup[17] && ball_v+10<=blockup[17]+10)||
                            (ball_v>=blockup[17] && ball_v <blockup[17]+10)))begin
                            n_block[17] <=0;
                            n_state <= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end  
                    if (block[18])begin
                        if (ball_v+10 == blockup[18]&&ball_h == blockleft[18]+30)begin//corner
                            n_block[18]<=0;
                            n_state <= RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v+10 == blockup[18]  && //top
                            ((ball_h+10 >blockleft[18] && ball_h+10<blockleft[18] +30)||
                            (ball_h >blockleft[18] && ball_h <blockleft[18] +30)))begin
                            n_block[18] <= 0;
                            n_state<=LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h ==blockleft[18] +30&& //right
                            ((ball_v+10>blockup[18] && ball_v+10<=blockup[18]+10)||
                            (ball_v>=blockup[18] && ball_v <blockup[18]+10)))begin
                            n_block[18] <=0;
                            n_state <= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end  
                    if (block[19])begin
                        if (ball_v+10 == blockup[19]&&ball_h == blockleft[19]+30)begin//corner
                            n_block[19]<=0;
                            n_state <= RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v+10 == blockup[19]  && //top
                            ((ball_h+10 >blockleft[19] && ball_h+10<blockleft[19] +30)||
                            (ball_h >blockleft[19] && ball_h <blockleft[19] +30)))begin
                            n_block[19] <= 0;
                            n_state<=LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h ==blockleft[19] +30&& //right
                            ((ball_v+10>blockup[19] && ball_v+10<=blockup[19]+10)||
                            (ball_v>=blockup[19] && ball_v <blockup[19]+10)))begin
                            n_block[19] <=0;
                            n_state <= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end  
                    if (block[20])begin
                        if (ball_v+10 == blockup[20]&&ball_h == blockleft[20]+30)begin//corner
                            n_block[20]<=0;
                            n_state <= RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v+10 == blockup[20]  && //top
                            ((ball_h+10 >blockleft[20] && ball_h+10<blockleft[20] +30)||
                            (ball_h >blockleft[20] && ball_h <blockleft[20] +30)))begin
                            n_block[20] <= 0;
                            n_state<=LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h ==blockleft[20] +30&& //right
                            ((ball_v+10>blockup[20] && ball_v+10<=blockup[20]+10)||
                            (ball_v>=blockup[20] && ball_v <blockup[20]+10)))begin
                            n_block[20] <=0;
                            n_state <= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end  
                    if (block[21])begin
                        if (ball_v+10 == blockup[21]&&ball_h == blockleft[21]+30)begin//corner
                            n_block[21]<=0;
                            n_state <= RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v+10 == blockup[21]  && //top
                            ((ball_h+10 >blockleft[21] && ball_h+10<blockleft[21] +30)||
                            (ball_h >blockleft[21] && ball_h <blockleft[21] +30)))begin
                            n_block[21] <= 0;
                            n_state<=LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h ==blockleft[21] +30&& //right
                            ((ball_v+10>blockup[21] && ball_v+10<=blockup[21]+10)||
                            (ball_v>=blockup[21] && ball_v <blockup[21]+10)))begin
                            n_block[21] <=0;
                            n_state <= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end  
                    if (block[22])begin
                        if (ball_v+10 == blockup[22]&&ball_h == blockleft[22]+30)begin//corner
                            n_block[22]<=0;
                            n_state <= RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v+10 == blockup[22]  && //top
                            ((ball_h+10 >blockleft[22] && ball_h+10<blockleft[22] +30)||
                            (ball_h >blockleft[22] && ball_h <blockleft[22] +30)))begin
                            n_block[22] <= 0;
                            n_state<=LEFTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h ==blockleft[22] +30&& //right
                            ((ball_v+10>blockup[22] && ball_v+10<=blockup[22]+10)||
                            (ball_v>=blockup[22] && ball_v <blockup[22]+10)))begin
                            n_block[22] <=0;
                            n_state <= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end  
                    if (block[23])begin
                        if (ball_v+10 == blockup[23]&&ball_h == blockleft[23]+30)begin//corner
                            n_block[23]<=0;
                            n_state <= RIGHTUP;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_v+10 == blockup[23]  && //top
                            ((ball_h+10 >blockleft[23] && ball_h+10<blockleft[23] +30)||
                            (ball_h >blockleft[23] && ball_h <blockleft[23] +30)))begin
                            n_block[23] <= 0;
                            n_state<=LEFTUP;
                             if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                        else if (ball_h ==blockleft[23] +30&& //right
                            ((ball_v+10>blockup[23] && ball_v+10<=blockup[23]+10)||
                            (ball_v>=blockup[23] && ball_v <blockup[23]+10)))begin
                            n_block[23] <=0;
                            n_state <= RIGHTDOWN;
                            if(LED!=10'b1111111111)begin
                                 if(LED==10'b00000000000) n_LED <= 10'b00000000011;
                                 else n_LED <= (LED<<2)+3;
                            end
                            
                        end
                    end  
                    if (win) n_state <= END;                           
                    else if (n_h == 10'd0||n_h==1023) n_state <= RIGHTDOWN;
                    else if (n_v >=190)begin
                        if (n_h+10 >=pikapos && n_h <=pikapos+30) n_state <= LEFTUP;
                        else if (n_v >=225) n_state <= END;
                    end 
                end
                END:begin
                    n_h <= ball_h;
                    n_v <= ball_v;
                    n_state <= END;
                    n_block <= block;
                    n_count <= count -1;
                    n_LED <=LED;
                    if (LED == 10'b0101010101&& count ==0) n_LED <= 10'b1010101010;
                    else if (LED == 10'b1010101010&& count ==0) n_LED <= 10'b0101010101;
                    else if (count == 0)n_LED <= 10'b1010101010;
                end
                default:begin
                    n_h <= 160;
                    n_v <= 165;
                    n_state <= RIGHTUP;
                    n_block<= 24'b111111111111111111111111;
                    n_LED <= 10'b00000000000;
                    n_count <= 0;
                end
            endcase
        end
        else begin
            n_h <= ball_h;
            n_v <= ball_v;
            n_state <= state;
            n_block<=block;
            n_LED <= LED;
            n_count <= count;
        end
    end
endmodule
