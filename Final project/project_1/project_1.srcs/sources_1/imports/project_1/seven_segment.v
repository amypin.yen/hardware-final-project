`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2016/10/13 22:20:27
// Design Name: 
// Module Name: seven_segment
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module seven_segment(clk,score,DIGIT,DISPLAY);
input clk;
input [10:0]score;
output [3:0]DIGIT;
output [6:0]DISPLAY;
reg [3:0]value;
reg [3:0]DIGIT;
wire [3:0]BCD0;
wire [3:0]BCD1;
wire [3:0]BCD2;
wire [3:0]BCD3;

assign BCD0 = 0;
assign BCD1 = (score%100)/10;
assign BCD2 = (score%1000)/100;
assign BCD3 = score/1000 ;

always@(posedge clk)begin
    case(DIGIT)
        4'b1110:begin
            value <= BCD1;
            DIGIT <= 4'b1101;
        end
        4'b1101:begin
            value <= BCD2;
            DIGIT <= 4'b1011;
        end
        4'b1011:begin
             value <= BCD3;
             DIGIT <= 4'b0111;
        end
        4'b0111:begin
            value <= BCD0;
            DIGIT <= 4'b1110;
        end   
        default begin
            value <= BCD0;
            DIGIT <= 4'b1110;
        end
    endcase
end
assign DISPLAY  = (value==4'd0) ? 7'b0000001 :
                  (value==4'd1) ? 7'b1001111 :
                  (value==4'd2) ? 7'b0010010 :
                  (value==4'd3) ? 7'b0000110 :
                  (value==4'd4) ? 7'b1001100 :
                  (value==4'd5) ? 7'b0100100 :
                  (value==4'd6) ? 7'b0100000 :
                  (value==4'd7) ? 7'b0001111 :
                  (value==4'd8) ? 7'b0000000 :
                  (value==4'd9) ? 7'b0000100 :
                                  7'b1111111 ;
endmodule
