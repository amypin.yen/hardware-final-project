
`define LB  32'd247 // B LOW
`define C  32'd262 // C 
`define E   32'd330 // E
`define F  32'd349 // F 
`define G  32'd392 // G 
`define A   32'd440 // A
`define B   32'd494 // B
`define D   32'd294 // D
`define LA   32'd220 // A-LOW
`define LF   32'd174 // F-LOW
`define HC   32'd524 // C-HIGH
`define LG   32'd196 // G-LOW
`define NON   32'd20000 //slience (over freq.)

module Music (
	input [9:0] ibeatNum,	
	output reg [31:0] tone
);

always @(*) begin
	case (ibeatNum)		// 1/4 beat
	10'd0 : tone =`NON;
    10'd1 : tone =`LF;
    10'd2 : tone =`LA;
    10'd3 : tone =`LB;
    10'd4 : tone =`C;
    10'd5 : tone =`LB;
    10'd6 : tone =`C;
    10'd7 : tone =`D;
    
    10'd8 : tone =`E;
    10'd9 : tone =`E;
    10'd10 : tone =`E;
    10'd11 : tone =`E;
    10'd12 : tone =`E;
    10'd13 : tone =`E;
    10'd14 : tone =`NON;
    10'd15 : tone =`NON;
    
    ///////////
    10'd16 : tone =`LA;
    10'd17 : tone =`LA;
    10'd18 : tone =`LB;
    10'd19 : tone =`LB;
    10'd20 : tone =`C;
    10'd21 : tone =`E;
    10'd22 : tone =`NON;
    10'd23 : tone =`G;
    
    10'd24 : tone =`NON;
    10'd25 : tone =`NON;
    10'd26 : tone =`NON;
    10'd27 : tone =`NON;
    10'd28 : tone =`NON;
    10'd29 : tone =`NON;
    10'd30 : tone =`NON;
    10'd31 : tone =`NON;
    
    ///////////
    10'd32 : tone =`C;
    10'd33 : tone =`C;
    10'd34 : tone =`LA;
    10'd35 : tone =`LA;
    10'd36 : tone =`C;
    10'd37 : tone =`C;
    10'd38 : tone =`LA;
    10'd39 : tone =`LA;
    
    10'd40 : tone =`C;
    10'd41 : tone =`C;
    10'd42 : tone =`LA;
    10'd43 : tone =`LA;
    10'd44 : tone =`C;
    10'd45 : tone =`C;
    10'd46 : tone =`LA;
    10'd47 : tone =`NON;
    
    ///////////
    10'd48 : tone =`LA;
    10'd49 : tone =`LA;
    10'd50 : tone =`C;
    10'd51 : tone =`NON;
    10'd52 : tone =`C;
    10'd53 : tone =`C;
    10'd54 : tone =`C;
    10'd55 : tone =`LB;
    
    10'd56 : tone =`LB;
    10'd57 : tone =`LB;
    10'd58 : tone =`LB;
    10'd59 : tone =`LB;
    10'd60 : tone =`NON;
    10'd61 : tone =`NON;
    10'd62 : tone =`NON;
    10'd63 : tone =`NON;
    
    ///////////
    10'd64 : tone =`C;
    10'd65 : tone =`C;
    10'd66 : tone =`LA;
    10'd67 : tone =`LA;
    10'd68 : tone =`C;
    10'd69 : tone =`C;
    10'd70 : tone =`LA;
    10'd71 : tone =`LA;
    
    10'd72 : tone =`C;
    10'd73 : tone =`C;
    10'd74 : tone =`LA;
    10'd75 : tone =`LA;
    10'd76 : tone =`C;
    10'd77 : tone =`C;
    10'd78 : tone =`LA;
    10'd79 : tone =`LA;
    
    ///////////
    10'd80 : tone =`E;
    10'd81 : tone =`NON;
    10'd82 : tone =`E;
    10'd83 : tone =`E;
    10'd84 : tone =`D;
    10'd85 : tone =`C;
    10'd86 : tone =`C;
    10'd87 : tone =`LB;
    
    10'd88 : tone =`LB;
    10'd89 : tone =`LB;
    10'd90 : tone =`LB;
    10'd91 : tone =`LB;
    10'd92 : tone =`NON;
    10'd93 : tone =`NON;
    10'd94 : tone =`NON;
    10'd95 : tone =`NON;
    
    ///////////
    10'd96 : tone =`C;
    10'd97 : tone =`C;
    10'd98 : tone =`LA;
    10'd99 : tone =`LA;
    10'd100 : tone =`C;
    10'd101 : tone =`C;
    10'd102 : tone =`LA;
    10'd103 : tone =`LA;
    
    10'd104 : tone =`C;
    10'd105 : tone =`C;
    10'd106 : tone =`LA;
    10'd107 : tone =`LA;
    10'd108 : tone =`C;
    10'd109 : tone =`C;
    10'd110 : tone =`LA;
    10'd111 : tone =`NON;
    
    ///////////
    10'd112 : tone =`LA;
    10'd113 : tone =`LA;
    10'd114 : tone =`C;
    10'd115 : tone =`NON;
    10'd116 : tone =`C;
    10'd117 : tone =`D;
    10'd118 : tone =`D;
    10'd119 : tone =`LB;
    
    10'd120 : tone =`LB;
    10'd121 : tone =`LB;
    10'd122 : tone =`LB;
    10'd123 : tone =`LB;
    10'd124 : tone =`NON;
    10'd125 : tone =`NON;
    10'd126 : tone =`LA;
    10'd127 : tone =`LA;
    
    ///////////
    10'd128 : tone =`C;
    10'd129 : tone =`NON;
    10'd130 : tone =`C;
    10'd131 : tone =`NON;
    10'd132 : tone =`C;
    10'd133 : tone =`NON;
    10'd134 : tone =`C;
    10'd135 : tone =`NON;
    
    10'd136 : tone =`NON;
    10'd137 : tone =`NON;
    10'd138 : tone =`NON;
    10'd139 : tone =`NON;
    10'd140 : tone =`LA;
    10'd141 : tone =`NON;
    10'd142 : tone =`LA;
    10'd143 : tone =`C;
    
    ///////////
    10'd144 : tone =`LB;
    10'd145 : tone =`LB;
    10'd146 : tone =`E;
    10'd147 : tone =`E;
    10'd148 : tone =`NON;
    10'd149 : tone =`NON;
    10'd150 : tone =`NON;
    10'd151 : tone =`NON;
    
    10'd152 : tone =`NON;
    10'd153 : tone =`NON;
    10'd154 : tone =`NON;
    10'd155 : tone =`NON;
    10'd156 : tone =`NON;
    10'd157 : tone =`NON;
    10'd158 : tone =`NON;
    10'd159 : tone =`NON;
    
    ///////////
    10'd160 : tone =`C;
    10'd161 : tone =`NON;
    10'd162 : tone =`C;
    10'd163 : tone =`NON;
    10'd164 : tone =`C;
    10'd165 : tone =`NON;
    10'd166 : tone =`C;
    10'd167 : tone =`NON;
    
    10'd168 : tone =`NON;
    10'd169 : tone =`NON;
    10'd170 : tone =`NON;
    10'd171 : tone =`NON;
    10'd172 : tone =`LA;
    10'd173 : tone =`NON;
    10'd174 : tone =`LA;
    10'd175 : tone =`C;
    
    ///////////
    10'd176 : tone =`LB;
    10'd177 : tone =`LB;
    10'd178 : tone =`LB;
    10'd179 : tone =`LB;
    10'd180 : tone =`NON;
    10'd181 : tone =`NON;
    10'd182 : tone =`NON;
    10'd183 : tone =`NON;
    
    10'd184 : tone =`NON;
    10'd185 : tone =`NON;
    10'd186 : tone =`NON;
    10'd187 : tone =`NON;
    10'd188 : tone =`NON;
    10'd189 : tone =`NON;
    10'd190 : tone =`LB;
    10'd191 : tone =`LB;
    
    ///////////
    10'd192 : tone =`LA;
    10'd193 : tone =`LA;
    10'd194 : tone =`LF;
    10'd195 : tone =`LF;
    10'd196 : tone =`LA;
    10'd197 : tone =`LA;
    10'd198 : tone =`LB;
    10'd199 : tone =`LB;
    
    10'd200 : tone =`C;
    10'd201 : tone =`C;
    10'd202 : tone =`LA;
    10'd203 : tone =`LA;
    10'd204 : tone =`NON;
    10'd205 : tone =`NON;
    10'd206 : tone =`NON;
    10'd207 : tone =`NON;
    
    ///////////
    10'd208 : tone =`LA;
    10'd209 : tone =`NON;
    10'd210 : tone =`LA;
    10'd211 : tone =`NON;
    10'd212 : tone =`LA;
    10'd213 : tone =`LA;
    10'd214 : tone =`E;
    10'd215 : tone =`E;
    
    10'd216 : tone =`C;
    10'd217 : tone =`C;
    10'd218 : tone =`C;
    10'd219 : tone =`C;
    10'd220 : tone =`NON;
    10'd221 : tone =`NON;
    10'd222 : tone =`LA;
    10'd223 : tone =`LA;
    
    ///////////
    10'd224 : tone =`D;
    10'd225 : tone =`D;
    10'd226 : tone =`C;
    10'd227 : tone =`C;
    10'd228 : tone =`LB;
    10'd229 : tone =`LB;
    10'd230 : tone =`LA;
    10'd231 : tone =`LA;
    
    10'd232 : tone =`LG;
    10'd233 : tone =`LG;
    10'd234 : tone =`LA;
    10'd235 : tone =`LA;
    10'd236 : tone =`LB;
    10'd237 : tone =`LB;
    10'd238 : tone =`LA;
    10'd239 : tone =`LA;
    
    ///////////
    10'd240 : tone =`LB;
    10'd241 : tone =`LB;
    10'd242 : tone =`LA;
    10'd243 : tone =`LA;
    10'd244 : tone =`LG;
    10'd245 : tone =`LG;
    10'd246 : tone =`LA;
    10'd247 : tone =`LA;
    
    10'd248 : tone =`LB;
    10'd249 : tone =`LB;
    10'd250 : tone =`LB;
    10'd251 : tone =`LB;
    10'd252 : tone =`C;
    10'd253 : tone =`C;
    10'd254 : tone =`D;
    10'd255 : tone =`D;
    
    ///////////
    10'd256 : tone =`E;
    10'd257 : tone =`E;
    10'd258 : tone =`A;
    10'd259 : tone =`NON;
    10'd260 : tone =`A;
    10'd261 : tone =`A;
    10'd262 : tone =`B;
    10'd263 : tone =`B;
    
    10'd264 : tone =`G;
    10'd265 : tone =`G;
    10'd266 : tone =`F;
    10'd267 : tone =`NON;
    10'd268 : tone =`F;
    10'd269 : tone =`F;
    10'd270 : tone =`E;
    10'd271 : tone =`E;
    
    ///////////
    10'd272 : tone =`F;
    10'd273 : tone =`F;
    10'd274 : tone =`E;
    10'd275 : tone =`NON;
    10'd276 : tone =`E;
    10'd277 : tone =`E;
    10'd278 : tone =`C;
    10'd279 : tone =`C;
    
    10'd280 : tone =`LA;
    10'd281 : tone =`LA;
    10'd282 : tone =`LA;
    10'd283 : tone =`NON;
    10'd284 : tone =`LA;
    10'd285 : tone =`LA;
    10'd286 : tone =`LB;
    10'd287 : tone =`LB;
    
    ///////////
    10'd288 : tone =`C;
    10'd289 : tone =`NON;
    10'd290 : tone =`C;
    10'd291 : tone =`NON;
    10'd292 : tone =`C;
    10'd293 : tone =`C;
    10'd294 : tone =`D;
    10'd295 : tone =`D;
    
    10'd296 : tone =`E;
    10'd297 : tone =`NON;
    10'd298 : tone =`E;
    10'd299 : tone =`E;
    10'd300 : tone =`D;
    10'd301 : tone =`D;
    10'd302 : tone =`C;
    10'd303 : tone =`C;
    
    ///////////
    10'd304 : tone =`LB;
    10'd305 : tone =`LB;
    10'd306 : tone =`LA;
    10'd307 : tone =`LA;
    10'd308 : tone =`LA;
    10'd309 : tone =`LA;
    10'd310 : tone =`LA;
    10'd311 : tone =`LA;
    
    10'd312 : tone =`NON;
    10'd313 : tone =`NON;
    10'd314 : tone =`NON;
    10'd315 : tone =`NON;
    10'd316 : tone =`C;
    10'd317 : tone =`C;
    10'd318 : tone =`D;
    10'd319 : tone =`D;
    
    ///////////
    10'd320 : tone =`E;
    10'd321 : tone =`E;
    10'd322 : tone =`A;
    10'd323 : tone =`NON;
    10'd324 : tone =`A;
    10'd325 : tone =`A;
    10'd326 : tone =`B;
    10'd327 : tone =`B;
    
    10'd328 : tone =`G;
    10'd329 : tone =`G;
    10'd330 : tone =`F;
    10'd331 : tone =`NON;
    10'd332 : tone =`F;
    10'd333 : tone =`F;
    10'd334 : tone =`E;
    10'd335 : tone =`E;
    
    ///////////
    10'd336 : tone =`F;
    10'd337 : tone =`F;
    10'd338 : tone =`E;
    10'd339 : tone =`NON;
    10'd340 : tone =`E;
    10'd341 : tone =`E;
    10'd342 : tone =`C;
    10'd343 : tone =`C;
    
    10'd344 : tone =`LA;
    10'd345 : tone =`LA;
    10'd346 : tone =`LA;
    10'd347 : tone =`LA;
    10'd348 : tone =`C;
    10'd349 : tone =`C;
    10'd350 : tone =`LB;
    10'd351 : tone =`LB;
    
    ///////////
    10'd352 : tone =`LA;
    10'd353 : tone =`LA;
    10'd354 : tone =`LA;
    10'd355 : tone =`LA;
    10'd356 : tone =`C;
    10'd357 : tone =`C;
    10'd358 : tone =`C;
    10'd359 : tone =`C;
    
    10'd360 : tone =`LB;
    10'd361 : tone =`LB;
    10'd362 : tone =`LB;
    10'd363 : tone =`LB;
    10'd364 : tone =`LA;
    10'd365 : tone =`LA;
    10'd366 : tone =`LA;
    10'd367 : tone =`LA;
    
    ///////////
    10'd368 : tone =`A;
    10'd369 : tone =`A;
    10'd370 : tone =`A;
    10'd371 : tone =`A;
    10'd372 : tone =`A;
    10'd373 : tone =`A;
    10'd374 : tone =`A;
    10'd375 : tone =`A;
    
    10'd376 : tone =`A;
    10'd377 : tone =`A;
    10'd378 : tone =`A;
    10'd379 : tone =`A;
    10'd380 : tone =`A;
    10'd381 : tone =`A;
    10'd382 : tone =`A;
    10'd383 : tone =`A;
    
    ///////////
    10'd384 : tone =`NON;
    10'd385 : tone =`NON;
    10'd386 : tone =`NON;
    10'd387 : tone =`NON;
    10'd388 : tone =`NON;
    10'd389 : tone =`NON;
    10'd390 : tone =`NON;
    10'd391 : tone =`NON;
    
    10'd392 : tone =`NON;
    10'd393 : tone =`NON;
    10'd394 : tone =`NON;
    10'd395 : tone =`NON;
    10'd396 : tone =`NON;
    10'd397 : tone =`NON;
    10'd398 : tone =`NON;
    10'd399 : tone =`NON;
    
    ///////////
    10'd400 : tone =`NON;
    10'd401 : tone =`NON;
    10'd402 : tone =`NON;
    10'd403 : tone =`NON;
    10'd404 : tone =`G;
    10'd405 : tone =`G;
    10'd406 : tone =`G;
    10'd407 : tone =`NON;
    
    10'd408 : tone =`G;
    10'd409 : tone =`G;
    10'd410 : tone =`G;
    10'd411 : tone =`NON;
    10'd412 : tone =`G;
    10'd413 : tone =`G;
    10'd414 : tone =`G;
    10'd415 : tone =`G;
    
    ///////////
    10'd416 : tone =`A;
    10'd417 : tone =`A;
    10'd418 : tone =`A;
    10'd419 : tone =`A;
    10'd420 : tone =`A;
    10'd421 : tone =`A;
    10'd422 : tone =`A;
    10'd423 : tone =`A;
    
    10'd424 : tone =`G;
    10'd425 : tone =`NON;
    10'd426 : tone =`G;
    10'd427 : tone =`NON;
    10'd428 : tone =`G;
    10'd429 : tone =`G;
    10'd430 : tone =`A;
    10'd431 : tone =`A;
    
    ///////////
    10'd432 : tone =`G;
    10'd433 : tone =`G;
    10'd434 : tone =`G;
    10'd435 : tone =`G;
    10'd436 : tone =`G;
    10'd437 : tone =`G;
    10'd438 : tone =`G;
    10'd439 : tone =`G;
    
    10'd440 : tone =`G;
    10'd441 : tone =`G;
    10'd442 : tone =`G;
    10'd443 : tone =`G;
    10'd444 : tone =`NON;
    10'd445 : tone =`NON;
    10'd446 : tone =`G;
    10'd447 : tone =`G;
    
    ///////////
    10'd448 : tone =`A;
    10'd449 : tone =`NON;
    10'd450 : tone =`A;
    10'd451 : tone =`NON;
    10'd452 : tone =`A;
    10'd453 : tone =`NON;
    10'd454 : tone =`A;
    10'd455 : tone =`NON;
    
    10'd456 : tone =`G;
    10'd457 : tone =`NON;
    10'd458 : tone =`G;
    10'd459 : tone =`NON;
    10'd460 : tone =`A;
    10'd461 : tone =`A;
    10'd462 : tone =`G;
    10'd463 : tone =`G;
    
    ///////////
    10'd464 : tone =`G;
    10'd465 : tone =`G;
    10'd466 : tone =`G;
    10'd467 : tone =`G;
    10'd468 : tone =`G;
    10'd469 : tone =`G;
    10'd470 : tone =`G;
    10'd471 : tone =`G;
    
    10'd472 : tone =`G;
    10'd473 : tone =`G;
    10'd474 : tone =`G;
    10'd475 : tone =`G;
    10'd476 : tone =`NON;
    10'd477 : tone =`NON;
    10'd478 : tone =`G;
    10'd479 : tone =`G;
    
    ///////////
    10'd480 : tone =`A;
    10'd481 : tone =`A;
    10'd482 : tone =`G;
    10'd483 : tone =`G;
    10'd484 : tone =`A;
    10'd485 : tone =`A;
    10'd486 : tone =`G;
    10'd487 : tone =`G;
    
    10'd488 : tone =`G;
    10'd489 : tone =`G;
    10'd490 : tone =`G;
    10'd491 : tone =`G;
    10'd492 : tone =`NON;
    10'd493 : tone =`NON;
    10'd494 : tone =`G;
    10'd495 : tone =`NON;
    
    ///////////
    10'd496 : tone =`G;
    10'd497 : tone =`NON;
    10'd498 : tone =`G;
    10'd499 : tone =`G;
    10'd500 : tone =`E;
    10'd501 : tone =`E;
    10'd502 : tone =`G;
    10'd503 : tone =`G;
    
    10'd504 : tone =`G;
    10'd505 : tone =`G;
    10'd506 : tone =`G;
    10'd507 : tone =`G;
    10'd508 : tone =`NON;
    10'd509 : tone =`NON;
    10'd510 : tone =`G;
    10'd511 : tone =`G;
    
    ///////////
    10'd512 : tone =`A;
    10'd513 : tone =`A;
    10'd514 : tone =`G;
    10'd515 : tone =`G;
    10'd516 : tone =`A;
    10'd517 : tone =`A;
    10'd518 : tone =`G;
    10'd519 : tone =`G;
    
    10'd520 : tone =`G;
    10'd521 : tone =`G;
    10'd522 : tone =`G;
    10'd523 : tone =`G;
    10'd524 : tone =`HC;
    10'd525 : tone =`HC;
    10'd526 : tone =`B;
    10'd527 : tone =`B;
    
    ///////////
    10'd528 : tone =`B;
    10'd529 : tone =`B;
    10'd530 : tone =`B;
    10'd531 : tone =`B;
    10'd532 : tone =`B;
    10'd533 : tone =`B;
    10'd534 : tone =`B;
    10'd535 : tone =`B;
    
    10'd536 : tone =`B;
    10'd537 : tone =`B;
    10'd538 : tone =`B;
    10'd539 : tone =`B;
    10'd540 : tone =`B;
    10'd541 : tone =`B;
    10'd542 : tone =`B;
    10'd543 : tone =`B;
    
    ///////////
    10'd544 : tone =`NON;
    10'd545 : tone =`LF;
    10'd546 : tone =`LA;
    10'd547 : tone =`LB;
    10'd548 : tone =`C;
    10'd549 : tone =`LB;
    10'd550 : tone =`C;
    10'd551 : tone =`D;
    
    10'd552 : tone =`E;
    10'd553 : tone =`E;
    10'd554 : tone =`E;
    10'd555 : tone =`E;
    10'd556 : tone =`E;
    10'd557 : tone =`E;
    10'd558 : tone =`NON;
    10'd559 : tone =`NON;
    
    ///////////
    10'd560 : tone =`A;
    10'd561 : tone =`A;
    10'd562 : tone =`C;
    10'd563 : tone =`E;
    10'd564 : tone =`NON;
    10'd565 : tone =`G;
    10'd566 : tone =`NON;
    10'd567 : tone =`NON;
    
    10'd568 : tone =`NON;
    10'd569 : tone =`NON;
    10'd570 : tone =`A;
    10'd571 : tone =`A;
    10'd572 : tone =`A;
    10'd573 : tone =`NON;
    10'd574 : tone =`NON;
    10'd575 : tone =`A;
    
    ///////////


		default : tone = `NON;
	endcase
end

endmodule
		
		
		
		