`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2016/12/13 16:13:25
// Design Name: 
// Module Name: Move
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module pikaMove(input Lleft,
        input Lright,
        input pause,
        input reset,
        input clk,
        input exit,
        input ending,
        input NEXTstage,
        input [3:0]Top_state,
        output reg [9:0]pikapos,
        output reg dir
    );
    reg en;
    reg [9:0]n_pika;
    reg n_dir;
    
    always @(posedge reset or posedge pause or posedge exit or posedge NEXTstage)begin
        if (reset||exit||NEXTstage||Top_state ==4'b0000) en<=0;
        else en<=~en;
    end
    always @(posedge clk or posedge reset or posedge exit or posedge NEXTstage)begin
        if (reset||exit||NEXTstage||Top_state==4'b0000)begin
            dir <=1;
            pikapos <= 160;
        end
        else begin
            dir <= n_dir;
            pikapos <= n_pika;
        end
    end
    always@(*)begin
        n_pika <= pikapos ;
        n_dir <= dir;
        if (en && ~ending)begin 
            if (Lleft)begin
                if (pikapos<15) n_pika <=0;
                else n_pika <= pikapos -15;
                n_dir <=0;
            end
            else if (Lright)begin
                if (pikapos >264) n_pika <= 279;
                else n_pika <= pikapos +15;
                n_dir <=1;
            end
        end     
    end 
endmodule
