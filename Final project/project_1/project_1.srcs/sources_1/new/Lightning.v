`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2016/12/22 15:51:00
// Design Name: 
// Module Name: Lightning
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Lightning1(input clk,
                input reset,
                input [9:0]LED,
                input isplay,
                input [9:0]pikapos,
                input [23:0]block,
                input light,
                input exit,
                input [3:0]Top_state,
                output reg [9:0]light_h,
                output reg [23:0]Lightblock,
                output reg [9:0]LightLED,
                output reg showlight
    );
    
    parameter WAIT = 1'b0;
    parameter LIGHTNING = 1'b1; 
    wire [9:0] blockleft [0:23]={
        10'd10,10'd60,10'd110,10'd160,10'd210,10'd260,
        10'd10,10'd60,10'd110,10'd160,10'd210,10'd260,
        10'd10,10'd60,10'd110,10'd160,10'd210,10'd260,
        10'd10,10'd60,10'd110,10'd160,10'd210,10'd260
        };
    
    reg [9:0]n_h;
    reg [23:0]n_block;
    reg n_showlight;
    reg state,n_state;
    reg [9:0]n_LED;
    reg [3:0]count,n_count;
    
    
    always @(posedge clk or posedge reset or posedge exit)begin
        if (reset ||exit||Top_state!=4'b0001)begin
            LightLED <= 10'd0;
            light_h<= 10'd0;
            Lightblock <= 24'b111111111111111111111111;
            showlight <= 0;
            state <= WAIT;
            count <= 4'd10;
        end
        else begin
            LightLED <= n_LED;
            light_h <= n_h;
            Lightblock <= n_block;
            showlight <= n_showlight;
            state <= n_state;
            count <= n_count;
        end
    end
    
    always@(*)begin
        n_block <= block;
        n_h <= pikapos;
        n_state <= WAIT;
        n_showlight <= showlight;
        n_LED <= LED;
        n_count <= 4'd10;
        if (isplay)begin
            case(state)
                WAIT :begin
                    n_LED <= LED;
                    n_h <= pikapos +5; 
                    n_state <= WAIT;
                    n_showlight <= 1'b0;
                    n_count <= 4'd10;
                    if(light &&LED ==10'b1111111111)begin
                        n_LED <= 0;
                        n_state <= LIGHTNING;
                        n_showlight <= 1'b1;
                        if (block[0])begin
                            if (light_h >= blockleft[0] && light_h <blockleft[0]+50) n_block[0] <= 0;
                            else if (light_h +30 > blockleft[0] && light_h +30 <=blockleft[0]+50) n_block[0] <=0;
                        end
                        if (block[1])begin
                            if (light_h >= blockleft[1] && light_h <blockleft[1]+50) n_block[1] <= 0;
                            else if (light_h +30 > blockleft[1] && light_h +30 <=blockleft[1]+50) n_block[1] <=0;
                        end
                        if (block[2])begin
                            if (light_h >= blockleft[2] && light_h <blockleft[2]+50) n_block[2] <= 0;
                            else if (light_h +30 > blockleft[2] && light_h +30 <=blockleft[2]+50) n_block[2] <=0;
                        end
                        if (block[3])begin
                            if (light_h >= blockleft[3] && light_h <blockleft[3]+50) n_block[3] <= 0;
                            else if (light_h +30 > blockleft[3] && light_h +30 <=blockleft[3]+50) n_block[3] <=0;
                        end
                        if (block[4])begin
                            if (light_h >= blockleft[4] && light_h <blockleft[4]+50) n_block[4] <= 0;
                            else if (light_h +30 > blockleft[4] && light_h +30 <=blockleft[4]+50) n_block[4] <=0;
                        end
                        if (block[5])begin
                            if (light_h >= blockleft[5] && light_h <blockleft[5]+50) n_block[5] <= 0;
                            else if (light_h +30 > blockleft[5] && light_h +30 <=blockleft[5]+50) n_block[5] <=0;
                        end
                        if (block[6])begin
                            if (light_h >= blockleft[6] && light_h <blockleft[6]+50) n_block[6] <= 0;
                            else if (light_h +30 > blockleft[6] && light_h +30 <=blockleft[6]+50) n_block[6] <=0;
                        end
                        if (block[7])begin
                            if (light_h >= blockleft[7] && light_h <blockleft[7]+50) n_block[7] <= 0;
                            else if (light_h +30 > blockleft[7] && light_h +30 <=blockleft[7]+50) n_block[7] <=0;
                        end
                        if (block[8])begin
                            if (light_h >= blockleft[8] && light_h <blockleft[8]+50) n_block[8] <= 0;
                            else if (light_h +30 > blockleft[8] && light_h +30 <=blockleft[8]+50) n_block[8] <=0;
                        end
                        if (block[9])begin
                            if (light_h >= blockleft[9] && light_h <blockleft[9]+50) n_block[9] <= 0;
                            else if (light_h +30 > blockleft[9] && light_h +30 <=blockleft[9]+50) n_block[9] <=0;
                        end
                        if (block[10])begin
                            if (light_h >= blockleft[10] && light_h <blockleft[10]+50) n_block[10] <= 0;
                            else if (light_h +30 > blockleft[10] && light_h +30 <=blockleft[10]+50) n_block[10] <=0;
                        end
                        if (block[11])begin
                            if (light_h >= blockleft[11] && light_h <blockleft[11]+50) n_block[11] <= 0;
                            else if (light_h +30 > blockleft[11] && light_h +30 <=blockleft[11]+50) n_block[11] <=0;
                        end
                        if (block[12])begin
                            if (light_h >= blockleft[12] && light_h <blockleft[12]+50) n_block[12] <= 0;
                            else if (light_h +30 > blockleft[12] && light_h +30 <=blockleft[12]+50) n_block[12] <=0;
                        end
                        if (block[13])begin
                            if (light_h >= blockleft[13] && light_h <blockleft[13]+50) n_block[13] <= 0;
                            else if (light_h +30 > blockleft[13] && light_h +30 <=blockleft[13]+50) n_block[13] <=0;
                        end
                        if (block[14])begin
                            if (light_h >= blockleft[14] && light_h <blockleft[14]+50) n_block[14] <= 0;
                            else if (light_h +30 > blockleft[14] && light_h +30 <=blockleft[14]+50) n_block[14] <=0;
                        end
                        if (block[15])begin
                            if (light_h >= blockleft[15] && light_h <blockleft[15]+50) n_block[15] <= 0;
                            else if (light_h +30 > blockleft[15] && light_h +30 <=blockleft[15]+50) n_block[15] <=0;
                        end
                        if (block[16])begin
                            if (light_h >= blockleft[16] && light_h <blockleft[16]+50) n_block[16] <= 0;
                            else if (light_h +30 > blockleft[16] && light_h +30 <=blockleft[16]+50) n_block[16] <=0;
                        end
                        if (block[17])begin
                            if (light_h >= blockleft[17] && light_h <blockleft[17]+50) n_block[17] <= 0;
                            else if (light_h +30 > blockleft[17] && light_h +30 <=blockleft[17]+50) n_block[17] <=0;
                        end
                        if (block[18])begin
                            if (light_h >= blockleft[18] && light_h <blockleft[18]+50) n_block[18] <= 0;
                            else if (light_h +30 > blockleft[18] && light_h +30 <=blockleft[18]+50) n_block[18] <=0;
                        end
                        if (block[19])begin
                            if (light_h >= blockleft[19] && light_h <blockleft[19]+50) n_block[19] <= 0;
                            else if (light_h +30 > blockleft[19] && light_h +30 <=blockleft[19]+50) n_block[19] <=0;
                        end
                        if (block[20])begin
                            if (light_h >= blockleft[20] && light_h <blockleft[20]+50) n_block[20] <= 0;
                            else if (light_h +30 > blockleft[20] && light_h +30 <=blockleft[20]+50) n_block[20] <=0;
                        end
                        if (block[21])begin
                            if (light_h >= blockleft[21] && light_h <blockleft[21]+50) n_block[21] <= 0;
                            else if (light_h +30 > blockleft[21] && light_h +30 <=blockleft[21]+50) n_block[21] <=0;
                        end
                        if (block[22])begin
                            if (light_h >= blockleft[22] && light_h <blockleft[22]+50) n_block[22] <= 0;
                            else if (light_h +30 > blockleft[22] && light_h +30 <=blockleft[22]+50) n_block[22] <=0;
                        end
                        if (block[23])begin
                            if (light_h >= blockleft[23] && light_h <blockleft[23]+50) n_block[23] <= 0;
                            else if (light_h +30 > blockleft[23] && light_h +30 <=blockleft[23]+50) n_block[23] <=0;
                        end
                    end
                end
                LIGHTNING:begin
                    n_LED <= LED;
                    n_h <= light_h;
                    n_state <= LIGHTNING;
                    n_showlight <= showlight;
                    n_count <= count -1;
                    if (count==0)begin
                       n_showlight <= 0;
                       n_state <= WAIT; 
                    end
                end
                default :begin
                    n_h <= pikapos;
                    n_state <= WAIT;
                    n_showlight <= showlight;
                    n_LED <= 0;
                    n_count <= 4'd10;
                end
            endcase
        end
    end
endmodule

module Lightning2(input clk,
                input reset,
                input [9:0]LED,
                input isplay,
                input [9:0]pikapos,
                input [23:0]block,
                input light,
                input exit,
                input [3:0]Top_state,
                output reg [9:0]light_h,
                output reg [23:0]Lightblock,
                output reg [9:0]LightLED,
                output reg showlight
    );
    
    parameter WAIT = 1'b0;
    parameter LIGHTNING = 1'b1; 
    wire [9:0] blockleft [0:23]={
        10'd145,10'd145,10'd55,10'd85,10'd115,10'd145,10'd175,10'd205,
        10'd235,10'd55,10'd85,10'd115,10'd145,10'd175,10'd205,10'd235,10'd130,10'd160,
        10'd100,10'd190,10'd70,10'd220,10'd40,10'd250
        };
    
    reg [9:0]n_h;
    reg [23:0]n_block;
    reg n_showlight;
    reg state,n_state;
    reg [9:0]n_LED;
    reg [3:0]count,n_count;
    
    
    always @(posedge clk or posedge reset or posedge exit)begin
        if (reset ||exit||Top_state!=4'b0010)begin
            LightLED <= 10'd0;
            light_h<= 10'd0;
            Lightblock <= 24'b111111111111111111111111;
            showlight <= 0;
            state <= WAIT;
            count <= 4'd10;
        end
        else begin
            LightLED <= n_LED;
            light_h <= n_h;
            Lightblock <= n_block;
            showlight <= n_showlight;
            state <= n_state;
            count <= n_count;
        end
    end
    
    always@(*)begin
        n_block <= block;
        n_h <= pikapos;
        n_state <= WAIT;
        n_showlight <= showlight;
        n_LED <= LED;
        n_count <= 4'd10;
        if (isplay)begin
            case(state)
                WAIT :begin
                    n_LED <= LED;
                    n_h <= pikapos +5; 
                    n_state <= WAIT;
                    n_showlight <= 1'b0;
                    n_count <= 4'd10;
                    if(light &&LED ==10'b1111111111)begin
                        n_LED <= 0;
                        n_state <= LIGHTNING;
                        n_showlight <= 1'b1;
                        if (block[0])begin
                            if (light_h >= blockleft[0] && light_h <blockleft[0]+50) n_block[0] <= 0;
                            else if (light_h +30 > blockleft[0] && light_h +30 <=blockleft[0]+50) n_block[0] <=0;
                        end
                        if (block[1])begin
                            if (light_h >= blockleft[1] && light_h <blockleft[1]+50) n_block[1] <= 0;
                            else if (light_h +30 > blockleft[1] && light_h +30 <=blockleft[1]+50) n_block[1] <=0;
                        end
                        if (block[2])begin
                            if (light_h >= blockleft[2] && light_h <blockleft[2]+50) n_block[2] <= 0;
                            else if (light_h +30 > blockleft[2] && light_h +30 <=blockleft[2]+50) n_block[2] <=0;
                        end
                        if (block[3])begin
                            if (light_h >= blockleft[3] && light_h <blockleft[3]+50) n_block[3] <= 0;
                            else if (light_h +30 > blockleft[3] && light_h +30 <=blockleft[3]+50) n_block[3] <=0;
                        end
                        if (block[4])begin
                            if (light_h >= blockleft[4] && light_h <blockleft[4]+50) n_block[4] <= 0;
                            else if (light_h +30 > blockleft[4] && light_h +30 <=blockleft[4]+50) n_block[4] <=0;
                        end
                        if (block[5])begin
                            if (light_h >= blockleft[5] && light_h <blockleft[5]+50) n_block[5] <= 0;
                            else if (light_h +30 > blockleft[5] && light_h +30 <=blockleft[5]+50) n_block[5] <=0;
                        end
                        if (block[6])begin
                            if (light_h >= blockleft[6] && light_h <blockleft[6]+50) n_block[6] <= 0;
                            else if (light_h +30 > blockleft[6] && light_h +30 <=blockleft[6]+50) n_block[6] <=0;
                        end
                        if (block[7])begin
                            if (light_h >= blockleft[7] && light_h <blockleft[7]+50) n_block[7] <= 0;
                            else if (light_h +30 > blockleft[7] && light_h +30 <=blockleft[7]+50) n_block[7] <=0;
                        end
                        if (block[8])begin
                            if (light_h >= blockleft[8] && light_h <blockleft[8]+50) n_block[8] <= 0;
                            else if (light_h +30 > blockleft[8] && light_h +30 <=blockleft[8]+50) n_block[8] <=0;
                        end
                        if (block[9])begin
                            if (light_h >= blockleft[9] && light_h <blockleft[9]+50) n_block[9] <= 0;
                            else if (light_h +30 > blockleft[9] && light_h +30 <=blockleft[9]+50) n_block[9] <=0;
                        end
                        if (block[10])begin
                            if (light_h >= blockleft[10] && light_h <blockleft[10]+50) n_block[10] <= 0;
                            else if (light_h +30 > blockleft[10] && light_h +30 <=blockleft[10]+50) n_block[10] <=0;
                        end
                        if (block[11])begin
                            if (light_h >= blockleft[11] && light_h <blockleft[11]+50) n_block[11] <= 0;
                            else if (light_h +30 > blockleft[11] && light_h +30 <=blockleft[11]+50) n_block[11] <=0;
                        end
                        if (block[12])begin
                            if (light_h >= blockleft[12] && light_h <blockleft[12]+50) n_block[12] <= 0;
                            else if (light_h +30 > blockleft[12] && light_h +30 <=blockleft[12]+50) n_block[12] <=0;
                        end
                        if (block[13])begin
                            if (light_h >= blockleft[13] && light_h <blockleft[13]+50) n_block[13] <= 0;
                            else if (light_h +30 > blockleft[13] && light_h +30 <=blockleft[13]+50) n_block[13] <=0;
                        end
                        if (block[14])begin
                            if (light_h >= blockleft[14] && light_h <blockleft[14]+50) n_block[14] <= 0;
                            else if (light_h +30 > blockleft[14] && light_h +30 <=blockleft[14]+50) n_block[14] <=0;
                        end
                        if (block[15])begin
                            if (light_h >= blockleft[15] && light_h <blockleft[15]+50) n_block[15] <= 0;
                            else if (light_h +30 > blockleft[15] && light_h +30 <=blockleft[15]+50) n_block[15] <=0;
                        end
                        if (block[16])begin
                            if (light_h >= blockleft[16] && light_h <blockleft[16]+50) n_block[16] <= 0;
                            else if (light_h +30 > blockleft[16] && light_h +30 <=blockleft[16]+50) n_block[16] <=0;
                        end
                        if (block[17])begin
                            if (light_h >= blockleft[17] && light_h <blockleft[17]+50) n_block[17] <= 0;
                            else if (light_h +30 > blockleft[17] && light_h +30 <=blockleft[17]+50) n_block[17] <=0;
                        end
                        if (block[18])begin
                            if (light_h >= blockleft[18] && light_h <blockleft[18]+50) n_block[18] <= 0;
                            else if (light_h +30 > blockleft[18] && light_h +30 <=blockleft[18]+50) n_block[18] <=0;
                        end
                        if (block[19])begin
                            if (light_h >= blockleft[19] && light_h <blockleft[19]+50) n_block[19] <= 0;
                            else if (light_h +30 > blockleft[19] && light_h +30 <=blockleft[19]+50) n_block[19] <=0;
                        end
                        if (block[20])begin
                            if (light_h >= blockleft[20] && light_h <blockleft[20]+50) n_block[20] <= 0;
                            else if (light_h +30 > blockleft[20] && light_h +30 <=blockleft[20]+50) n_block[20] <=0;
                        end
                        if (block[21])begin
                            if (light_h >= blockleft[21] && light_h <blockleft[21]+50) n_block[21] <= 0;
                            else if (light_h +30 > blockleft[21] && light_h +30 <=blockleft[21]+50) n_block[21] <=0;
                        end
                        if (block[22])begin
                            if (light_h >= blockleft[22] && light_h <blockleft[22]+50) n_block[22] <= 0;
                            else if (light_h +30 > blockleft[22] && light_h +30 <=blockleft[22]+50) n_block[22] <=0;
                        end
                        if (block[23])begin
                            if (light_h >= blockleft[23] && light_h <blockleft[23]+50) n_block[23] <= 0;
                            else if (light_h +30 > blockleft[23] && light_h +30 <=blockleft[23]+50) n_block[23] <=0;
                        end
                    end
                end
                LIGHTNING:begin
                    n_LED <= LED;
                    n_h <= light_h;
                    n_state <= LIGHTNING;
                    n_showlight <= showlight;
                    n_count <= count -1;
                    if (count==0)begin
                       n_showlight <= 0;
                       n_state <= WAIT; 
                    end
                end
                default :begin
                    n_h <= pikapos;
                    n_state <= WAIT;
                    n_showlight <= showlight;
                    n_LED <= 0;
                    n_count <= 4'd10;
                end
            endcase
        end
    end
endmodule

module Lightning3(input clk,
                input reset,
                input [9:0]LED,
                input isplay,
                input [9:0]pikapos,
                input [23:0]block,
                input light,
                input exit,
                input [3:0]Top_state,
                output reg [9:0]light_h,
                output reg [23:0]Lightblock,
                output reg [9:0]LightLED,
                output reg showlight
    );
    
    parameter WAIT = 1'b0;
    parameter LIGHTNING = 1'b1; 
    wire [9:0] blockleft [0:23]={
        10'd25,10'd55,10'd55,10'd85,10'd85,10'd85,10'd115,10'd115,10'd115,10'd115,
        10'd145,10'd145,10'd145,10'd145,10'd175,10'd175,10'd175,10'd175,
        10'd205,10'd205,10'd205,10'd235,10'd235,10'd265
        };
    
    reg [9:0]n_h;
    reg [23:0]n_block;
    reg n_showlight;
    reg state,n_state;
    reg [9:0]n_LED;
    reg [3:0]count,n_count;
    
    
    always @(posedge clk or posedge reset or posedge exit)begin
        if (reset ||exit||Top_state!=4'b0011)begin
            LightLED <= 10'd0;
            light_h<= 10'd0;
            Lightblock <= 24'b111111111111111111111111;
            showlight <= 0;
            state <= WAIT;
            count <= 4'd10;
        end
        else begin
            LightLED <= n_LED;
            light_h <= n_h;
            Lightblock <= n_block;
            showlight <= n_showlight;
            state <= n_state;
            count <= n_count;
        end
    end
    
    always@(*)begin
        n_block <= block;
        n_h <= pikapos;
        n_state <= WAIT;
        n_showlight <= showlight;
        n_LED <= LED;
        n_count <= 4'd10;
        if (isplay)begin
            case(state)
                WAIT :begin
                    n_LED <= LED;
                    n_h <= pikapos +5; 
                    n_state <= WAIT;
                    n_showlight <= 1'b0;
                    n_count <= 4'd10;
                    if(light &&LED ==10'b1111111111)begin
                        n_LED <= 0;
                        n_state <= LIGHTNING;
                        n_showlight <= 1'b1;
                        if (block[0])begin
                            if (light_h >= blockleft[0] && light_h <blockleft[0]+50) n_block[0] <= 0;
                            else if (light_h +30 > blockleft[0] && light_h +30 <=blockleft[0]+50) n_block[0] <=0;
                        end
                        if (block[1])begin
                            if (light_h >= blockleft[1] && light_h <blockleft[1]+50) n_block[1] <= 0;
                            else if (light_h +30 > blockleft[1] && light_h +30 <=blockleft[1]+50) n_block[1] <=0;
                        end
                        if (block[2])begin
                            if (light_h >= blockleft[2] && light_h <blockleft[2]+50) n_block[2] <= 0;
                            else if (light_h +30 > blockleft[2] && light_h +30 <=blockleft[2]+50) n_block[2] <=0;
                        end
                        if (block[3])begin
                            if (light_h >= blockleft[3] && light_h <blockleft[3]+50) n_block[3] <= 0;
                            else if (light_h +30 > blockleft[3] && light_h +30 <=blockleft[3]+50) n_block[3] <=0;
                        end
                        if (block[4])begin
                            if (light_h >= blockleft[4] && light_h <blockleft[4]+50) n_block[4] <= 0;
                            else if (light_h +30 > blockleft[4] && light_h +30 <=blockleft[4]+50) n_block[4] <=0;
                        end
                        if (block[5])begin
                            if (light_h >= blockleft[5] && light_h <blockleft[5]+50) n_block[5] <= 0;
                            else if (light_h +30 > blockleft[5] && light_h +30 <=blockleft[5]+50) n_block[5] <=0;
                        end
                        if (block[6])begin
                            if (light_h >= blockleft[6] && light_h <blockleft[6]+50) n_block[6] <= 0;
                            else if (light_h +30 > blockleft[6] && light_h +30 <=blockleft[6]+50) n_block[6] <=0;
                        end
                        if (block[7])begin
                            if (light_h >= blockleft[7] && light_h <blockleft[7]+50) n_block[7] <= 0;
                            else if (light_h +30 > blockleft[7] && light_h +30 <=blockleft[7]+50) n_block[7] <=0;
                        end
                        if (block[8])begin
                            if (light_h >= blockleft[8] && light_h <blockleft[8]+50) n_block[8] <= 0;
                            else if (light_h +30 > blockleft[8] && light_h +30 <=blockleft[8]+50) n_block[8] <=0;
                        end
                        if (block[9])begin
                            if (light_h >= blockleft[9] && light_h <blockleft[9]+50) n_block[9] <= 0;
                            else if (light_h +30 > blockleft[9] && light_h +30 <=blockleft[9]+50) n_block[9] <=0;
                        end
                        if (block[10])begin
                            if (light_h >= blockleft[10] && light_h <blockleft[10]+50) n_block[10] <= 0;
                            else if (light_h +30 > blockleft[10] && light_h +30 <=blockleft[10]+50) n_block[10] <=0;
                        end
                        if (block[11])begin
                            if (light_h >= blockleft[11] && light_h <blockleft[11]+50) n_block[11] <= 0;
                            else if (light_h +30 > blockleft[11] && light_h +30 <=blockleft[11]+50) n_block[11] <=0;
                        end
                        if (block[12])begin
                            if (light_h >= blockleft[12] && light_h <blockleft[12]+50) n_block[12] <= 0;
                            else if (light_h +30 > blockleft[12] && light_h +30 <=blockleft[12]+50) n_block[12] <=0;
                        end
                        if (block[13])begin
                            if (light_h >= blockleft[13] && light_h <blockleft[13]+50) n_block[13] <= 0;
                            else if (light_h +30 > blockleft[13] && light_h +30 <=blockleft[13]+50) n_block[13] <=0;
                        end
                        if (block[14])begin
                            if (light_h >= blockleft[14] && light_h <blockleft[14]+50) n_block[14] <= 0;
                            else if (light_h +30 > blockleft[14] && light_h +30 <=blockleft[14]+50) n_block[14] <=0;
                        end
                        if (block[15])begin
                            if (light_h >= blockleft[15] && light_h <blockleft[15]+50) n_block[15] <= 0;
                            else if (light_h +30 > blockleft[15] && light_h +30 <=blockleft[15]+50) n_block[15] <=0;
                        end
                        if (block[16])begin
                            if (light_h >= blockleft[16] && light_h <blockleft[16]+50) n_block[16] <= 0;
                            else if (light_h +30 > blockleft[16] && light_h +30 <=blockleft[16]+50) n_block[16] <=0;
                        end
                        if (block[17])begin
                            if (light_h >= blockleft[17] && light_h <blockleft[17]+50) n_block[17] <= 0;
                            else if (light_h +30 > blockleft[17] && light_h +30 <=blockleft[17]+50) n_block[17] <=0;
                        end
                        if (block[18])begin
                            if (light_h >= blockleft[18] && light_h <blockleft[18]+50) n_block[18] <= 0;
                            else if (light_h +30 > blockleft[18] && light_h +30 <=blockleft[18]+50) n_block[18] <=0;
                        end
                        if (block[19])begin
                            if (light_h >= blockleft[19] && light_h <blockleft[19]+50) n_block[19] <= 0;
                            else if (light_h +30 > blockleft[19] && light_h +30 <=blockleft[19]+50) n_block[19] <=0;
                        end
                        if (block[20])begin
                            if (light_h >= blockleft[20] && light_h <blockleft[20]+50) n_block[20] <= 0;
                            else if (light_h +30 > blockleft[20] && light_h +30 <=blockleft[20]+50) n_block[20] <=0;
                        end
                        if (block[21])begin
                            if (light_h >= blockleft[21] && light_h <blockleft[21]+50) n_block[21] <= 0;
                            else if (light_h +30 > blockleft[21] && light_h +30 <=blockleft[21]+50) n_block[21] <=0;
                        end
                        if (block[22])begin
                            if (light_h >= blockleft[22] && light_h <blockleft[22]+50) n_block[22] <= 0;
                            else if (light_h +30 > blockleft[22] && light_h +30 <=blockleft[22]+50) n_block[22] <=0;
                        end
                        if (block[23])begin
                            if (light_h >= blockleft[23] && light_h <blockleft[23]+50) n_block[23] <= 0;
                            else if (light_h +30 > blockleft[23] && light_h +30 <=blockleft[23]+50) n_block[23] <=0;
                        end
                    end
                end
                LIGHTNING:begin
                    n_LED <= LED;
                    n_h <= light_h;
                    n_state <= LIGHTNING;
                    n_showlight <= showlight;
                    n_count <= count -1;
                    if (count==0)begin
                       n_showlight <= 0;
                       n_state <= WAIT; 
                    end
                end
                default :begin
                    n_h <= pikapos;
                    n_state <= WAIT;
                    n_showlight <= showlight;
                    n_LED <= 0;
                    n_count <= 4'd10;
                end
            endcase
        end
    end
endmodule
module Lightning4(input clk,
                input reset,
                input [9:0]LED,
                input isplay,
                input [9:0]pikapos,
                input [23:0]block,
                input light,
                input exit,
                input [3:0]Top_state,
                output reg [9:0]light_h,
                output reg [23:0]Lightblock,
                output reg [9:0]LightLED,
                output reg showlight
    );
    
    parameter WAIT = 1'b0;
    parameter LIGHTNING = 1'b1; 
     wire [9:0] blockleft [0:23]={
         10'd40,10'd100,10'd190,10'd250
         ,10'd70,10'd220,10'd100,10'd190,10'd10,10'd40,10'd70,10'd130
         ,10'd160,10'd220,10'd250,10'd280,10'd100,10'd190,10'd70,10'd220
         ,10'd40,10'd100,10'd190,10'd250
         };
    
    reg [9:0]n_h;
    reg [23:0]n_block;
    reg n_showlight;
    reg state,n_state;
    reg [9:0]n_LED;
    reg [3:0]count,n_count;
    
    
    always @(posedge clk or posedge reset or posedge exit)begin
        if (reset ||exit||Top_state!=4'b0100)begin
            LightLED <= 10'd0;
            light_h<= 10'd0;
            Lightblock <= 24'b111111111111111111111111;
            showlight <= 0;
            state <= WAIT;
            count <= 4'd10;
        end
        else begin
            LightLED <= n_LED;
            light_h <= n_h;
            Lightblock <= n_block;
            showlight <= n_showlight;
            state <= n_state;
            count <= n_count;
        end
    end
    
    always@(*)begin
        n_block <= block;
        n_h <= pikapos;
        n_state <= WAIT;
        n_showlight <= showlight;
        n_LED <= LED;
        n_count <= 4'd10;
        if (isplay)begin
            case(state)
                WAIT :begin
                    n_LED <= LED;
                    n_h <= pikapos +5; 
                    n_state <= WAIT;
                    n_showlight <= 1'b0;
                    n_count <= 4'd10;
                    if(light &&LED ==10'b1111111111)begin
                        n_LED <= 0;
                        n_state <= LIGHTNING;
                        n_showlight <= 1'b1;
                        if (block[0])begin
                            if (light_h >= blockleft[0] && light_h <blockleft[0]+50) n_block[0] <= 0;
                            else if (light_h +30 > blockleft[0] && light_h +30 <=blockleft[0]+50) n_block[0] <=0;
                        end
                        if (block[1])begin
                            if (light_h >= blockleft[1] && light_h <blockleft[1]+50) n_block[1] <= 0;
                            else if (light_h +30 > blockleft[1] && light_h +30 <=blockleft[1]+50) n_block[1] <=0;
                        end
                        if (block[2])begin
                            if (light_h >= blockleft[2] && light_h <blockleft[2]+50) n_block[2] <= 0;
                            else if (light_h +30 > blockleft[2] && light_h +30 <=blockleft[2]+50) n_block[2] <=0;
                        end
                        if (block[3])begin
                            if (light_h >= blockleft[3] && light_h <blockleft[3]+50) n_block[3] <= 0;
                            else if (light_h +30 > blockleft[3] && light_h +30 <=blockleft[3]+50) n_block[3] <=0;
                        end
                        if (block[4])begin
                            if (light_h >= blockleft[4] && light_h <blockleft[4]+50) n_block[4] <= 0;
                            else if (light_h +30 > blockleft[4] && light_h +30 <=blockleft[4]+50) n_block[4] <=0;
                        end
                        if (block[5])begin
                            if (light_h >= blockleft[5] && light_h <blockleft[5]+50) n_block[5] <= 0;
                            else if (light_h +30 > blockleft[5] && light_h +30 <=blockleft[5]+50) n_block[5] <=0;
                        end
                        if (block[6])begin
                            if (light_h >= blockleft[6] && light_h <blockleft[6]+50) n_block[6] <= 0;
                            else if (light_h +30 > blockleft[6] && light_h +30 <=blockleft[6]+50) n_block[6] <=0;
                        end
                        if (block[7])begin
                            if (light_h >= blockleft[7] && light_h <blockleft[7]+50) n_block[7] <= 0;
                            else if (light_h +30 > blockleft[7] && light_h +30 <=blockleft[7]+50) n_block[7] <=0;
                        end
                        if (block[8])begin
                            if (light_h >= blockleft[8] && light_h <blockleft[8]+50) n_block[8] <= 0;
                            else if (light_h +30 > blockleft[8] && light_h +30 <=blockleft[8]+50) n_block[8] <=0;
                        end
                        if (block[9])begin
                            if (light_h >= blockleft[9] && light_h <blockleft[9]+50) n_block[9] <= 0;
                            else if (light_h +30 > blockleft[9] && light_h +30 <=blockleft[9]+50) n_block[9] <=0;
                        end
                        if (block[10])begin
                            if (light_h >= blockleft[10] && light_h <blockleft[10]+50) n_block[10] <= 0;
                            else if (light_h +30 > blockleft[10] && light_h +30 <=blockleft[10]+50) n_block[10] <=0;
                        end
                        if (block[11])begin
                            if (light_h >= blockleft[11] && light_h <blockleft[11]+50) n_block[11] <= 0;
                            else if (light_h +30 > blockleft[11] && light_h +30 <=blockleft[11]+50) n_block[11] <=0;
                        end
                        if (block[12])begin
                            if (light_h >= blockleft[12] && light_h <blockleft[12]+50) n_block[12] <= 0;
                            else if (light_h +30 > blockleft[12] && light_h +30 <=blockleft[12]+50) n_block[12] <=0;
                        end
                        if (block[13])begin
                            if (light_h >= blockleft[13] && light_h <blockleft[13]+50) n_block[13] <= 0;
                            else if (light_h +30 > blockleft[13] && light_h +30 <=blockleft[13]+50) n_block[13] <=0;
                        end
                        if (block[14])begin
                            if (light_h >= blockleft[14] && light_h <blockleft[14]+50) n_block[14] <= 0;
                            else if (light_h +30 > blockleft[14] && light_h +30 <=blockleft[14]+50) n_block[14] <=0;
                        end
                        if (block[15])begin
                            if (light_h >= blockleft[15] && light_h <blockleft[15]+50) n_block[15] <= 0;
                            else if (light_h +30 > blockleft[15] && light_h +30 <=blockleft[15]+50) n_block[15] <=0;
                        end
                        if (block[16])begin
                            if (light_h >= blockleft[16] && light_h <blockleft[16]+50) n_block[16] <= 0;
                            else if (light_h +30 > blockleft[16] && light_h +30 <=blockleft[16]+50) n_block[16] <=0;
                        end
                        if (block[17])begin
                            if (light_h >= blockleft[17] && light_h <blockleft[17]+50) n_block[17] <= 0;
                            else if (light_h +30 > blockleft[17] && light_h +30 <=blockleft[17]+50) n_block[17] <=0;
                        end
                        if (block[18])begin
                            if (light_h >= blockleft[18] && light_h <blockleft[18]+50) n_block[18] <= 0;
                            else if (light_h +30 > blockleft[18] && light_h +30 <=blockleft[18]+50) n_block[18] <=0;
                        end
                        if (block[19])begin
                            if (light_h >= blockleft[19] && light_h <blockleft[19]+50) n_block[19] <= 0;
                            else if (light_h +30 > blockleft[19] && light_h +30 <=blockleft[19]+50) n_block[19] <=0;
                        end
                        if (block[20])begin
                            if (light_h >= blockleft[20] && light_h <blockleft[20]+50) n_block[20] <= 0;
                            else if (light_h +30 > blockleft[20] && light_h +30 <=blockleft[20]+50) n_block[20] <=0;
                        end
                        if (block[21])begin
                            if (light_h >= blockleft[21] && light_h <blockleft[21]+50) n_block[21] <= 0;
                            else if (light_h +30 > blockleft[21] && light_h +30 <=blockleft[21]+50) n_block[21] <=0;
                        end
                        if (block[22])begin
                            if (light_h >= blockleft[22] && light_h <blockleft[22]+50) n_block[22] <= 0;
                            else if (light_h +30 > blockleft[22] && light_h +30 <=blockleft[22]+50) n_block[22] <=0;
                        end
                        if (block[23])begin
                            if (light_h >= blockleft[23] && light_h <blockleft[23]+50) n_block[23] <= 0;
                            else if (light_h +30 > blockleft[23] && light_h +30 <=blockleft[23]+50) n_block[23] <=0;
                        end
                    end
                end
                LIGHTNING:begin
                    n_LED <= LED;
                    n_h <= light_h;
                    n_state <= LIGHTNING;
                    n_showlight <= showlight;
                    n_count <= count -1;
                    if (count==0)begin
                       n_showlight <= 0;
                       n_state <= WAIT; 
                    end
                end
                default :begin
                    n_h <= pikapos;
                    n_state <= WAIT;
                    n_showlight <= showlight;
                    n_LED <= 0;
                    n_count <= 4'd10;
                end
            endcase
        end
    end
endmodule
module Lightning5(input clk,
                input reset,
                input [9:0]LED,
                input isplay,
                input [9:0]pikapos,
                input [23:0]block,
                input light,
                input exit,
                input [3:0]Top_state,
                output reg [9:0]light_h,
                output reg [23:0]Lightblock,
                output reg [9:0]LightLED,
                output reg showlight
    );
    
    parameter WAIT = 1'b0;
    parameter LIGHTNING = 1'b1; 
    wire [9:0] blockleft [0:23]={
     10'd205,10'd175,10'd205,10'd55,10'd145,10'd175,10'd205,10'd235,10'd25
     ,10'd85,10'd115,10'd145,10'd175,10'd205,10'd235,10'd265,10'd55,10'd145
     ,10'd175,10'd205,10'd235,10'd175,10'd205,10'd205
     };
    
    reg [9:0]n_h;
    reg [23:0]n_block;
    reg n_showlight;
    reg state,n_state;
    reg [9:0]n_LED;
    reg [3:0]count,n_count;
    
    
    always @(posedge clk or posedge reset or posedge exit)begin
        if (reset ||exit||Top_state!=4'b0101)begin
            LightLED <= 10'd0;
            light_h<= 10'd0;
            Lightblock <= 24'b111111111111111111111111;
            showlight <= 0;
            state <= WAIT;
            count <= 4'd10;
        end
        else begin
            LightLED <= n_LED;
            light_h <= n_h;
            Lightblock <= n_block;
            showlight <= n_showlight;
            state <= n_state;
            count <= n_count;
        end
    end
    
    always@(*)begin
        n_block <= block;
        n_h <= pikapos;
        n_state <= WAIT;
        n_showlight <= showlight;
        n_LED <= LED;
        n_count <= 4'd10;
        if (isplay)begin
            case(state)
                WAIT :begin
                    n_LED <= LED;
                    n_h <= pikapos +5; 
                    n_state <= WAIT;
                    n_showlight <= 1'b0;
                    n_count <= 4'd10;
                    if(light &&LED ==10'b1111111111)begin
                        n_LED <= 0;
                        n_state <= LIGHTNING;
                        n_showlight <= 1'b1;
                        if (block[0])begin
                            if (light_h >= blockleft[0] && light_h <blockleft[0]+50) n_block[0] <= 0;
                            else if (light_h +30 > blockleft[0] && light_h +30 <=blockleft[0]+50) n_block[0] <=0;
                        end
                        if (block[1])begin
                            if (light_h >= blockleft[1] && light_h <blockleft[1]+50) n_block[1] <= 0;
                            else if (light_h +30 > blockleft[1] && light_h +30 <=blockleft[1]+50) n_block[1] <=0;
                        end
                        if (block[2])begin
                            if (light_h >= blockleft[2] && light_h <blockleft[2]+50) n_block[2] <= 0;
                            else if (light_h +30 > blockleft[2] && light_h +30 <=blockleft[2]+50) n_block[2] <=0;
                        end
                        if (block[3])begin
                            if (light_h >= blockleft[3] && light_h <blockleft[3]+50) n_block[3] <= 0;
                            else if (light_h +30 > blockleft[3] && light_h +30 <=blockleft[3]+50) n_block[3] <=0;
                        end
                        if (block[4])begin
                            if (light_h >= blockleft[4] && light_h <blockleft[4]+50) n_block[4] <= 0;
                            else if (light_h +30 > blockleft[4] && light_h +30 <=blockleft[4]+50) n_block[4] <=0;
                        end
                        if (block[5])begin
                            if (light_h >= blockleft[5] && light_h <blockleft[5]+50) n_block[5] <= 0;
                            else if (light_h +30 > blockleft[5] && light_h +30 <=blockleft[5]+50) n_block[5] <=0;
                        end
                        if (block[6])begin
                            if (light_h >= blockleft[6] && light_h <blockleft[6]+50) n_block[6] <= 0;
                            else if (light_h +30 > blockleft[6] && light_h +30 <=blockleft[6]+50) n_block[6] <=0;
                        end
                        if (block[7])begin
                            if (light_h >= blockleft[7] && light_h <blockleft[7]+50) n_block[7] <= 0;
                            else if (light_h +30 > blockleft[7] && light_h +30 <=blockleft[7]+50) n_block[7] <=0;
                        end
                        if (block[8])begin
                            if (light_h >= blockleft[8] && light_h <blockleft[8]+50) n_block[8] <= 0;
                            else if (light_h +30 > blockleft[8] && light_h +30 <=blockleft[8]+50) n_block[8] <=0;
                        end
                        if (block[9])begin
                            if (light_h >= blockleft[9] && light_h <blockleft[9]+50) n_block[9] <= 0;
                            else if (light_h +30 > blockleft[9] && light_h +30 <=blockleft[9]+50) n_block[9] <=0;
                        end
                        if (block[10])begin
                            if (light_h >= blockleft[10] && light_h <blockleft[10]+50) n_block[10] <= 0;
                            else if (light_h +30 > blockleft[10] && light_h +30 <=blockleft[10]+50) n_block[10] <=0;
                        end
                        if (block[11])begin
                            if (light_h >= blockleft[11] && light_h <blockleft[11]+50) n_block[11] <= 0;
                            else if (light_h +30 > blockleft[11] && light_h +30 <=blockleft[11]+50) n_block[11] <=0;
                        end
                        if (block[12])begin
                            if (light_h >= blockleft[12] && light_h <blockleft[12]+50) n_block[12] <= 0;
                            else if (light_h +30 > blockleft[12] && light_h +30 <=blockleft[12]+50) n_block[12] <=0;
                        end
                        if (block[13])begin
                            if (light_h >= blockleft[13] && light_h <blockleft[13]+50) n_block[13] <= 0;
                            else if (light_h +30 > blockleft[13] && light_h +30 <=blockleft[13]+50) n_block[13] <=0;
                        end
                        if (block[14])begin
                            if (light_h >= blockleft[14] && light_h <blockleft[14]+50) n_block[14] <= 0;
                            else if (light_h +30 > blockleft[14] && light_h +30 <=blockleft[14]+50) n_block[14] <=0;
                        end
                        if (block[15])begin
                            if (light_h >= blockleft[15] && light_h <blockleft[15]+50) n_block[15] <= 0;
                            else if (light_h +30 > blockleft[15] && light_h +30 <=blockleft[15]+50) n_block[15] <=0;
                        end
                        if (block[16])begin
                            if (light_h >= blockleft[16] && light_h <blockleft[16]+50) n_block[16] <= 0;
                            else if (light_h +30 > blockleft[16] && light_h +30 <=blockleft[16]+50) n_block[16] <=0;
                        end
                        if (block[17])begin
                            if (light_h >= blockleft[17] && light_h <blockleft[17]+50) n_block[17] <= 0;
                            else if (light_h +30 > blockleft[17] && light_h +30 <=blockleft[17]+50) n_block[17] <=0;
                        end
                        if (block[18])begin
                            if (light_h >= blockleft[18] && light_h <blockleft[18]+50) n_block[18] <= 0;
                            else if (light_h +30 > blockleft[18] && light_h +30 <=blockleft[18]+50) n_block[18] <=0;
                        end
                        if (block[19])begin
                            if (light_h >= blockleft[19] && light_h <blockleft[19]+50) n_block[19] <= 0;
                            else if (light_h +30 > blockleft[19] && light_h +30 <=blockleft[19]+50) n_block[19] <=0;
                        end
                        if (block[20])begin
                            if (light_h >= blockleft[20] && light_h <blockleft[20]+50) n_block[20] <= 0;
                            else if (light_h +30 > blockleft[20] && light_h +30 <=blockleft[20]+50) n_block[20] <=0;
                        end
                        if (block[21])begin
                            if (light_h >= blockleft[21] && light_h <blockleft[21]+50) n_block[21] <= 0;
                            else if (light_h +30 > blockleft[21] && light_h +30 <=blockleft[21]+50) n_block[21] <=0;
                        end
                        if (block[22])begin
                            if (light_h >= blockleft[22] && light_h <blockleft[22]+50) n_block[22] <= 0;
                            else if (light_h +30 > blockleft[22] && light_h +30 <=blockleft[22]+50) n_block[22] <=0;
                        end
                        if (block[23])begin
                            if (light_h >= blockleft[23] && light_h <blockleft[23]+50) n_block[23] <= 0;
                            else if (light_h +30 > blockleft[23] && light_h +30 <=blockleft[23]+50) n_block[23] <=0;
                        end
                    end
                end
                LIGHTNING:begin
                    n_LED <= LED;
                    n_h <= light_h;
                    n_state <= LIGHTNING;
                    n_showlight <= showlight;
                    n_count <= count -1;
                    if (count==0)begin
                       n_showlight <= 0;
                       n_state <= WAIT; 
                    end
                end
                default :begin
                    n_h <= pikapos;
                    n_state <= WAIT;
                    n_showlight <= showlight;
                    n_LED <= 0;
                    n_count <= 4'd10;
                end
            endcase
        end
    end
endmodule