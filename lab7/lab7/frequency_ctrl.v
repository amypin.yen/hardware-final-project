`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2016/11/10 20:15:03
// Design Name: 
// Module Name: frequency_ctrl
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module frequency_ctrl(reset,clk,ready, ready_out,freq);
    input reset;
    input clk;
    input ready;
    input [31:0]freq;
    output ready_out;

    parameter WAIT = 1'b0;
    parameter COUNT = 1'b1;
    reg state;
    reg [31:0]count_down ;
    reg ready_out;
    reg [511:0]key_out;
    always @(posedge clk or posedge reset)begin
        if (reset)begin
            state <= WAIT;
            count_down = 100_000_000 / freq;
            ready_out = 1'b0;
            key_out = 512'd0;
       end
       else begin
            case(state)
                WAIT: begin
                    if (ready ==1'b1)begin
                        ready_out = 1'b1;
                        count_down = count_down-16'd1;
                        state = COUNT;
                    end
                    else begin
                        ready_out = 1'b0;
                        count_down = 100_000_000 / freq;
                        state = WAIT;
                    end
                end
                COUNT:begin
                    if (count_down==16'd0)begin
                        ready_out = 1'b0;
                        count_down = 100_000_000 / freq;
                        state = WAIT;
                    end
                    else begin
                        ready_out = 1'b1;
                        count_down = count_down - 16'd1;
                        state = COUNT;
                        key_out = key_out;
                    end
                end
                default :begin
                    ready_out = 1'b0;
                    count_down = 100_000_000 / freq;
                    state = WAIT;
                end
            endcase
        end
     end                
endmodule

