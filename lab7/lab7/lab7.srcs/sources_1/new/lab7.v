`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2016/11/17 18:47:14
// Design Name: 
// Module Name: lab7
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module lab7(clk,Stop,Play,Speed,Quality,Mute,Repeat,Music,pmod_1,pmod_2,pmod_3);
    input clk;
    input Stop,Play;
    input [1:0]Speed,Quality;
    input Mute,Repeat,Music;
    output pmod_1,pmod_2,pmod_3;
    
    assign pmod_2 = 1'b1;
    assign pmod_3 = (Mute)? 1'b0:1'b1;
    reg play=1'b0;
    wire s_clk= (play)? clk:1'b1;
    wire [31:0]s_tone = (Music)? tone2:tone1;
    wire [8:0]ibeatNum1,ibeatNum2;
    wire [31:0]tone1,tone2;
    wire Stop1 =(pul_stop)? 1'b1:
                (Music)? 1'b1:1'b0;
    wire Stop2 =(pul_stop)? 1'b1:
                (Music)? 1'b0:1'b1;
    wire [9:0]duty = (Quality[1]&&Quality[0])? 10'd512:
                    (Quality[1])? 10'd384:
                    (Quality[0])? 10'd256:10'd128;
    wire [31:0]freq = (Speed[1]&&Speed[0])? 32'd16:
                    (Speed[1])? 32'd12:
                    (Speed[0])? 32'd4:32'd8;
    debounce d1(de_stop,Stop,clk);
    onepulse on1(de_stop,clk,pul_stop);
    debounce d2(de_play,Play,clk);
    onepulse on2(de_play,clk,pul_play);
    frequency_ctrl f1(.reset(pul_stop),.clk(clk),.ready(pul_play), .ready_out(freq_play),.freq(freq));
    parameter BEAT_FREQ = 32'd8;	//one beat=0.125sec
    parameter DUTY_BEST = 10'd512;    //duty cycle=50%
    PWM_gen btSpeedGen ( .clk(s_clk), .reset(pul_stop),.freq(freq),.duty(DUTY_BEST), .PWM(beatfreq));
    
    //manipulate beat
    PlayerCtrl playerCtrl_00 ( .clk(beatfreq),.reset(Stop1), .Repeat(Repeat), .pul_play(freq_play),.ibeat(ibeatNum1));
    PlayerCtrl #(63) playerCtrl_01( .clk(beatfreq),.reset(Stop2), .Repeat(Repeat), .pul_play(freq_play),.ibeat(ibeatNum2));    
        
    //Generate variant freq. of tones
    Music music00 ( .ibeatNum(ibeatNum1),.tone(tone1));
    Music2 music01( .ibeatNum(ibeatNum2),.tone(tone2));
    
    // Generate particular freq. signal
    PWM_gen toneGen ( .clk(s_clk), .reset(pul_stop), .freq(s_tone),.duty(duty), .PWM(pmod_1));
    
    always@(posedge pul_stop or posedge pul_play)begin
        if (pul_stop)begin
            play = 1'b0;
        end
        else begin
            if (pul_play)begin
                if (Repeat)play = ~play;
                else begin
                    if (ibeatNum1 == 9'd128|| ibeatNum2 == 9'd64)play <=play;
                    else play = ~play;
                end
            end
            else begin
                play = play;
            end
        end
    end 
    
endmodule
