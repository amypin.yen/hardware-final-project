//
//
//
//

module PlayerCtrl (
	input clk,
	input reset,
	input Repeat,
	input pul_play,
	output reg [8:0] ibeat
);
parameter BEATLEAGTH = 127;
reg been;
always @(posedge clk, posedge reset) begin
	if (reset)begin
	   been = 1'b0;
	   ibeat <= 0;
    end
	else if (ibeat < BEATLEAGTH&&been==1'b0) begin
	   been <= been;
	   ibeat <= ibeat + 1;
    end
	else begin
	   if (Repeat&&been!=1'b1)begin
	       been = 1'b0;
	       ibeat <= 0;
	   end
	   else begin
	       if (pul_play)begin
	           been = 1'b0;
	           ibeat <= 0;
	       end
	       else begin
	           been = 1'b1;
	           ibeat <= BEATLEAGTH+1;
	       end
	   end
    end
end

endmodule