`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2016/10/10 03:25:07
// Design Name: 
// Module Name: lab3_2
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module lab3_2(clk,reset,sel_clk,LED);
    input clk,reset,sel_clk;
    output [15:0]LED ;
    
    //initialize
    reg LR=1'b1,AD=1'b1;
    reg [15:0]n_LED ;
    reg [15:0]LED = 16'b0000000000000000;
    wire c_clk = (sel_clk)? clk19:clk26;
    clock_divider c1(clk,clk26);
    clock_divider #(19) c2(clk,clk19);

    always @(posedge c_clk or posedge reset)begin
        if (reset)begin
            LED = 16'b0000000000000000;
            LR = 1'b1;
            AD = 1'b1;
        end
        else begin
            if (LR)begin
                if (AD) n_LED = (LED + 8'b10000000)<<1'b1;
                else n_LED = (LED - 9'b100000000) >>1'b1;
            end
            else begin
                if (AD) n_LED = {8'b00000001,LED[7:0]}>>1'b1;
                else n_LED = (LED<<1'b1) - 9'b100000000;
            end     
            if (n_LED == 16'b1111111100000000||n_LED == 16'b0000000011111111) AD = 1'b0;
            else if (n_LED == 16'b0000000000000000)begin
                AD = 1'b1;
                if (LED >= 9'b100000000) LR = 1'b0;
                else LR = 1'b1;
            end
            LED = n_LED;
        end
    end
endmodule
