@echo off
set xv_path=C:\\Xi\\Vivado\\2016.2\\bin
call %xv_path%/xsim lab3_1_behav -key {Behavioral:sim_1:Functional:lab3_1} -tclbatch lab3_1.tcl -log simulate.log
if "%errorlevel%"=="0" goto SUCCESS
if "%errorlevel%"=="1" goto END
:END
exit 1
:SUCCESS
exit 0
