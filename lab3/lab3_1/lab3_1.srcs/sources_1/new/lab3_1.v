`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2016/10/06 22:33:30
// Design Name: 
// Module Name: lab3_1
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module lab3_1(clk,reset,LED);
    input clk,reset;
    output [15:0]LED ;
    
    //initialize
    reg LR=1'b1,AD=1'b1;
    reg [15:0]n_LED ;
    reg [15:0]LED = 16'b0000000000000000;
    
    clock_divider c1(clk,clk26);
    
    always @(posedge clk26 or posedge reset)begin
        if (reset)begin
            LED = 16'b0000000000000000;
            LR = 1'b1;
            AD = 1'b1;
        end
        else begin
            if (LR)begin
                if (AD) n_LED = (LED + 8'b10000000)<<1'b1;
                else n_LED = (LED - 9'b100000000) >>1'b1;
            end
            else begin
                if (AD) n_LED = {8'b00000001,LED[7:0]}>>1'b1;
                else n_LED = (LED<<1'b1) - 9'b100000000;
            end     
            if (n_LED == 16'b1111111100000000||n_LED == 16'b0000000011111111) AD = 1'b0;
            else if (n_LED == 16'b0000000000000000)begin
                AD = 1'b1;
                if (LED >= 9'b100000000) LR = 1'b0;
                else LR = 1'b1;
            end
            LED = n_LED;
        end
    end
            
    /*always @(posedge clk26 or posedge reset)begin
        if(reset) begin
            LED[15:0] = 16'b0000000000000000;
            num =4'b1000;
        end
        else begin
            LED = {LED[15:8],8'b10000000};
            num <= n_num;
        end
    end
    assign n_LED = (reset)? 16'b0000000100000000:(LR^UD)? 1'b1:1'b1;
    assign n_num = (reset)? 4'b1001:
                 (num == 4'b1111 && n_num ==4'b1111)? num -1'b1:
                 (num == 4'b0000 && n_num == 4'b0000)? num +1'b1:
                (UD)? num-1:num+1;
    assign LR = (reset)? 1'b1:
            (LED == 0)? LR +1'b1:LR;
    assign UD = (reset)? 1'b1:
            (LED == 16'b1111111100000000||LED == 16'b0000000011111111)? UD +1'b1:UD;
            */
endmodule 
