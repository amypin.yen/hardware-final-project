`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2016/10/13 18:41:34
// Design Name: 
// Module Name: Lab4_1
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Lab4_1(DIGIT,DISPLAY,cout,en,reset,dir,clk);
    input en,reset,dir,clk;
    output [3:0] DIGIT;
    output [6:0] DISPLAY;
    output cout;
    
    wire [3:0]BCD;
    clock_divider #(24)c1(clk,clk25);
    clock_divider #(12) c2(clk,clk13);
    
    debounce d1(re,reset,clk13);
    debounce d2(enable,en,clk13);
    
    lab2_2 la1(clk25,re,enable,dir,BCD,cout);
    
    seven_seg_decoder de1(clk13,BCD,DISPLAY,DIGIT);
endmodule

module debounce (pb_debounced,pb,clk);
    output pb_debounced;
    input pb,clk;
    
    reg [3:0]shift_reg;
    
    always @(posedge clk)begin
        shift_reg[3:1]<=shift_reg[2:0];
        shift_reg[0]<=pb;
    end
    
    assign pb_debounced = (shift_reg == 4'b1111)? 1'b1:1'b0;
endmodule

module seven_seg_decoder (clk,BCD,dis,dig);
    input clk;
    input [3:0]BCD;
    output [6:0]dis;
    output [3:0]dig;
    assign dig = 4'b1110;
    
    assign dis = (BCD==4'd0) ? 7'b0000001 : 
                    (BCD==4'd1) ? 7'b1001111 : 
                    (BCD==4'd2) ? 7'b0010010 : 
                    (BCD==4'd3) ? 7'b0000110 : 
                    (BCD==4'd4) ? 7'b1001100 : 
                    (BCD==4'd5) ? 7'b0100100 : 
                    (BCD==4'd6) ? 7'b0100000 : 
                    (BCD==4'd7) ? 7'b0001111 : 
                    (BCD==4'd8) ? 7'b0000000 : 
                    (BCD==4'd9) ? 7'b0000100 : 7'b1111111 ;
endmodule