`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2016/10/06 18:17:51
// Design Name: 
// Module Name: clock_divider
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module clock_divider(clk,clk26);
    parameter bit = 25;
    input clk;
    output clk26;
    
    reg [bit:0]count;
    wire [bit:0] next_count;
    
    always @(posedge clk)begin
        count<=next_count;
    end
    assign next_count = count+1;
    assign clk26 =count[bit];
endmodule
