`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2016/10/13 21:58:36
// Design Name: 
// Module Name: Lab4_2
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Lab4_2(DIGIT,DISPLAY,cout,en,reset,dir,clk);
    input en,reset,dir,clk;
    output [3:0] DIGIT;
    output [6:0] DISPLAY;
    output cout;
    
    wire [3:0]BCD0,BCD1;
    clock_divider #(24)c1(clk,clk25);
    clock_divider #(12) c2(clk,clk13);
    
    debounce d1(re,reset,clk13);
    debounce d2(enable,en,clk13);
    
    lab2_2_1 la1(clk25,re,enable,dir,BCD0,en2);
    lab2_2_2 la2(clk25,re,en2,dir,BCD1,cout);
    
    seven_seg_decoder de1(clk13,BCD0,BCD1,DISPLAY,DIGIT);
endmodule

module debounce (pb_debounced,pb,clk);
    output pb_debounced;
    input pb,clk;
    
    reg [3:0]shift_reg;
    
    always @(posedge clk)begin
        shift_reg[3:1]<=shift_reg[2:0];
        shift_reg[0]<=pb;
    end
    
    assign pb_debounced = (shift_reg == 4'b1111)? 1'b1:1'b0;
endmodule

module seven_seg_decoder (clk,BCD0,BCD1,dis,dig);
    input clk;
    input [3:0]BCD0,BCD1;
    output [6:0]dis;
    output [3:0]dig;
    
    reg  [3:0]dig = 4'b1110;
    reg [3:0]value;
    always @(posedge clk)begin
        case(dig)
            4'b1110 :begin
                dig <=4'b1101;
                value <= BCD1;
            end
            4'b1101:begin
                dig <=4'b1110;
                value <= BCD0;
            end
            default begin
                dig <=4'b1111;
            end
        endcase
    end
    assign dis = (value==4'd0) ? 7'b0000001 : 
                    (value==4'd1) ? 7'b1001111 : 
                    (value==4'd2) ? 7'b0010010 : 
                    (value==4'd3) ? 7'b0000110 : 
                    (value==4'd4) ? 7'b1001100 : 
                    (value==4'd5) ? 7'b0100100 : 
                    (value==4'd6) ? 7'b0100000 : 
                    (value==4'd7) ? 7'b0001111 : 
                    (value==4'd8) ? 7'b0000000 : 
                    (value==4'd9) ? 7'b0000100 : 7'b1111111 ;
endmodule