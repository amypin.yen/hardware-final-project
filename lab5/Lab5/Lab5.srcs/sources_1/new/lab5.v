`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2016/10/25 02:09:56
// Design Name: 
// Module Name: lab5
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module lab5(clk,reset,money_5,money_10,cancel,business_ticket,general_ticket
                ,drop_money,drop_business_ticket,drop_general_ticket,DIGIT,DISPLAY);
    input clk,reset;
    input money_5,money_10;
    input cancel;
    input business_ticket,general_ticket;
    
    output [9:0]drop_money;
    output drop_business_ticket,drop_general_ticket;
    output [3:0]DIGIT;
    output [6:0]DISPLAY;
    
    parameter s0 = 2'b00;
    parameter s1 = 2'b01;
    parameter s2 = 2'b10;
    parameter s3 = 2'b11;
        
    reg [0:3] BCD0 = 4'b0000,BCD1 = 4'b0000,BCD2 = 4'b0000,BCD3 = 4'b0000;
    reg [0:1]state,n_state;
    reg drop_business_ticket,drop_general_ticket;
    reg[9:0]drop_money=10'b0000000000;
    clock_divider #(12) d1(clk,clk13);
    clock_divider #(15) d2(clk,clk16);
    clock_divider #(24) d3(clk,clk25);
    
    debounce de1(de_business,business_ticket,clk16);
    debounce de2(de_general,general_ticket,clk16);
    debounce de3(de_cancel,cancel,clk16);
    debounce de4(de_10,money_10,clk16);
    debounce de5(de_5,money_5,clk16);
    
    onepulse on1(de_business,clk16,pul_business);
    onepulse on2(de_general,clk16,pul_general);
    onepulse on3(de_cancel,clk16,pul_cancel);
    onepulse on4(de_10,clk16,pul_10);
    onepulse on5(de_5,clk16,pul_5);
    
    seven_seg_decoder seg1(clk13,BCD0,BCD1,BCD2,BCD3,DISPLAY,DIGIT);
    wire s_clk = (state >=s2)? clk25:clk16;
    
    always @(posedge clk13 or posedge reset)begin
        if (reset)begin
            state <= s0;
        end
        else begin
            state <= n_state ;
        end
    end 
    always @(posedge s_clk or posedge reset)begin
        case (state)
            s0:begin
                drop_business_ticket <=1'b0;
                drop_general_ticket <= 1'b0;
                if (pul_business) begin
                    {BCD3,BCD2} = 8'b01110101;
                    n_state <= s1;
                end
                else if (pul_general) begin
                    {BCD3,BCD2} = 8'b01010101;
                    n_state <=s1;
                end
                else n_state <= s0;
            end
            s1:begin
                drop_business_ticket <=1'b0;
                drop_general_ticket <= 1'b0;
                if (pul_10) BCD1 = BCD1+4'd1;
                else if (pul_5)begin
                    if (BCD0 == 4'd5)begin
                        BCD0 = 4'd0;
                        BCD1 = BCD1+4'd1;
                    end
                    else begin
                        BCD0 =4'd5;                       
                    end
                end
                    
                else if (cancel)begin
                    BCD3 = 4'd0;
                    BCD2 =4'd0;
                    n_state <= s3;
                end
                else begin
                    n_state <=s1;
                end
                if (cancel) n_state <=s3;
                else if ((BCD1==BCD3&&BCD2==BCD0)||BCD1>BCD3) begin     
                    n_state <=s2;        
                end
                else n_state <= s1;
            end
            s2:begin      
                if (BCD0==BCD2 && BCD1==BCD3) begin
                    BCD0= 4'd0;
                    BCD1= 4'd0;
                    BCD2= 4'd0;
                    BCD3= 4'd0;
                end
                else begin
                    BCD0= 4'd5;
                    BCD1= 4'd0;
                    BCD2= 4'd0;
                    BCD3= 4'd0;
                end
                if (BCD3 == 4'b0101) drop_general_ticket = 1'b1;
                else drop_general_ticket = 1'b1;
                BCD3 = 4'd0;
                BCD2 =4'd0;
                n_state <= s3;
            end
            s3:begin
                drop_business_ticket = 1'b0;
                drop_general_ticket = 1'b0;
                if (BCD1!=4'd0 )begin
                    BCD1 = BCD1 - 4'd1;
                    n_state <= s3;
                    drop_money = 10'b1111111111;
                end
                else if (BCD0 !=4'd0)begin
                    BCD1 = 4'd0;
                    BCD0 = 4'd0;
                    n_state <= s3;
                    drop_money = 10'b1111100000;
                end
                else begin
                    BCD1 = 4'd0;
                    BCD0 = 4'd0;
                    n_state = s0;
                    drop_money = 10'd0;
                end
            end
            default :begin
                n_state <=s0 ;
                drop_business_ticket <=1'b0;
                drop_general_ticket <= 1'b0;  
                drop_money = 10'd0; 
            end
        endcase
        if (reset)begin
            BCD0 <=4'd0;
            BCD1 <=4'd0;
            BCD2 <=4'd0;
            BCD3 <=4'd0;
            drop_business_ticket <=1'b0;
            drop_general_ticket <= 1'b0;  
            drop_money = 10'd0; 
        end
        else begin
            BCD0 <=BCD0;
            BCD1 <=BCD1;
            BCD2 <=BCD2;
            BCD3 <=BCD3;
            drop_business_ticket <=drop_business_ticket;
            drop_general_ticket <= drop_general_ticket;  
            drop_money = drop_money; 
        end
            
    end
    
                    
endmodule

module debounce (pb_debounced,pb,clk);
    output pb_debounced;
    input pb,clk;
    
    reg [3:0]shift_reg;
    
    always @(posedge clk)begin
        shift_reg[3:1]<=shift_reg[2:0];
        shift_reg[0]<=pb;
    end
    
    assign pb_debounced = (shift_reg == 4'b1111)? 1'b1:1'b0;
endmodule

module onepulse (PB_debounced,clk,PB_single_pulse);
    input PB_debounced,clk;
    output PB_single_pulse;
    
    reg PB_single_pulse;
    reg PB_debounced_delay;
    
    always@(posedge clk)begin
        if (PB_debounced ==1'b1& PB_debounced_delay ==1'b0) PB_single_pulse <=1'b1;
        else PB_single_pulse <=1'b0;
        PB_debounced_delay <=PB_debounced;
    end
endmodule

module seven_seg_decoder (clk,BCD0,BCD1,BCD2,BCD3,dis,dig);
    input clk;
    input [3:0]BCD0,BCD1,BCD2,BCD3;
    output [6:0]dis;
    output [3:0]dig;
    
    reg  [3:0]dig = 4'b1110;
    reg [3:0]value;
    always @(posedge clk)begin
        case(dig)
            4'b1110 :begin
                dig <=4'b1101;
                value <= BCD1;
            end
            4'b1101:begin
                dig <=4'b1011;
                value <= BCD2;
            end
            4'b1011:begin
                dig <=4'b0111;
                value <= BCD3;
            end
            4'b0111:begin
                dig <=4'b1110;
                value <= BCD0;
            end
            default begin
                dig <=4'b1111;
                value <= 4'b0000;
            end
        endcase
    end
    assign dis = (value==4'd0) ? 7'b0000001 : 
                    (value==4'd1) ? 7'b1001111 : 
                    (value==4'd2) ? 7'b0010010 : 
                    (value==4'd3) ? 7'b0000110 : 
                    (value==4'd4) ? 7'b1001100 : 
                    (value==4'd5) ? 7'b0100100 : 
                    (value==4'd6) ? 7'b0100000 : 
                    (value==4'd7) ? 7'b0001111 : 
                    (value==4'd8) ? 7'b0000000 : 
                    (value==4'd9) ? 7'b0000100 : 7'b1111111 ;
endmodule
