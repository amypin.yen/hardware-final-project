`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2016/10/25 02:09:56
// Design Name: 
// Module Name: lab5
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module lab6(clk,reset,PS2_DATA,PS2_CLK,drop_money,drop_business_ticket,drop_general_ticket,DIGIT,DISPLAY);
    input clk,reset;
    
    inout PS2_DATA;
    inout PS2_CLK;
    
    output [9:0]drop_money;
    output drop_business_ticket,drop_general_ticket;
    output [3:0]DIGIT;
    output [6:0]DISPLAY;
    
    parameter s0 = 2'b00;
    parameter s1 = 2'b01;
    parameter s2 = 2'b10;
    parameter s3 = 2'b11;
    
    parameter [8:0] B_char = 9'b0_0011_0010;
    parameter [8:0] G_char = 9'b0_0011_0100;
    parameter [8:0] LE_shift = 9'b0_0001_0010;
    parameter [8:0] RI_shift = 9'b0_0101_1001;

    wire [8:0] KEY_CODES [0:19] = {
        9'b0_0100_0101,    // 0 => 45
        9'b0_0001_0110,    // 1 => 16
        9'b0_0001_1110,    // 2 => 1E
        9'b0_0010_0110,    // 3 => 26
        9'b0_0010_0101,    // 4 => 25
        9'b0_0010_1110,    // 5 => 2E
        9'b0_0011_0110,    // 6 => 36
        9'b0_0011_1101,    // 7 => 3D
        9'b0_0111_0110,    // 8 => 3E
        9'b0_0100_0110,    // 9 => 46
        
        9'b0_0111_0000, // right_0 => 70
        9'b0_0110_1001, // right_1 => 69
        9'b0_0111_0010, // right_2 => 72
        9'b0_0111_1010, // right_3 => 7A
        9'b0_0110_1011, // right_4 => 6B
        9'b0_0111_0011, // right_5 => 73
        9'b0_0111_0100, // right_6 => 74
        9'b0_0110_1100, // right_7 => 6C
        9'b0_0111_0110, // right_8 => 75
        9'b0_0111_1101  // right_9 => 7D
    };
    reg [0:3] BCD0 = 4'b0000,BCD1 = 4'b0000,BCD2 = 4'b0000,BCD3 = 4'b0000;
    reg [0:1]state,n_state;
    reg drop_business_ticket,drop_general_ticket;
    reg[9:0]drop_money=10'b0000000000;   
    
    wire shift_down;
    wire [511:0] key_down;
    wire [8:0] last_change;
    wire been_ready; 
    reg [3:0] key_num;
    KeyboardDecoder key_de(
        .key_down(key_down),
        .last_change(last_change),
        .key_valid(been_ready),
        .PS2_DATA(PS2_DATA),
        .PS2_CLK(PS2_CLK),
        .rst(reset),
        .clk(clk)
    );
    wire key_out,ready_out;

    frequency_ctrl f1(reset,clk, been_ready, key_down, ready_out,key_out);

   
    wire shift = (key_down[LE_shift]||key_down[RI_shift])? 1'b1:1'b0;
    
    clock_divider #(12) d1(clk,clk13);
    clock_divider #(15) d2(clk,clk16);
    clock_divider #(24) d3(clk,clk25);
    
    seven_seg_decoder seg1(clk13,BCD0,BCD1,BCD2,BCD3,DISPLAY,DIGIT);
    wire s_clk = (state >=s2)? clk25:clk16;
    
    always @(posedge clk13 or posedge reset)begin
        if (reset)begin
            state <= s0;
        end
        else begin
            state <= n_state ;
        end
    end 
    always @(posedge s_clk or posedge reset)begin
        case (state)
            s0:begin
                drop_money = 10'b0000000000;
                drop_business_ticket =1'b0;
                drop_general_ticket = 1'b0;
                if (ready_out&&key_down[B_char]&&shift&&last_change == B_char) begin
                    BCD0 = 4'd0;
                    BCD1 = 4'd0;
                    BCD2 = 4'd3;
                    BCD3 = 4'd7;
                    n_state <= s1;
                end
                else if (ready_out&&key_down[G_char]&&shift&&last_change == G_char) begin
                    BCD0 = 4'd0;
                    BCD1 = 4'd0;
                    BCD2 = 4'd6;
                    BCD3 = 4'd6;
                    n_state <=s1;
                end
                else n_state <= s0;
            end
            s1:begin
                drop_money = 10'b0000000000;    
                drop_business_ticket =1'b0;
                drop_general_ticket = 1'b0;
                if (ready_out&&(|key_down)&&key_num != 4'b1111) begin
                    if (key_num == 4'd1)begin
                        BCD0 = (BCD0 + 4'd1)%10;
                        if (BCD0 ==4'd0) BCD1 = BCD1 + 4'd1;
                        else BCD1 <= BCD1;
                        BCD2 <= BCD2;
                        BCD3 <= BCD3;
                    end
                    else if (key_num == 4'd5)begin
                        BCD0 = (BCD0 + 4'd5)%10;
                        if (BCD0 < 4'd5) BCD1 = BCD1 +4'd1;
                        else BCD1 <= BCD1;
                        BCD2 <= BCD2;
                        BCD3 <= BCD3;
                    end
                    else if (key_num == 4'd0)begin
                        BCD0 <= BCD0;
                        BCD1 = BCD1+4'd1;
                        BCD2 <= BCD2;
                        BCD3 <= BCD3;
                    end
                    else if (key_num == 4'd8)begin
                        BCD0 <= BCD0;         
                        BCD1 <= BCD1;
                        BCD3 = 4'd0;
                        BCD2 = 4'd0;
                    end
                    else begin
                        BCD0 <= BCD0;
                        BCD1 <= BCD1;
                        BCD2 <= BCD2;
                        BCD3 <= BCD3;
                    end
                    if ({BCD3,BCD2}<={BCD1,BCD0}&&key_num!=4'd8) n_state <=s2;        
                    else if (key_num==4'd8)n_state <= s3;    
                    else  n_state <= s1;
                end
                else begin
                    BCD0 <= BCD0;
                    BCD1 <= BCD1;
                    BCD2 <= BCD2;
                    BCD3 <= BCD3;
                    n_state <=s1;
                end  
            end
            s2:begin      
                drop_money = 10'b0000000000;
                if (BCD3 == 4'd6) drop_general_ticket = 1'b1;
                else drop_business_ticket = 1'b1;
                if (BCD2>BCD0) begin
                    BCD0= BCD0+4'd10-BCD2;
                    BCD1= BCD1-BCD3-4'd1;
                    BCD2= 4'd0;
                    BCD3= 4'd0;
                end
                else begin
                    BCD0= BCD0-BCD2;
                    BCD1= BCD1-BCD3;
                    BCD2= 4'd0;
                    BCD3= 4'd0;
                end
                n_state <= s3;
            end
            s3:begin
                drop_business_ticket = 1'b0;
                drop_general_ticket = 1'b0;
                if (BCD1!=4'd0)begin
                    BCD0 = BCD0;               
                    BCD1 = BCD1 - 4'd1;
                    BCD2 <= BCD2;
                    BCD3 <= BCD3;
                    n_state <= s3;
                    drop_money = 10'b1111111111;
                end
                else if (BCD0 >=4'd5)begin
                    BCD0 = BCD0-4'd5;
                    BCD1 = BCD1;
                    BCD2 <= BCD2;
                    BCD3 <= BCD3;
                    n_state <= s3;
                    drop_money = 10'b1111100000;
                end
                else if (BCD0!=4'd0&&BCD0 <4'd5)begin
                    BCD0 = BCD0-4'd1;
                    BCD1 = BCD1;
                    BCD2 <= BCD2;
                    BCD3 <= BCD3;
                    n_state <= s3;
                    drop_money = 10'b1000000000;
                end
                else begin
                    BCD0 <= BCD0;
                    BCD1 <= BCD1;
                    BCD2 <= BCD2;
                    BCD3 <= BCD3;
                    n_state = s0;
                    drop_money = 10'd0;
                end
            end
            default :begin
                BCD0 = 4'd0;
                BCD1 = 4'd0;
                BCD2 = 4'd0;
                BCD3 = 4'd0;
                n_state <=s0 ;
                drop_business_ticket <=1'b0;
                drop_general_ticket <= 1'b0;  
                drop_money = 10'd0; 
            end
        endcase
        if (reset)begin
            BCD0 <=4'd0;
            BCD1 <=4'd0;
            BCD2 <=4'd0;
            BCD3 <=4'd0;
            drop_business_ticket <=1'b0;
            drop_general_ticket <= 1'b0;  
            drop_money <= 10'd0; 
        end
        else begin
            BCD0 <=BCD0;
            BCD1 <=BCD1;
            BCD2 <=BCD2;
            BCD3 <=BCD3;
            drop_business_ticket <=drop_business_ticket;
            drop_general_ticket <= drop_general_ticket;  
            drop_money = drop_money; 
        end       
    end  
    
    always @ (*) begin
        case (last_change)
            KEY_CODES[00] : key_num = 4'b0000;
            KEY_CODES[01] : key_num = 4'b0001;
            KEY_CODES[02] : key_num = 4'b0010;
            KEY_CODES[03] : key_num = 4'b0011;
            KEY_CODES[04] : key_num = 4'b0100;
            KEY_CODES[05] : key_num = 4'b0101;
            KEY_CODES[06] : key_num = 4'b0110;
            KEY_CODES[07] : key_num = 4'b0111;
            KEY_CODES[08] : key_num = 4'b1000;
            KEY_CODES[09] : key_num = 4'b1001;
            KEY_CODES[10] : key_num = 4'b0000;
            KEY_CODES[11] : key_num = 4'b0001;
            KEY_CODES[12] : key_num = 4'b0010;
            KEY_CODES[13] : key_num = 4'b0011;
            KEY_CODES[14] : key_num = 4'b0100;
            KEY_CODES[15] : key_num = 4'b0101;
            KEY_CODES[16] : key_num = 4'b0110;
            KEY_CODES[17] : key_num = 4'b0111;
            KEY_CODES[18] : key_num = 4'b1000;
            KEY_CODES[19] : key_num = 4'b1001;
            default          : key_num = 4'b1111;
        endcase
    end            
endmodule
