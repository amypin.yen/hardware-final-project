`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2016/11/10 18:39:52
// Design Name: 
// Module Name: seven_seg
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module seven_seg_decoder (clk,BCD0,BCD1,BCD2,BCD3,dis,dig);
    input clk;
    input [3:0]BCD0,BCD1,BCD2,BCD3;
    output [6:0]dis;
    output [3:0]dig;
    
    reg  [3:0]dig = 4'b1110;
    reg [3:0]value;
    always @(posedge clk)begin
        case(dig)
            4'b1110 :begin
                dig <=4'b1101;
                value <= BCD1;
            end
            4'b1101:begin
                dig <=4'b1011;
                value <= BCD2;
            end
            4'b1011:begin
                dig <=4'b0111;
                value <= BCD3;
            end
            4'b0111:begin
                dig <=4'b1110;
                value <= BCD0;
            end
            default begin
                dig <=4'b1111;
                value <= 4'b0000;
            end
        endcase
    end
    assign dis = (value==4'd0) ? 7'b0000001 : 
                    (value==4'd1) ? 7'b1001111 : 
                    (value==4'd2) ? 7'b0010010 : 
                    (value==4'd3) ? 7'b0000110 : 
                    (value==4'd4) ? 7'b1001100 : 
                    (value==4'd5) ? 7'b0100100 : 
                    (value==4'd6) ? 7'b0100000 : 
                    (value==4'd7) ? 7'b0001111 : 
                    (value==4'd8) ? 7'b0000000 : 
                    (value==4'd9) ? 7'b0000100 : 7'b1111111 ;
endmodule
